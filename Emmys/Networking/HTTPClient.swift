//
//  HTTPClient.swift
//  Emmys
//
//  Created by Syed Asim Najam on 31/01/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import Alamofire

public typealias JSON = [String: Any]

final class HTTPClient {
    private static let sharedInstanceAlamoFire: SessionManager = {
        let configuration: URLSessionConfiguration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 180
        configuration.timeoutIntervalForResource = 180
        return SessionManager(configuration: configuration)
    }()
    
    private static func request(
        _ path: String,
        method: HTTPMethod = .get,
        parameters: JSON? = nil
    ) -> DataRequest {
        print("Request URL »»»»»»»»»» \(path)")
        let request = HTTPClient.sharedInstanceAlamoFire.request(
            path,
            method: .get,
            parameters: parameters,
            headers: HTTPHeaders.header
        )
        .validate(contentType: ["application/json"])
        return request
    }
    
    public static func requestData(
        _ path: String,
        parameters: JSON? = nil,
        completion: @escaping (_ obj: Data?) -> Void,
        failure: @escaping (_ status: String, _ error: NSError) -> Void
    ) {
        func handleFailure(response: HTTPURLResponse?, request: URLRequest?) -> (status: String, error: NSError){
            let url = request?.url?.absoluteString
            let description = response?.description
            let error = NSError(domain: "Data", code: response?.statusCode ?? 404, userInfo: nil)
            let errorDescription = "API Request: \(String(describing: url)) failed, description: \(String(describing: description))"
            print(errorDescription)
            return(errorDescription, error)
        }
        request(path, method: .get, parameters: parameters).response { response in
            if let httpResponse = response.response, let data = response.data  {
                switch httpResponse.statusCode {
                case 200..<300:
                    completion(data)
                default:
                    let requestFailure = handleFailure(
                        response: response.response,
                        request: response.request
                    )
                    failure(
                        requestFailure.status,
                        requestFailure.error
                    )
                }
            } else {
                let requestFailure = handleFailure(
                    response: response.response,
                    request: response.request
                )
                failure(
                    requestFailure.status,
                    requestFailure.error
                )
            }
        }
    }
}
