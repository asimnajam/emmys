//
//  PrimaryTextField.swift
//  Emmys
//
//  Created by Syed Asim Najam on 02/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

final class PrimaryTextField: UITextField {
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)

    enum TextFieldType {
        case bordered
        case filled
    }
    
    enum MobileTextFieldType {
        case `default`
        case signup
    }
    
    enum Kind {
        case name
        case mobile(type: MobileTextFieldType)
        case email
        case password
        case confirmPassword
        case otp

        var placeHolder: String {
            switch self {
            case .name:
                return localizationManager.localizedStringForKey(key: "signup_form_name")
            case .mobile:
                return localizationManager.localizedStringForKey(key: "signup_form_mobile")
            case .email:
                return localizationManager.localizedStringForKey(key: "signup_form_email")
            case .password:
                return localizationManager.localizedStringForKey(key: "signup_form_password")
            case .confirmPassword:
                return localizationManager.localizedStringForKey(key: "signup_form_confirm_password")
            case .otp:
                return ""
            }
        }
        
        var image: UIImage? {
            switch self {
            case .name:
                return UIImage(named: "name")?.maskWithColor(color: .white)
            case .email:
                return UIImage(named: "email")?.maskWithColor(color: .white)
            case .mobile(let type):
                switch type {
                case .default:
                    return UIImage(named: "name")?.maskWithColor(color: .white)
                case .signup:
                    return UIImage(named: "call")?.maskWithColor(color: .white)
                }
                
            case .password, .confirmPassword:
                return UIImage(named: "password")?.maskWithColor(color: .white)
            case .otp:
                return nil
            }
        }
        
        var keyboardType: UIKeyboardType {
            switch self {
            case .name:
                return .alphabet
            case .mobile:
                return .numberPad
            case .email:
                return .emailAddress
            case .otp:
                return .default
            default:
                return .default
            }
        }
    }
    
    private let textFieldType: TextFieldType
    init(textFieldType: Kind, type: TextFieldType = .filled) {
        self.textFieldType = type
        super.init(frame: CGRect.zero)
        configure(textFieldType, type: type)
    }

    required init?(coder aDecoder: NSCoder) {
        textFieldType = .filled
        super.init(coder: aDecoder)
    }
    
    private func configure(_ textFieldType: Kind, type: TextFieldType) {
        switch textFieldType {
        case .otp:
            break
        default:
            addLeftImageView(textFieldType, type: type)
        }
        
        
        switch type {
        case .filled:
            backgroundColor = Colors.blueColor
            textColor = .white
        case .bordered:
            layer.cornerRadius = 5.0
            layer.borderColor = Colors.blueColor.cgColor
            layer.borderWidth = 1.0
        }
        
        keyboardType = textFieldType.keyboardType
        isSecureTextEntry = secureTextEntry(textFieldType)
        autocapitalizationType = .none
        switch emmysManager.currentLanguage {
        case .arabic:
            semanticContentAttribute = .forceRightToLeft
            textAlignment = .right
        case .english:
            semanticContentAttribute = .forceLeftToRight
            textAlignment = .left
        }
    }
    
    private func addLeftImageView(_ textFieldType: Kind, type: TextFieldType) {
        let placeHolderColor = type == .filled ? UIColor.white : UIColor.gray
        attributedPlaceholder = NSAttributedString(
            string: textFieldType.placeHolder,
            attributes: [
                NSAttributedString.Key.foregroundColor: placeHolderColor
            ]
        )
        layer.cornerRadius = 5.0
        if type == .bordered {
            return
        }
        let viewPadding = UIView(
            frame: CGRect(
                x: 0,
                y: 0,
                width: 40 ,
                height: Int(frame.size.height)
            )
        )
        
        let imageView = UIImageView (
            frame:CGRect(
                x: 0,
                y: 0,
                width: 20 ,
                height: 20
            )
        )
        imageView.contentMode = .scaleAspectFit
        imageView.center = viewPadding.center
        imageView.image  = textFieldType.image
        viewPadding .addSubview(imageView)
        leftView = viewPadding
        leftViewMode = .always
    }
    
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        if textFieldType == .bordered {
            return bounds.inset(by: padding)
        } else {
            let dx: CGFloat = emmysManager.currentLanguage == .arabic ? -40.0 : 40.0
            return bounds.offsetBy(dx: dx, dy: 0)
            //(by: padding)
        }
    }

    override public func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        if textFieldType == .bordered {
            return bounds.inset(by: padding)
        } else {
            let dx: CGFloat = emmysManager.currentLanguage == .arabic ? -40.0 : 40.0
            return bounds.offsetBy(dx: dx, dy: 0)
        }
    }

    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        if textFieldType == .bordered {
            return bounds.inset(by: padding)
        } else {
            let dx: CGFloat = emmysManager.currentLanguage == .arabic ? -40.0 : 40.0
            return bounds.offsetBy(dx: dx, dy: 0)
        }
    }
    
    private func secureTextEntry(_ kind: Kind) -> Bool {
        switch kind {
        case .confirmPassword, .password:
            return true
        default:
            return false
        }
    }
}
