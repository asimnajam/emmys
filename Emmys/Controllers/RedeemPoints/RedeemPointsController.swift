//
//  RedeemPointsController.swift
//  Emmys
//
//  Created by Asim Najam on 2/9/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import RxSwift

class RedeemPointsController: UIViewController {
    typealias OffersCell = TableViewContainerCell<RedeemPointsCellView>
    static let minRedeemPoints = 500
    let contentView: RedeemPointsView
    let viewType: RedeemPointsView.ViewType
    private let disposebag = DisposeBag()
    
    var offerListWithLanguage: [OfferList] {
        let levels: [String] = [
            LocalizedLanguage.bronze,
            LocalizedLanguage.silver,
            LocalizedLanguage.gold,
            LocalizedLanguage.platinum,
            LocalizedLanguage.diamond
        ]
        let details: [String] = [
            LocalizedLanguage.juice_and_sandwich_free,
            LocalizedLanguage.sar_Coupon_Consumable_in_Emmys_or_Crunched,
            LocalizedLanguage.playstation_4, LocalizedLanguage.iPhone_11,
            LocalizedLanguage.round_Trip_to_Dubai_or_any_approved_location_Hotel_3_Days
        ]
        return OfferList.getList.enumerated().map { OfferList(flag: $0.element.flag, id: $0.element.id, levels: levels[$0.offset], points: $0.element.points, details: details[$0.offset]) }
    }
    
    init(type: RedeemPointsView.ViewType) {
        self.viewType = type
        contentView = RedeemPointsView(frame: .zero, type: type)
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.tableView.registerCell(OffersCell.self)
        contentView.tableView.delegate = self
        contentView.tableView.dataSource = self
        contentView.redeemButton.addTarget(self, action: #selector(showRedeemView), for: .touchUpInside)
        setEmmysLogo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if viewType == .redeemClaim {
            getUserDetail()
            contentView.tableView.reloadData()
        }
    }
    
    @objc func showRedeemView() {
        guard let user = emmysManager.user else { return }
        if let closestOffer = OfferList.closestOffer(user.userBalancePoints) {
            redeemVoucherAPI(offer: closestOffer)
        }
    }
    
    func setupUserPoints(user: UserInfo) {
        showRedeemPopover(user: user)
        if user.userBalancePoints >= RedeemPointsController.minRedeemPoints {
            contentView.redeemButton.isEnabled = true
            contentView.noPointsLabel.isHidden = true
            contentView.underLinedLabel.isHidden = true
            contentView.redeemButton.setTitle("\(LocalizedLanguage.redeem) \(user.userBalancePoints) \(LocalizedLanguage.points)", for: .normal)
        } else {
            contentView.redeemButton.isEnabled = false
            contentView.noPointsLabel.isHidden = false
            contentView.underLinedLabel.isHidden = false
            contentView.redeemButton.setTitle("\(LocalizedLanguage.redeem) \(user.userBalancePoints) \(LocalizedLanguage.points)", for: .normal)
            contentView.noPointsLabel.text = LocalizedLanguage.you_donot_have_sufficient_points_to_redeem
            contentView.underLinedLabel.text = LocalizedLanguage.min_500_points_required
        }
    }
    
    func showRedeemPopover(user: UserInfo) {
        if let closestOffer = OfferList.closestOffer(user.userBalancePoints) {
            let redeemPopover = RedeemPointsPopoverView(
                point: closestOffer.points
            )
            redeemPopover.redeemButtonObs.subscribe(onNext: { [weak self] _ in
                self?.showRedeemView()
                }).disposed(by: disposebag)
            view.addSubview(redeemPopover)
            redeemPopover.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
        }
    }
    
    func redeemVoucherAPI(offer: OfferList) {
        guard let user = emmysManager.user else { return }
        APICommunicator.redeemPoints(customerID: user.userCode, offerID: "\(offer.id)", points: "\(offer.points)") { [weak self] result in
            switch result {
            case .success(let redeemVoucher):
                guard let self = self, let voucher = redeemVoucher else { return }
                self.showRedeemClaimVoucherView(offer: offer, voucher: voucher)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func showRedeemClaimVoucherView(offer: OfferList, voucher: RedeemVoucher) {
        navigationController?.pushViewController(
            RedeemVoucherCodeController(
                offer: offer,
                voucher: voucher
            ),
            animated: true)
    }
    
    func getUserDetail() {
        guard let id = emmysManager.activeUserID else { return }
        APICommunicator.getUserInfo(customerID: id).done { [weak self] userInfo in
            guard let self = self, let user = userInfo else { return }
            self.setupUserPoints(user: user)
        }.catch { error in }
    }
}

extension RedeemPointsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offerListWithLanguage.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: String(describing: OffersCell.self),
            for: indexPath
        )
        guard let cardCell = cell as? OffersCell else {
            return cell
        }
        
        let offer = offerListWithLanguage[indexPath.row]
        let cardView: RedeemPointsCellView
        if let existingCardView = cardCell.cellView {
            cardView = existingCardView
        } else {
            cardView = RedeemPointsCellView()
            cardCell.cellView = cardView
        }
        cardView.mainLabel.text = "\(offer.levels) = \(offer.points)"
        cardView.infoLabel.text = "(\(offer.details))"
        return cardCell
    }
}
