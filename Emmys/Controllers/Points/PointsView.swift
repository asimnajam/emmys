//
//  PointsView.swift
//  Emmys
//
//  Created by Amir on 2/5/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class PointsView: UIView {
    let pointsView: BorderedView = {
        let view = BorderedView(type: .points)
        return view
    }()
    
    let totalPointsButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle("Total Points", for: .normal)
        button.setTitleColor(.white, for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        backgroundColor = .white
        addSubview(pointsView)
        addSubview(totalPointsButton)
        pointsView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
            make.height.equalTo(240.0)
        }
        totalPointsButton.snp.makeConstraints { make in
            make.top.equalTo(pointsView.snp.bottom).offset(10.0)
            make.leading.equalToSuperview().offset(20.0)
            make.trailing.equalToSuperview().inset(20.0)
            make.height.equalTo(44.0)
        }
    }
}
