//
//  SelectLanguageController.swift
//  Emmys
//
//  Created by Syed Asim Najam on 02/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import RxSwift

class SelectLanguageController: UIViewController {
    enum ViewType {
        case settings
        case registration
    }
    private let contentView = SelectLanguageView(frame: .zero)
    private let type: ViewType
    let languageChangePubSubject = PublishSubject<SupportingLangauges>()
    
    init(type: ViewType) {
        self.type = type
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.englishButton.addTarget(self, action: #selector(selectEnglishLanguage), for: .touchUpInside)
        contentView.arabicButton.addTarget(self, action: #selector(selectArabicLanguage), for: .touchUpInside)
        applyNavigationBarStyle(UIViewController.NavigationBarStyle.transparent)
        if type == .settings {
            setEmmysLogo()
            hideBackButton()
        }
    }
    
    @objc private func selectEnglishLanguage() {
        changeLanguage(language: .english)
    }
    
    @objc private func selectArabicLanguage() {
        changeLanguage(language: .arabic)
    }
    
    private func changeLanguage(language: SupportingLangauges) {
        emmysManager.changeLanguage(language: language)
        languageChangePubSubject.onNext(language)
        switch type {
        case .settings:
            break
//            navigationController?.popViewController(animated: true)
        case .registration:
            navigationController?.pushViewController(
                LoginSignupSelectionController(),
                animated: true
            )
        }
    }
}
