//
//  TableViewContainerCell.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import SnapKit
import UIKit

public final class TableViewContainerCell<View: UIView>: UITableViewCell {

    public var cellView: View? {
        willSet {
            cellView?.removeFromSuperview()
        }
        didSet {
            guard cellView != nil else { return }
            setUpViews()
            layoutIfNeeded()
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        layoutIfNeeded()
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var edges = UIEdgeInsets(
        top: 5,
        left: 0,
        bottom: 5,
        right: 0
    )

    public var isMenuContainer: Bool = false {
        didSet {
            edges = UIEdgeInsets(
                top: 5,
                left: 20,
                bottom: 5,
                right: 20
            )
        }
    }

    private func setUpViews() {
        guard let cellView = cellView else { return }
        contentView.addSubview(cellView)
        cellView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(edges)
        }
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
    }
}

public extension UITableViewCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

public extension UITableView {
    func registerCell<Cell: UITableViewCell>(_ cellClass: Cell.Type) {
        register(
            cellClass,
            forCellReuseIdentifier: cellClass.reuseIdentifier
        )
    }

    func dequeueReusableCell<Cell: UITableViewCell>(forIndexPath indexPath: IndexPath) -> Cell {
        guard let cell = self.dequeueReusableCell(
            withIdentifier: Cell.reuseIdentifier,
            for: indexPath
        ) as? Cell else {
            fatalError("Error for cell id: \(Cell.reuseIdentifier) at \(indexPath))")
        }
        return cell
    }

    func registerByNib<Cell: UITableViewCell>(_ cellClass: Cell.Type) {
        register(
            UINib(nibName: String(describing: Cell.self), bundle: nil),
            forCellReuseIdentifier: String(describing: cellClass)
        )
    }
}
