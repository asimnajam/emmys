//
//  HeaderView.swift
//  Emmys
//
//  Created by Syed Asim Najam on 04/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit


public final class HeaderView: UIView {
    private let containerView: UIView = {
        let view = UIView(frame: .zero)
        return view
    }()
    
    private let logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "emmysLogo")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private let headerLabel: UILabel = {
        let label = UILabel.largeLabel
        return label
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupViews() {
        for view in [logoImageView, headerLabel] {
            containerView.addSubview(view)
        }
        addSubview(containerView)
        backgroundColor = .white//Palette.lightBackground

        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
       
        containerView.layoutMargins = .init(top: 0, left: 0, bottom: 30.0, right: 0)

        logoImageView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.8)
            make.height.equalTo(80)
            make.centerX.equalToSuperview()
            
        }
        
        headerLabel.snp.makeConstraints { make in
            make.top.equalTo(logoImageView.snp.bottom).offset(30.0)
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.bottomMargin.equalToSuperview()
        }
    }

    public func configureWithTitle(title: String) {
        headerLabel.text = title
    }
}
