//
//  VerifyOTPController.swift
//  Emmys
//
//  Created by Syed Asim Najam on 02/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class VerifyOTPController: UIViewController {
    let contentView = VerifyOTPView(frame: .zero)
    private let viewModel: VerifyOTPModel
    private let disposeBag = DisposeBag()
    
    let otpControllerDismissSubject: PublishSubject<(String, ConrtollerType)>
    init(model: VerifyOTPModel, otpControllerDismissSubject: PublishSubject<(String, ConrtollerType)>) {
        self.viewModel = model
        self.otpControllerDismissSubject = otpControllerDismissSubject
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.verifyOTPButton.addTarget(self, action: #selector(verifyOTP), for: .touchUpInside)
        contentView.resnedOTPButton.addTarget(self, action: #selector(resnedOTP), for: .touchUpInside)
        switch viewModel.type {
        case .forgetPassword:
            contentView.setResendOTPView()
        case .registration:
            contentView.setVerifyOTPView()
        }
        hideBackButton()
        bindObservables()
        hideKeyboardWhenTappedAround()
    }
    
    @objc func closeButtonAction() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func verifyOTP(_ sender: UIButton) {
        switch viewModel.type {
        case .forgetPassword:
            proceedToResetPassword()
        case .registration:
            proceedToLogin()
        }
    }
    
    func proceedToLogin() {
        guard let id = viewModel.customerID else { return }
        guard let otp = returnOTP() else {
            return
        }
        APICommunicator.verifyOTP(customerID: id, otp: otp) { [weak self] result in
            switch result {
            case .success(let otp):
                guard let self = self, let otp = otp else { return }
                if otp.isValiedOTP {
                    self.otpControllerDismissSubject.onNext(
                        (
                            self.viewModel.customerID ?? "",
                            .otp(kind: self.viewModel.type)
                        )
                    )
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func proceedToResetPassword() {
        guard let otp = returnOTP() else {
            return
        }
        self.otpControllerDismissSubject.onNext((otp, .otp(kind: self.viewModel.type)))
    }
    
    private func returnOTP() -> String? {
        guard let otp1 = contentView.firstOTP.text, !otp1.isEmpty,
            let otp2 = contentView.secondtOTP.text, !otp2.isEmpty,
            let otp3 = contentView.thirdOTP.text, !otp3.isEmpty,
            let otp4 = contentView.fourthOTP.text, !otp4.isEmpty
        else {
        Toast.show(style: Toast.Style.error(message: "Please Enter OTP"), view: view)
            return nil
        }
        let otp = "\(otp1)\(otp2)\(otp3)\(otp4)"
        return otp
    }
    
    @objc func resnedOTP(_ sender: UIButton) {
        
        
    }

    private func bindObservables() {
        contentView.firstOTP.delegate = self
        contentView.secondtOTP.delegate = self
        contentView.thirdOTP.delegate = self
        contentView.fourthOTP.delegate = self
        
        contentView.firstOTP
            .rx.controlEvent(.editingChanged)
            .subscribe(onNext: { [weak self] _ in
            self?.contentView.secondtOTP.becomeFirstResponder()
        }).disposed(by: disposeBag)
        
        contentView.secondtOTP
            .rx.controlEvent(.editingChanged)
            .subscribe(onNext: { [weak self] _ in
            self?.contentView.thirdOTP.becomeFirstResponder()
        }).disposed(by: disposeBag)
        
        contentView.thirdOTP
            .rx.controlEvent(.editingChanged)
            .subscribe(onNext: { [weak self] _ in
            
            self?.contentView.fourthOTP.becomeFirstResponder()
        }).disposed(by: disposeBag)
        
//        contentView.fourthOTP
//            .rx.controlEvent(.editingChanged)
//            .subscribe(onNext: { [weak self] _ in
//        }).disposed(by: disposeBag)
    }
}

extension VerifyOTPController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        let isValidLength = newString.length <= maxLength
        return isValidLength
    }
}
