//
//  VoucherHistory.swift
//  Emmys
//
//  Created by Asim Najam on 2/21/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation


final class VoucherHistory: Decodable {
    var voucherNumber: String = ""
    var isUsed: Bool = false
    var voucherDate: Date? = Date()
    var points: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case voucherNumber = "VOUCHER_NO"
        case voucherDate = "VOUCHER_DATE"
        case points = "POINT"
        case isUsed = "IS_USED"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        voucherNumber = container.safeDecode(.voucherNumber) ?? ""
        isUsed = container.safeDecode(.isUsed) ?? false
        points = container.safeDecode(.points) ?? 0
        voucherDate = container.safeDecode(.voucherDate, using: Date.Formatters.formattedString) ?? nil
    }
}
