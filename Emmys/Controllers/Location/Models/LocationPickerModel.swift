//
//  LocationPickerModel.swift
//  Emmys
//
//  Created by Asim Najam on 2/13/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import GoogleMaps

final class LocationPickerModel {
    private(set) var branch: BranchList
    
    var coordinate: CLLocationCoordinate2D? {
        guard let url = URL(string: branch.location),
            let coordinatePath = url.pathComponents.first(where: { $0.contains("@") })
            else {
                return nil
        }
        let coordinate = coordinatePath.components(separatedBy: ",").filter { $0.contains(".") }
        
        guard let latitudeString = coordinate.first,
            let latitude = Double(latitudeString.removeCharacters(
                from: "@"
            )),
            let longitudeString = coordinate.last,
            let longitude = Double(longitudeString)
            else {
                return nil
        }
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    init(branch: BranchList) {
        self.branch = branch
    }
}
