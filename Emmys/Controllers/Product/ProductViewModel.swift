//
//  ProductViewModel.swift
//  Emmys
//
//  Created by Asim Najam on 2/13/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation

final class ProductViewModel {
    private var itemList: [Item] = []
    
    init() {
        getItemList()
    }
    private func getItemList(points: String = "0") {
        APICommunicator.getItemList(points: points) { [weak self]  result in
            guard let self = self else { return }
            switch result {
            case .success(let itemList):
                self.itemList = itemList
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    func getItemsByItem(code: String) -> [Item] {
        return itemList.filter { $0.code == code }
    }
}
