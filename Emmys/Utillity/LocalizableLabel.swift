//
//  LocalizableLabel.swift
//  Emmys
//
//  Created by Asim Najam on 2/20/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class LocalizableLabel: UILabel {
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        if emmysManager.currentLanguage == .arabic {
            textAlignment = .right
        } else {
            textAlignment = .left
        }
    }
}

final class AboutUsHeadingLabel: UILabel {
    let border = CALayer()
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        if emmysManager.currentLanguage == .arabic {
            textAlignment = .right
        } else {
            textAlignment = .left
        }
        border.removeFromSuperlayer()
        border.backgroundColor = Colors.blueColor.cgColor
        border.frame = CGRect(
            x: 0,
            y: self.frame.size.height - 1,
            width: self.frame.size.width,
            height: 1
        )
        layer.addSublayer(border)
    }
}
