//
//  SignupView.swift
//  Emmys
//
//  Created by Syed Asim Najam on 02/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import SnapKit

class SignupView: UIView {
//    let closeButton: UIButton = {
//        let button = UIButton()
//        button.setImage(UIImage(named: "closeIcon"), for: .normal)
//        return button
//    }()
    let headerView: HeaderView = {
        let view = HeaderView(frame: .zero)
        view.configureWithTitle(title: LocalizedLanguage.register)
        return view
    }()
    let mandotryLabel: UILabel = {
        let label = LocalizableLabel()
        label.font = UIFont.xSmallFont
        label.text = LocalizedLanguage.non_mandotry
        label.textColor = Colors.blueColor
        return label
    }()
    
    let starImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "star")?.maskWithColor(color: Colors.blueColor))
        imageView.frame = CGRect(x: 0, y: 0, width: 5, height: 5)
        return imageView
    }()
    
    lazy var nonMandotryStackView: UIStackView = {
        let subviews = emmysManager.currentLanguage == .arabic ?
            [mandotryLabel, starImageView] :
            [starImageView, mandotryLabel]
        let stackView = UIStackView(arrangedSubviews: subviews)
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        return stackView
    }()
    
    let checkmarkButton: CheckmarkButton = {
        let button = CheckmarkButton()
        button.isChecked = false
        return button
    }()
    
    let keepLoginLabel: UILabel = {
        let label = UnderLinedLabel()
        label.font = UIFont.xSmallFont
        label.text = LocalizedLanguage.keep_me_login
        label.textColor = Colors.blueColor
        return label
    }()
    
    lazy var checkMarkStackView: UIStackView = {
        let subviews = emmysManager.currentLanguage == .arabic ?
            [keepLoginLabel, checkmarkButton] :
            [checkmarkButton, keepLoginLabel]
        let stackView = UIStackView(arrangedSubviews: subviews)
        stackView.axis = .horizontal
        stackView.distribution = .equalCentering
        return stackView
    }()
    
    let nameTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .name)
        return textField
    }()
    
    let mobileTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .mobile(type: .signup))
        return textField
    }()
    
    let emailTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .email)
        return textField
    }()
    
    let passwordTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .password)
        return textField
    }()
    
    let confirmPasswordTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .confirmPassword)
        return textField
    }()
    
    let registerNowButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle(LocalizedLanguage.register_now, for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        directionalLayoutMargins = .init(top: 0, leading: 50.0, bottom: 0, trailing: 50.0)
        backgroundColor = .white
        add(views: [
            headerView,
//            closeButton,
            nameTextField,
            mobileTextField,
            emailTextField,
            nonMandotryStackView,
            passwordTextField,
            confirmPasswordTextField,
            checkMarkStackView,
            registerNowButton
        ])
//        closeButton.snp.makeConstraints { make in
//            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
//            make.width.height.equalTo(44.0)
//            make.leading.equalToSuperview()
//        }
        headerView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)//.offset(10.0)
            make.leadingMargin.trailingMargin.equalToSuperview()
        }
        
        nameTextField.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom)
            make.height.equalTo(50.0)
        }
        
        mobileTextField.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.height.equalTo(50.0)
            make.top.equalTo(nameTextField.snp.bottom).offset(10)
        }
        
        emailTextField.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.height.equalTo(50.0)
            make.top.equalTo(mobileTextField.snp.bottom).offset(10)
        }
        
        nonMandotryStackView.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()//.offset(10.0)
            make.top.equalTo(emailTextField.snp.bottom).offset(10)
        }
        
        passwordTextField.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.height.equalTo(50.0)
            make.top.equalTo(nonMandotryStackView.snp.bottom).offset(10)
        }
        
        confirmPasswordTextField.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.height.equalTo(50.0)
            make.top.equalTo(passwordTextField.snp.bottom).offset(10)
        }
        
        checkMarkStackView.snp.makeConstraints { make in
            switch emmysManager.currentLanguage {
            case .arabic:
                make.trailingMargin.equalToSuperview()
            case .english:
                make.leadingMargin.equalToSuperview()
            }
            //.offset(10.0)
            make.top.equalTo(confirmPasswordTextField.snp.bottom).offset(10)
        }
        
        registerNowButton.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(checkMarkStackView.snp.bottom).offset(10)
            make.height.equalTo(50.0)
        }
        starImageView.snp.makeConstraints { make in
            make.width.height.equalTo(14.0)
        }
        checkmarkButton.snp.makeConstraints { make in
            make.width.height.equalTo(20.0)
        }
        
        checkmarkButton.layer.borderColor = UIColor.gray.cgColor
        checkmarkButton.layer.borderWidth = 1.0
        
        if emmysManager.currentLanguage == .arabic {
            nonMandotryStackView.setCustomSpacing(10.0, after: mandotryLabel)
            checkMarkStackView.setCustomSpacing(10.0, after: keepLoginLabel)
        } else {
            nonMandotryStackView.setCustomSpacing(10.0, after: starImageView)
            checkMarkStackView.setCustomSpacing(10.0, after: checkmarkButton)
        }
        
//        bringSubviewToFront(closeButton)
    }
    
    func setPlaceHolderText() {
        
    }
    
    
}
