//
//  VoucherHistoryController.swift
//  Emmys
//
//  Created by Asim Najam on 2/21/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class VoucherHistoryController: UIViewController {
    typealias HistoryCell = TableViewContainerCell<VoucherHistoryCellView>
    private let contentView = VoucherHistoryView(frame: .zero)
    private var vouchers: [VoucherHistory] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.tableView.registerCell(HistoryCell.self)
        contentView.tableView.delegate = self
        contentView.tableView.dataSource = self
        getVoucherHistory()
        setEmmysLogo()
    }
    
    func getVoucherHistory() {
        guard let id = emmysManager.activeUserID else { return }
        APICommunicator.voucherHistory(customerID: id) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let voucherHistory):
                self.vouchers = voucherHistory
                self.contentView.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension VoucherHistoryController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vouchers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: String(describing: HistoryCell.self),
            for: indexPath
        )
        guard let cardCell = cell as? HistoryCell else {
            return cell
        }
        
        let voucher = vouchers[indexPath.row]
        
        let cardView: VoucherHistoryCellView
        if let existingCardView = cardCell.cellView {
            cardView = existingCardView
        } else {
            cardView = VoucherHistoryCellView(frame: .zero, kind: .cell)
            cardCell.cellView = cardView
        }
        cardView.configure(with: voucher)
        cardCell.selectionStyle = .none
        return cardCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = VoucherHistoryCellView(frame: .zero, kind: .header)
        headerView.configureVoucherHeader()
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
}

//class VoucherHistoryController: UIViewController {
//
//    typealias HistoryCell = TableViewContainerCell<PointsHistoryCellView>
//    let contentView = VoucherHistoryView(frame: .zero)
//    var vouchers: [VoucherHistory] = []
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        view.addSubview(contentView)
//        contentView.snp.makeConstraints { make in
//            make.edges.equalToSuperview()
//        }
//        contentView.tableView.registerCell(HistoryCell.self)
//        contentView.tableView.delegate = self
//        contentView.tableView.dataSource = self
//        getVoucherHistory()
//    }
//
//    init() {
//        super.init(nibName: nil, bundle: nil)
//    }
//
//    @available(*, unavailable) required init?(coder _: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//}
//
//extension VoucherHistoryController: UITableViewDelegate, UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return vouchers.count
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(
//            withIdentifier: String(describing: HistoryCell.self),
//            for: indexPath
//        )
//        guard let cardCell = cell as? HistoryCell else {
//            return cell
//        }
//        let voucher = vouchers[indexPath.row]
//        let isUsedVoucher = voucher.isUsed
//        let cardView: PointsHistoryCellView
//
//        if let existingCardView = cardCell.cellView {
//            cardView = existingCardView
//        } else {
//            cardView = PointsHistoryCellView(frame: .zero, kind: .cell)
//            cardCell.cellView = cardView
//        }
//        cardView.configureVoucherHistory(
//            date: voucher.voucherDate.formattedString,
//            voucherCode: voucher.voucherNumber,
//            points: "\(voucher.points)",
//            isUsed: isUsedVoucher
//        )
//        cardCell.selectionStyle = .none
//        return cardCell
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 60.0
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = PointsHistoryCellView(frame: .zero, kind: .header)
//        headerView.configureVoucherHeader()
//        return headerView
//    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40.0
//    }
//
//    func getVoucherHistory() {
//        guard let id = emmysManager.customerID else { return }
//        APICommunicator.voucherHistory(customerID: id) { [weak self] result in
//            guard let self = self else { return }
//            switch result {
//            case .success(let voucherHistory):
//                self.vouchers = voucherHistory
//                self.contentView.tableView.reloadData()
//            case .failure(let error):
//                print(error)
//            }
//        }
//    }
//}
