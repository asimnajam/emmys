//
//  Colors.swift
//  Emmys
//
//  Created by Syed Asim Najam on 04/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
    // Emmys Logo Blue Color: rgb(38, 70, 147)
    static let blueColor = UIColor(displayP3Red: CGFloat(38.0/255.0), green: CGFloat(70.0/255.0), blue: CGFloat(147.0/255.0), alpha: 1.0)
    
    static let blueColorDisabled = UIColor(displayP3Red: CGFloat(91.0/255.0), green: CGFloat(116.0/255.0), blue: CGFloat(173.0/255.0), alpha: 1.0)
    static let lightGrey = UIColor(displayP3Red: CGFloat(161.0/255.0), green: CGFloat(172.0/255.0), blue: CGFloat(208.0/255.0), alpha: 1.0)
    static let redColor = UIColor(named: "red")
    static let greenColor = UIColor(named: "green")
}
