//
//  ContainerCollectionView.swift
//  Emmys
//
//  Created by Amir on 2/6/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import UIKit

public class ContainerCollectionViewCell<View: UIView>: UICollectionViewCell {
//    public var disposeBag = DisposeBag()

    public var cellView: View? {
        willSet {
            cellView?.removeFromSuperview()
        }
        didSet {
            guard cellView != nil else { return }
            setupViews()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        layoutIfNeeded()
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupViews() {
        guard let cellView = cellView else { return }
        addSubview(cellView)

        cellView.snp.remakeConstraints { make in
            make.margins.equalToSuperview()
        }
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
//        if let stashable = cellView as? PrepareForReusable {
//            stashable.prepareForReuse()
//        }
//        disposeBag = DisposeBag()
    }

    public override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes)
        -> UICollectionViewLayoutAttributes {
        layoutAttributes.bounds.size.height = systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        return layoutAttributes
    }
}
