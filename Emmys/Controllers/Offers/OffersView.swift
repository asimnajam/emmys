//
//  OffersView.swift
//  Emmys
//
//  Created by Asim Najam on 2/8/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class OffersView: UIView {
    let headerView: HeaderView = {
        let view = HeaderView(frame: .zero)
        view.configureWithTitle(title: "Offers")
        return view
    }()
    let tableView: UITableView = {
         let tableView = UITableView()
         tableView.rowHeight = UITableView.automaticDimension
         tableView.estimatedRowHeight = 60.0
         tableView.separatorInset = .zero
         tableView.separatorStyle = .none
         tableView.backgroundColor = .white
         tableView.tableFooterView = UIView()
         tableView.tableHeaderView = UIView()
         return tableView
     }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        backgroundColor = .white
        addSubview(headerView)
        addSubview(tableView)
        directionalLayoutMargins = .init(top: 0, leading: 50.0, bottom: 0, trailing: 50.0)
        headerView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.leadingMargin.trailingMargin.equalToSuperview()
        }
        
        tableView.snp.makeConstraints { make in
            make.top.equalTo(headerView.snp.bottom)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottom)
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
        }
    }

}
