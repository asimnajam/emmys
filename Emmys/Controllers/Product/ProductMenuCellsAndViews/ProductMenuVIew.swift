//
//  ProductMenuVIew.swift
//  Emmys
//
//  Created by Asim Najam on 2/10/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

final class ProductMenuVIew: UIView {
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.green
        collectionView.showsVerticalScrollIndicator = false
        return collectionView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor  = .red
        add(views: [collectionView])
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
//            make.leading.equalToSuperview().offset(10.0)
//            make.trailing.equalToSuperview().inset(10.0)
            make.width.equalTo(300.0)
            make.bottom.equalToSuperview()
        }
    }
}

final class ProductMenuCollectionView: UIView, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    let contentView = ProductMenuVIew()
    override init(frame: CGRect) {
        super.init(frame: frame)
//        setupView()
        contentView.collectionView.register(
            ProductMenuCell.self,
            forCellWithReuseIdentifier: String(
                describing: ProductMenuCell.self
            )
        )
        contentView.collectionView.delegate = self
        contentView.collectionView.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    typealias ProductMenuCell = ContainerCollectionViewCell<ProductMenuCellView>
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: String(
                describing: ProductMenuCell.self
            ),
            for: indexPath) as? ProductMenuCell
            else {
                return UICollectionViewCell()
        }
        let card: ProductMenuCellView
        if let existing = cell.cellView {
            card = existing
        } else {
            let newCardView = ProductMenuCellView()
            cell.cellView = newCardView
            card = newCardView
        }
        card.menuLabel.text = "100 Points"
        card.backgroundColor = .red
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let dividingVal: Int
        if collectionView.numberOfItems(inSection: 0) > 3 {
            dividingVal = 3
        } else {
            dividingVal = collectionView.numberOfItems(inSection: 0)
        }
        let padding = Int(ProductView.menuCollectionViewInterimSpacing) * (2)
        let collectionViewSize = (Int(collectionView.frame.size.width) - padding) / dividingVal
            //collectionView.numberOfItems(inSection: 0)

        return CGSize(width: collectionViewSize, height: collectionViewSize)
        
    }
}
