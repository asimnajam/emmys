//
//  BillHeaderInfo.swift
//  Emmys
//
//  Created by Asim Najam on 2/20/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation

class ResultSet: Decodable {
    var flag: Int = 0
    var addedOn: Date = Date()
    var branch: String = ""
    var earnedPoints: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case flag = "Flag"
        case addedOn = "ADDED_ON"
        case branch = "BRANCH"
        case earnedPoints = "EARNED_POINTS"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        flag = tryOrNil {
            try container.decode(Int.self, forKey: .flag)
            } ?? 0
        addedOn = container.safeDecode(.addedOn, using: Date.Formatters.formatterISO8601) ?? Date()
        branch = tryOrNil {
            try container.decode(String.self, forKey: .branch)
            } ?? ""
        earnedPoints = tryOrNil {
            try container.decode(Int.self, forKey: .earnedPoints)
            } ?? 0
    }
}

class BillHeaderDetails: Decodable {
    var itemName: String = ""
    var quantity: Int = 0
    var points: Int = 20
    
    enum CodingKeys: String, CodingKey {
        case itemName = "ITEM_NAME"
        case quantity = "QTY"
        case points = "POINTS"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        itemName = tryOrNil {
            try container.decode(String.self, forKey: .itemName)
            } ?? ""
        quantity = tryOrNil {
            try container.decode(Int.self, forKey: .quantity)
            } ?? 0
        points = tryOrNil {
            try container.decode(Int.self, forKey: .points)
            } ?? 0
    }
}

final class BillHeaderInfo: Decodable {
    var resultSet: [ResultSet] = []
    var details: [BillHeaderDetails] = []
    
    enum CodingKeys: String, CodingKey {
        case resultSet = "resultset"
        case details = "Details"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        resultSet = tryOrNil {
            try container.decode([ResultSet].self, forKey: .resultSet)
            } ?? []
        details = tryOrNil {
            try container.decode([BillHeaderDetails].self, forKey: .details)
            } ?? []
    }
}
