//
//  TabBarController.swift
//  Emmys
//
//  Created by Syed Asim Najam on 04/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

final class TabBarController: UITabBarController {
    private let disposeBag = DisposeBag()
    private var selectedMenu: Menu = .myProfile
    private lazy var sideMenuTableViewController = SideMenuTableController(
        tabBar: self
    )
    private var sideMenu: ENSideMenu?
    private let pointsController = PointsHistoryController()
    private let qrReaderController = QRCodeReaderController()
    private let redeemController = RedeemPointsController(type: .redeemClaim)
    private let menuController = ProductController()
    private var navControllers: [UIViewController] {
        let controllers = [
            menuController,
            pointsController,
            qrReaderController,
            redeemController
        ]
        switch emmysManager.currentLanguage {
        case .english:
            return controllers
        case .arabic:
            return controllers.reversed()
        }
    }
    private var isShowWalletView: Bool
    
    init(isShowWalletView: Bool = true) {
        self.isShowWalletView = isShowWalletView
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.barTintColor = Colors.blueColor
        tabBar.isTranslucent = false
        loadTabBar()
        setNavBar()
        initSideMenu()
    }
    
    func initSideMenu() {
        let postion: ENSideMenuPosition = emmysManager.currentLanguage == .arabic ? .right : .left
        sideMenu = ENSideMenu(sourceView: self.view, menuViewController: sideMenuTableViewController, menuPosition: postion)
        sideMenu?.menuWidth = 230.0
        sideMenu?.bouncingEnabled = false
        sideMenu?.animationDuration = 0.2
    }
    
    private func loadTabBar() {
        setTabBarItems()
        let navigationControllers = navControllers.map { UINavigationController(rootViewController: $0) }
        switch emmysManager.currentLanguage {
        case .english:
            selectedIndex = 0
        case .arabic:
            selectedIndex = 3
        }
        viewControllers = navigationControllers
        if isShowWalletView {
            let walletController = WalletController()
            self.setupNavigationbarFor(controller: walletController)
            navigationControllers[selectedIndex].pushViewController(walletController, animated: false)
        }
        setupNavigationBarButtons()
    }
    
    func setupNavigationBarButtons() {
        navControllers.forEach({ controller in
            setupNavigationbarFor(controller: controller)
        })
    }
    
    private func setupNavigationbarFor(controller: UIViewController) {
        let logo = UIImage(named: "emmysLogo")
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40.0, height: 80.0))
        imageView.contentMode = .scaleAspectFit
        imageView.image = logo
        controller.navigationItem.titleView = imageView
        setNavigationBarButtonsWith(controller)
    }
    
    private func setNavigationBarButtonsWith(_ controller: UIViewController) {
        let homeButton = UIButton(type: .custom)
        let homeImage = UIImage(named: "home")?.maskWithColor(color: Colors.blueColor).resizeWith(width: 20.0, height: 20.0)
        homeButton.frame = CGRect(x: 0, y: 0, width: 44.0, height: 44.0)
        homeButton.setImage(homeImage, for: .normal)
        homeButton.addTarget(self, action: #selector(showWalletView), for: .touchUpInside)
        
        let menuButton = UIButton(type: .custom)
               let menuImage = UIImage(named: "hamburger")?.maskWithColor(color: Colors.blueColor).resizeWith(width: 20.0, height: 20.0)
               menuButton.frame = CGRect(x: 0, y: 0, width: 44.0, height: 44.0)
               menuButton.setImage(menuImage, for: .normal)
               menuButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
               
        
        switch emmysManager.currentLanguage {
        case .arabic:
            controller.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: menuButton)
            controller.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: homeButton)
        case .english:
            controller.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
            controller.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: homeButton)
        }
    }
    
    private func setTabBarItems() {
        pointsController.tabBarItem = TabBarMenu.points.tabBarItem
        qrReaderController.tabBarItem = TabBarMenu.scan.tabBarItem
        redeemController.tabBarItem = TabBarMenu.redeem.tabBarItem
        menuController.tabBarItem = TabBarMenu.menu.tabBarItem
    }

    private func setNavBar() {
        let logo = UIImage(named: "emmysLogo")
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40.0, height: 80.0))
        imageView.contentMode = .scaleAspectFit
        imageView.image = logo
        navigationItem.titleView = imageView
        setNavigationBarButtons()
    }
    
    private func setNavigationBarButtons() {
        let menuButton = UIButton(type: .custom)
        let menuImage = UIImage(named: "hamburger")?.maskWithColor(color: Colors.blueColor).resizeWith(width: 20.0, height: 20.0)
        menuButton.frame = CGRect(x: 0, y: 0, width: 44.0, height: 44.0)
        menuButton.setImage(menuImage, for: .normal)
        menuButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
        
        
        let homeButton = UIButton(type: .custom)
        let homeImage = UIImage(named: "home")?.maskWithColor(color: Colors.blueColor).resizeWith(width: 20.0, height: 20.0)
        homeButton.frame = CGRect(x: 0, y: 0, width: 44.0, height: 44.0)
        homeButton.setImage(homeImage, for: .normal)
        homeButton.addTarget(self, action: #selector(showWalletView), for: .touchUpInside)
        
        switch emmysManager.currentLanguage {
        case .arabic:
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: menuButton)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: homeButton)
        case .english:
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: homeButton)
        }
    }
    
    private func hideNavigationButtons() {
        navigationItem.rightBarButtonItem = nil
        navigationItem.leftBarButtonItem = nil
    }
    
    @objc private func showWalletView() {
        let walletController = WalletController()
        walletController.modalPresentationStyle = .overCurrentContext
        push(walletController)
    }
    
    @objc private func showMenu() {
        sideMenu?.toggleMenu()
    }
       
    private func logout() {
        do {
            let realm = try Realm()
            let userInfo = realm.objects(UserInfo.self)
            let branches = realm.objects(BranchList.self)
            let offers = realm.objects(OfferList.self)
            let user = realm.objects(User.self)
            
            try realm.write {
                realm.delete(userInfo)
                realm.delete(branches)
                realm.delete(offers)
                realm.delete(user)
            }
        } catch {
            print("Realm Error on Logout: \(error)")
        }
        let controller = MainController()
        let navController = UINavigationController(rootViewController: controller)
        if let window = UIApplication.shared.keyWindow {
            navController.view.frame = window.bounds
            UIView.transition(with: window, duration: 0.5, options: [.allowAnimatedContent, .transitionFlipFromLeft], animations: {
                window.rootViewController = navController
            }, completion: nil)
        }
    }
    
    func dismissSideMenu(menu: Menu) {
        DispatchQueue.main.async {
            self.decideController(menu: menu)
        }
    }
    
    func decideController(menu: Menu) {
        sideMenu?.toggleMenu()
        switch menu {
        case .dealsnOffer:
            let controller = ProductController()
            controller.modalPresentationStyle = .overCurrentContext
            self.push(controller)
        case .languages:
            let controller = SelectLanguageController(type: .settings)
            controller.modalPresentationStyle = .overCurrentContext
            controller.languageChangePubSubject
                .subscribe(onNext: { [weak self] language in
                    guard let self = self else { return }
                    self.languageChangeAction()
                })
                .disposed(by: self.disposeBag)
            self.push(controller)
        case .logout:
            self.logout()
        case.contactUs:
            let controller = ContactUsController()
            controller.modalPresentationStyle = .overCurrentContext
            self.push(controller)
        case .myProfile:
            let controller = UserProfileController()
            controller.modalPresentationStyle = .overCurrentContext
            self.push(controller)
        case .rewardSystem:
            let controller = RedeemPointsController(
                type: RedeemPointsView.ViewType.redeemInfo
            )
            controller.modalPresentationStyle = .overCurrentContext
            self.push(controller)
        case .location:
            let controller = LocationController()
            controller.modalPresentationStyle = .overCurrentContext
            self.push(controller)
            
        case .voucherHistory:
            let controller = VoucherHistoryController()
            controller.modalPresentationStyle = .overCurrentContext
            self.push(controller)
        case .aboutUs:
            let controller = AboutUsController()
            controller.modalPresentationStyle = .overCurrentContext
            self.push(controller)
        }
    }
    private func push(_ controller: UIViewController) {
        if !(controller is SelectLanguageController) {
            setupNavigationbarFor(controller: controller)
        }
        showController(controller)
    }
    
    private func showController(_ controller: UIViewController) {
        guard let selectedController = navControllers.get(selectedIndex) else { return }
        selectedController.navigationController?.pushViewController(controller, animated:true)
    }
    
    private func languageChangeAction() {
        isShowWalletView = true
        loadTabBar()
        self.initSideMenu()
        self.sideMenuTableViewController.contentView.tableView.reloadData()
    }
}
