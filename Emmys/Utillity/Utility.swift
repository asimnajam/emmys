//
//  Utility.swift
//  Emmys
//
//  Created by Syed Asim Najam on 31/01/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import UIKit

public func tryOrNil<T>(f: () throws -> T?) -> T? {
    do {
        return try f()
    } catch {
        print(error)
        return nil
    }
}

extension UIView {
    public func add(views: [UIView]) {
        for view in views {
            addSubview(view)
        }
    }
}

extension UITextField {
    public static func setupKeyboardHack(window: UIWindow?) {
        UITextField.appearance().keyboardAppearance = .dark
        let lagFreeField: UITextField = UITextField()
        window?.addSubview(lagFreeField)
        lagFreeField.becomeFirstResponder()
        lagFreeField.resignFirstResponder()
        lagFreeField.removeFromSuperview()
    }
}

extension UIFont {
    static var xSmallFont: UIFont {
        return UIFont.systemFont(ofSize: 12.0)
    }
    
    static var smallBoldFont: UIFont {
        return UIFont.systemFont(ofSize: 14.0, weight: .bold)
    }
    
    static var smallFont: UIFont {
        return UIFont.systemFont(ofSize: 14.0)
    }
    
    static var mediumFont: UIFont {
        return UIFont.systemFont(ofSize: 18.0)
    }
    
    static var mediumBoldFont: UIFont {
        return UIFont.systemFont(ofSize: 18.0, weight: .bold)
    }
    
    static var largeFont: UIFont {
        return UIFont.systemFont(ofSize: 26.0)
    }
    
    static var xlargeFont: UIFont {
        return UIFont.systemFont(ofSize: 32.0)
    }
}

extension UIImage {
    public func maskWithColor(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()!
        color.setFill()
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        let rect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        context.draw(cgImage!, in: rect)
        context.setBlendMode(CGBlendMode.sourceIn)
        context.addRect(rect)
        context.drawPath(using: CGPathDrawingMode.fill)
        let coloredImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return coloredImage!
    }
}

public extension String {
    var replacedArabicDigitsWithEnglish: String {
        var str = self
        let map = ["٠": "0",
                   "١": "1",
                   "٢": "2",
                   "٣": "3",
                   "٤": "4",
                   "٥": "5",
                   "٦": "6",
                   "٧": "7",
                   "٨": "8",
                   "٩": "9"]
        map.forEach { str = str.replacingOccurrences(of: $0, with: $1) }
        return str
    }
}

extension Optional {
    public var exists: Bool {
        switch self {
        case .none:
            return false
        case .some:
            return true
        }
    }

    public var doesNotExist: Bool {
        return !exists
    }
}


class ButtonWithImage: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
            titleEdgeInsets = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        }
    }
}

extension String {

    func removeCharacters(from forbiddenChars: CharacterSet) -> String {
        let passed = self.unicodeScalars.filter { !forbiddenChars.contains($0) }
        return String(String.UnicodeScalarView(passed))
    }

    func removeCharacters(from: String) -> String {
        return removeCharacters(from: CharacterSet(charactersIn: from))
    }
    
    func removeWhiteSpaces() -> String {
        return self.trimmingCharacters(in: .whitespaces)
//        string.trimmingCharacters(in: .whitespaces)
    }
}



extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension Array where Element : Hashable {
    var unique: [Element] {
        return Array(Set(self))
    }
}

extension UIButton {
    func alignImageAndTitleVertically() {
        let spacing: CGFloat = 20.0
        let imageSize = imageView!.frame.size
        titleEdgeInsets = UIEdgeInsets(top: (imageSize.height + spacing), left: -imageSize.width, bottom: 0, right: 0)
        imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
    }
}

extension Array where Element: OfferList {
    func closestOfferBy(point: Int) {
        
    }
}

extension UIView {
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = Colors.blueColor.cgColor
        layer.shadowOpacity = 0.6
        layer.shadowRadius = 8
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        layer.shadowOffset = CGSize(width: 0, height: 5)
    }
}

extension String {
    var isValid: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    
    var isValidMobileNumber: Bool {
        let phoneRegex = "^(00966|966|009660|9660)[0-9]{9}$"
        let phoneRegex2 = "^(05)[0-8]{8}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        if !phoneTest.evaluate(with: self) {
            let newPhoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex2)
            return newPhoneTest.evaluate(with: self)
        } else {
            return true
        }
    }
    
    var isValidPassword: Bool {
        self.count >= 6
    }
}

extension UIViewController {
    func setEmmysLogo() {
        let logo = UIImage(named: "emmysLogo")
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40.0, height: 80.0))
        imageView.contentMode = .scaleAspectFit
        imageView.image = logo
        navigationItem.titleView = imageView
    }
}

extension UIImage {
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

extension UIImage {
    public func resizeWith(width: CGFloat, height: CGFloat = 0.0) -> UIImage? {
        let imageView = UIImageView(
            frame: CGRect(
                origin: .zero,
                size: CGSize(
                    width: width,
                    height: height > 0.0 ? height : CGFloat(ceil(width / size.width * size.height)
                    )
                )
            )
        )
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result.withRenderingMode(renderingMode)
    }
}

extension UIViewController {
    var topbarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
    
    func add(_ child: UIViewController) {
        let topSafeArea: CGFloat
        var bottomSafeArea = self.navigationController?.navigationBar.frame.height ?? 0.0
        addChild(child)
        view.addSubview(child.view)
        print(view.bounds)
        let guide = view.safeAreaLayoutGuide
        let height = guide.layoutFrame.size.height
        
        
        if #available(iOS 11.0, *) {
            topSafeArea = view.safeAreaInsets.top
//            bottomSafeArea = view.safeAreaInsets.bottom
        } else {
            topSafeArea = topLayoutGuide.length
//            bottomSafeArea = bottomLayoutGuide.length
        }
        
        child.view.frame = CGRect(x: 0, y: topSafeArea, width: view.bounds.width, height: height - bottomSafeArea)
        child.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        child.didMove(toParent: self)
    }
    
//    viewController.view.frame = view.bounds
//    viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    
//    addChildViewController(viewController)
//
//    // Add Child View as Subview
//    view.addSubview(viewController.view)
//
//    // Configure Child View
//    viewController.view.frame = view.bounds
//    viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//
//    // Notify Child View Controller
//    viewController.didMove(toParentViewController: self)
//
//
    func remove() {
        // Just to be safe, we check that this view controller
        // is actually added to a parent before removing it.
        guard parent != nil else {
            return
        }

        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
}

extension Array {
    func get(_ index: Int) -> Element? {
        if index >= 0, index < count {
            return self[index]
        } else {
            return nil
        }
    }
}

extension Int {
    func roundToNearest(multiple: Int) -> Int {
        let remainder = abs(self) % multiple
        if remainder == 0 {
            return self
        } else {
            return self - remainder
        }
    }
}
