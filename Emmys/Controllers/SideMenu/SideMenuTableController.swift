//
//  SideMenuTableController.swift
//  Emmys
//
//  Created by Asim Najam on 2/8/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import RxSwift

enum Menu: Int, CaseIterable {
    case myProfile
    case rewardSystem
    case dealsnOffer
    case voucherHistory
    case location
    case contactUs
    case aboutUs
    case languages
    case logout
    
    var title: String {
        switch self {
        case .languages:
            return LocalizedLanguage.language
        case .logout:
            return LocalizedLanguage.logout
        case .location:
            return LocalizedLanguage.location
        case .myProfile:
            return LocalizedLanguage.my_profile
        case .dealsnOffer:
            return LocalizedLanguage.deals_and_offers
        case .contactUs:
            return LocalizedLanguage.contact_us
        case .rewardSystem:
            return LocalizedLanguage.reward_system
        case .voucherHistory:
            return LocalizedLanguage.voucherHistory
        case .aboutUs:
            return LocalizedLanguage.aboutUs
        }
    }
    
    var image: UIImage? {
        let image: UIImage?
        switch self {
        case .languages:
            image = UIImage(named: "language2")
        case .logout:
            image = UIImage(named: "logout")
        case .location:
            image = UIImage(named: "location2")
        case .myProfile:
            image = UIImage(named: "my_profile")
        case .dealsnOffer:
            image = UIImage(named: "deal")
        case .contactUs:
            image = UIImage(named: "contact")
        case .rewardSystem:
            image = UIImage(named: "reward")
        case .voucherHistory:
            image = UIImage(named: "voucherHistory")
        case .aboutUs:
            image = UIImage(named: "aboutUs")
        }
        return image?.maskWithColor(color: Colors.blueColor)
    }
}

class SideMenuTableController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    typealias OffersCell = TableViewContainerCell<SideMenuCellView>
    let contentView = SideMenuView(frame: .zero)
    let tabBar: TabBarController
    let disposeBag = DisposeBag()
    
    init(tabBar: TabBarController) {
        self.tabBar = tabBar
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.tableView.registerCell(OffersCell.self)
        contentView.tableView.delegate = self
        contentView.tableView.dataSource = self
        applyNavigationBarStyle(.transparent)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Menu.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: String(describing: OffersCell.self),
            for: indexPath
        )
        cell.selectionStyle = .none
        guard let cardCell = cell as? OffersCell else {
            return cell
        }
        
        let cardView: SideMenuCellView
        cardView = SideMenuCellView()
        cardCell.cellView = cardView
        let menu = Menu(rawValue: indexPath.row)
        cardView.menuLabel.text = menu?.title
        cardView.menuImage.image = menu?.image
        return cardCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menuItem = Menu(rawValue: indexPath.row) {
            self.tabBar.dismissSideMenu(menu: menuItem)
        }
    }
}

