//
//  CloseButton.swift
//  Emmys
//
//  Created by Asim Najam on 2/15/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

final class CloseButton: UIButton {
    static let button: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "closeIcon"), for: .normal)
        return button
    }()
}
