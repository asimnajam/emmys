//
//  BorderedView.swift
//  Emmys
//
//  Created by Amir on 2/5/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

final class BorderedView: UIView {
    enum ViewType {
        case points
        case totalPoints
        case normal
    }
    
    let topLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Colors.blueColor
        label.font = UIFont.mediumFont
        label.numberOfLines = 2
        label.text = ""
        return label
    }()
    
    let mainIntroLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Colors.blueColor
        label.font = UIFont.xlargeFont
        label.numberOfLines = 2
        label.text = ""
        return label
    }()
    
    let button: UIButton = {
        let button = UIButton()
        button.setTitle("Detail", for: .normal)
        button.setTitleColor(Colors.blueColor, for: .normal)
        return button
    }()
    
    init(type: ViewType) {
        super.init(frame: .zero)
        configure(type: type)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        backgroundColor = .white
        layer.borderColor = Colors.blueColor.cgColor
        layer.borderWidth = 2.0
    }
    func configure(type: ViewType) {
        switch type {
        case .points:
            setupPointsView()
        case .totalPoints:
            setupTotalPointsView()
        case .normal:
            setupNormalView()
        }
    }
    
    func setupPointsView() {
        add(views: [topLabel, mainIntroLabel, button])
        mainIntroLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
        }
        topLabel.snp.makeConstraints { make in
            make.bottom.equalTo(mainIntroLabel.snp.top)//.offset(-20.0)
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
        }
        
        button.snp.makeConstraints { make in
            make.top.equalTo(mainIntroLabel.snp.bottom).offset(10.0)
            make.leading.equalToSuperview().offset(20.0)
            make.trailing.equalToSuperview().inset(20.0)
            make.height.equalTo(44.0)
        }
        button.layer.borderColor = Colors.blueColor.cgColor
        button.layer.borderWidth = 2.0
        layer.borderColor = Colors.blueColor.cgColor
        layer.borderWidth = 2.0
    }
    
    func setupTotalPointsView() {
        add(views: [mainIntroLabel])
        mainIntroLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.width.equalTo(120.0)
        }
    }
    
    func setupNormalView() {
        add(views: [mainIntroLabel])
        mainIntroLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().offset(10.0)
        }
    }
}

