//
//  UserProfileView.swift
//  Emmys
//
//  Created by Asim Najam on 2/9/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class UserProfileView: UIView {
    let userImageButton: UIButton = {
        let button = UIButton(type: .custom)
        button.layer.borderColor = Colors.blueColor.cgColor
        button.layer.borderWidth = 1.0
        button.imageView?.contentMode = .scaleAspectFit
        button.setBackgroundImage(UIImage(named: "userImage"), for: .normal)
        return button
    }()
    
    let myAccountLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Colors.blueColor
        label.font = UIFont.xlargeFont
        label.numberOfLines = 2
        label.text = LocalizedLanguage.my_profile
        return label
    }()
    
    let nameTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .name, type: .filled)
        return textField
    }()
    
    let mobileTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .mobile(type: .signup), type: .filled)
        return textField
    }()
    
    let emailTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .email, type: .filled)
        return textField
    }()
    
    let submitButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle(LocalizedLanguage.update_profile, for: .normal)
        return button
    }()
    
   override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 100.0, leading: 20.0, bottom: 20.0, trailing: 20.0)
        let customSpacing: CGFloat = 10.0
        let stackView = UIStackView(arrangedSubviews: [
            myAccountLabel,
            nameTextField,
            mobileTextField,
            emailTextField,
            submitButton
        ])
        
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        add(views: [userImageButton, stackView])
        
        userImageButton.snp.makeConstraints { make in
            make.width.equalToSuperview().multipliedBy(0.5)
            make.height.equalTo(userImageButton.snp.width)
            make.centerX.equalToSuperview()
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
        }
        stackView.snp.makeConstraints { make in
            make.top.equalTo(userImageButton.snp.bottom).offset(10.0)
            make.height.equalTo(200.0 + 40.0)
            make.leadingMargin.trailingMargin.equalToSuperview()
//            make.centerY.equalToSuperview()
        }
        for view in [
            myAccountLabel,
            nameTextField,
            mobileTextField,
            emailTextField
            ] {
                stackView.setCustomSpacing(customSpacing, after: view)
        }
    }

}
