//
//  UnderLinedButton.swift
//  Emmys
//
//  Created by Asim Najam on 3/3/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

final class UnderLinedButton: UIButton {
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        let border = CALayer()
        border.backgroundColor = UIColor.gray.cgColor
        border.frame = CGRect(
            x: 0,
            y: self.frame.size.height - 1,
            width: self.frame.size.width,
            height: 1
        )
        layer.addSublayer(border)
    }
}
