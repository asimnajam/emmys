//
//  OffersCellView.swift
//  Emmys
//
//  Created by Asim Najam on 2/8/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class OffersCellView: UIView {
    let productImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "camera")
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = Colors.blueColor
        return imageView
    }()
    
    let productInfoLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = UIFont.mediumFont
        label.numberOfLines = 0
        label.text = "Your 500 points Bronze voucher is ready to collect from any branch"
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor  = Colors.blueColor
        add(views: [productImage, productInfoLabel])
        productImage.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10.0)
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
            make.bottom.lessThanOrEqualTo(productInfoLabel.snp.top).offset(-20.0).priority(750)
        }
        productInfoLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
            make.bottom.equalToSuperview().inset(10.0)
        }
        layer.cornerRadius = 10.0
        layer.masksToBounds = true
    }
}
