//
//  LocationPickerView.swift
//  Emmys
//
//  Created by Asim Najam on 2/13/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class LocationPickerView: UIView {
    let headerView: HeaderView = {
        let view = HeaderView(frame: .zero)
        view.configureWithTitle(title: "Location")
        return view
    }()
    
    let mapView: GMSMapView = {
        let view = GMSMapView()
        view.mapType = GMSMapViewType.normal
        view.accessibilityElementsHidden = false
        view.isMyLocationEnabled = true
        view.layer.borderColor = Colors.blueColor.cgColor
        view.layer.borderWidth = 1.0
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 0, leading: 50.0, bottom: 0, trailing: 50.0)
        add(views: [headerView, mapView])
        headerView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.leadingMargin.trailingMargin.equalToSuperview()
        }
        mapView.snp.makeConstraints { make in
            make.bottomMargin.trailingMargin.leadingMargin.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom)
        }
        
        
    }

}
