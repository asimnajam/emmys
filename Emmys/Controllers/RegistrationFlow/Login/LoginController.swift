//
//  LoginController.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import RxSwift

class LoginController: UIViewController {
    private let contentView = LoginView(frame: .zero)
    private let loginControllerDismissSubject: PublishSubject<(String, ConrtollerType)>
    private let authModel: AuthModel
    let disposeBag = DisposeBag()
    
    init(loginControllerDismissSubject: PublishSubject<(String, ConrtollerType)>, authModel: AuthModel) {
        self.authModel = authModel
        self.loginControllerDismissSubject = loginControllerDismissSubject
        super.init(nibName: nil, bundle: nil)
        
        authModel.loginAPIPubSubject.asObservable().subscribe(onNext: { [weak self] (error, isLoginSuccessfully) in
            guard let self = self else { return }
            if !isLoginSuccessfully {
                self.view.hideLoader()
                Toast.show(style: Toast.Style.error(message: error), view: self.view)
            }
            }).disposed(by: disposeBag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.loginButton.addTarget(self, action: #selector(loginButtonAction), for: .touchUpInside)
        contentView.forgetPasswordButton.addTarget(self, action: #selector(forgetPassword), for: .touchUpInside)
        view.layoutIfNeeded()
        hideKeyboardWhenTappedAround()
    }
    
    @objc func closeButtonAction() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func loginButtonAction(_ sender: UIButton) {
//        authModel.login(mobileNumber: "966444555666", password: "asdasd")
//        return
        guard let mobile = contentView.mobileTextField.text, !mobile.isEmpty
            else {
                Toast.show(style: Toast.Style.error(message: "Please enter your mobile number"), view: view)
                return
        }

        guard mobile.isValidMobileNumber else {
            Toast.show(style: Toast.Style.error(message: "Please enter your valid mobile number"), view: view)
            return
        }

        guard let password = contentView.passwordTextField.text, !password.isEmpty
            else {
                Toast.show(style: Toast.Style.error(message: "Please enter your password"), view: view)
                return
        }

        guard password.isValidPassword else {
            Toast.show(style: Toast.Style.error(message: "Password length should be minimum 6 charactors"), view: view)
            return
        }

        view.showLoader()
        authModel.login(mobileNumber: mobile, password: password)
    }
    
    @objc func forgetPassword(_ sender: UIButton) {
        self.loginControllerDismissSubject.onNext(("", .login))
    }
}
