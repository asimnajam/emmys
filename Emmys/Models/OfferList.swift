//
//  OfferList.swift
//  Emmys
//
//  Created by Syed Asim Najam on 31/01/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import RealmSwift

final class OfferList: Object, Decodable {
    @objc dynamic var flag: Int = 0
    @objc dynamic var id: Int = 0
    @objc dynamic var levels: String = ""
    @objc dynamic var points: Int = 0
    @objc dynamic var details: String = ""
    
    init(flag: Int, id: Int, levels: String, points: Int, details: String) {
        self.flag = flag
        self.levels = levels
        self.points = points
        self.details = details
    }
    
    enum CodingKeys: String, CodingKey {
        case flag = "FLAG"
        case id = "ID"
        case levels = "LEVELS"
        case points = "POINTS"
        case details = "DETAILS"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        flag = tryOrNil {
            try container.decode(Int.self, forKey: .flag)
            } ?? 0
        id = tryOrNil {
            try container.decode(Int.self, forKey: .id)
            } ?? 0
        levels = tryOrNil {
            try container.decode(String.self, forKey: .levels)
            } ?? ""
        points = tryOrNil {
            try container.decode(Int.self, forKey: .points)
            } ?? 0
        details = tryOrNil {
            try container.decode(String.self, forKey: .details)
            } ?? ""
    }
    
    public required init() {}
    
    static var getList: [OfferList] {
        let realmDB = Database.realmDb()
        let branches = realmDB.objects(OfferList.self)
        return Array(branches)
    }
    
    static func save(offerList: [OfferList]) {
        let realmDB = Database.realmDb()
        realmDB.write(onError: Database.funcAndLine) {
            realmDB.add(offerList)
        }
    }
    
    static func closestOffer(_ userBalancePoints: Int) -> OfferList? {
        var closestOffer: OfferList? = nil
        for offer in getList {
            if userBalancePoints >= offer.points {
                closestOffer = offer
            }
        }
        return closestOffer
    }
}
