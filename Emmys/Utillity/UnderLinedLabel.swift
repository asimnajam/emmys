//
//  UnderLinedLabel.swift
//  Emmys
//
//  Created by Asim Najam on 3/3/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

final class UnderLinedLabel: LocalizableLabel {
    let border = CALayer()
    
    override init() {
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        setup()
    }
    
    private func setup() {
        border.removeFromSuperlayer()
        border.backgroundColor = UIColor.gray.cgColor
        border.frame = CGRect(
            x: 0,
            y: self.frame.size.height - 1,
            width: self.frame.size.width,
            height: 1
        )
        layer.addSublayer(border)
    }
}
