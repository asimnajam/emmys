//
//  UIImageViewExtension.swift
//  Emmys
//
//  Created by Asim Najam on 2/12/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import UIKit

final class CustomImageView: UIImageView {
    var imageCache: [String: UIImage?] = [:]
    var imageID: String = ""
//    let acitivityIndicator = UIActivityIndicatorView(style: .gray)
    
    
    func loadImageFromURL(itemID: String) {
//        acitivityIndicator.hidesWhenStopped = true
//        acitivityIndicator.frame = CGRect(x: frame.width / 2, y: frame.height / 2, width: 20.0, height: 20.0)
//        acitivityIndicator.startAnimating()
////        addSubview(acitivityIndicator)
        self.imageID = itemID
        if let imageFromCache = imageCache[itemID] {
            self.image = imageFromCache
//            acitivityIndicator.stopAnimating()
            return
        }
        APICommunicator.getItemImage(itemID) { [weak self] result in
            switch result {
            case .success(let imageData):
                DispatchQueue.main.async {
                    let imageToCache = UIImage(data: imageData)
                    if self?.imageID == itemID {
                        self?.imageCache[itemID] = imageToCache
                    }
                    self?.image = imageToCache
//                    self?.acitivityIndicator.stopAnimating()
                }
            case .failure(_):
                DispatchQueue.main.async {
                    let imageToCache = UIImage(color: .lightGray)
                    if self?.imageID == itemID {
                        self?.imageCache[itemID] = imageToCache
                    }
                    self?.image = imageToCache
//                    self?.acitivityIndicator.stopAnimating()
                }
            }
        }
    }
}
