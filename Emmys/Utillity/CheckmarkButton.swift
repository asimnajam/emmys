//
//  CheckmarkButton.swift
//  Emmys
//
//  Created by Asim Najam on 2/21/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

public final class CheckmarkButton: UIButton {
   
    var selectedImage: UIImage? {
        UIImage(named: "checkmark")?.maskWithColor(color: Colors.blueColor)
    }

    var unselectedImage: UIImage? {
        UIImage(named: "checkmark")?.maskWithColor(color: .white)
    }

    public init() {
        super.init(frame: CGRect.zero)
        imageView?.contentMode = .scaleAspectFill
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public var isChecked: Bool = false {
        didSet {
            switch isChecked {
            case true:
                setImage(
                    selectedImage,
                    for: UIControl.State()
                )
            case false:
                setImage(
                    unselectedImage,
                    for: UIControl.State()
                )
            }
        }
    }

    var removeImage: Bool = false {
        didSet {
            setBackgroundImage(nil, for: UIControl.State())
        }
    }

    func getIsCheck() -> Bool {
        return isChecked
    }
}
