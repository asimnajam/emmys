//
//  ResetPasswordController.swift
//  Emmys
//
//  Created by Asim Najam on 2/16/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import RxSwift

class ResetPasswordController: UIViewController {
    private let contentView = ResetPasswordView(frame: .zero)
    let controllerDismissSubject: PublishSubject<(String, ConrtollerType)>
    let mobileNumber: String
    let otp: String
    
    init(mobileNumber: String, otp: String, controllerDismissSubject: PublishSubject<(String, ConrtollerType)>) {
        self.mobileNumber = mobileNumber
        self.otp = otp
        self.controllerDismissSubject = controllerDismissSubject
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.submitButton.addTarget(self, action: #selector(submitButtonAction), for: .touchUpInside)
//        contentView.closeButton.addTarget(self, action: #selector(closeButtonAction), for: .touchUpInside)
    }
    
    @objc func closeButtonAction() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func submitButtonAction() {
        guard let password = contentView.passwordTextField.text, !password.isEmpty
            else {
                Toast.show(
                    style: .error(message: "Please Enter Password!"),
                    view: self.view
                )
                return
        }

        guard let confirmPassword = contentView.confirmPasswordTextField.text, !confirmPassword.isEmpty
            else {
                Toast.show(
                    style: .error(message: "Please Enter Confirm Password!"),
                    view: self.view
                )
                return
        }

        guard password == confirmPassword else {
            Toast.show(
                style: .error(message: "Password and Confirm password should be same!"),
                view: self.view
            )
            return
        }

        APICommunicator.resetPassword(mobile: mobileNumber, password: password, otp: otp) { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .success(let resetPassword):
                guard let resetPassword = resetPassword
                    else { return }
                if resetPassword.isUpdated {
                    Toast.show(style: .info(message: "Please Login to Continue"), view: self.view)
                    Toast.show(style: .info(message: "Please Login to Continue"), view: self.view) {
                        DispatchQueue.main.async {
                            self.controllerDismissSubject.onNext(("", .resetPassword))
                        }
                    }
                }
            case .failure(_):
                Toast.show(style: .error(message: "Error Updating password"), view: self.view)
            }
        }
//            self.controllerDismissSubject.onNext(("", .resetPassword))
    }

}
