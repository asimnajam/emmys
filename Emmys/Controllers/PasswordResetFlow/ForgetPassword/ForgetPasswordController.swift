//
//  ForgetPasswordController.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import  RxSwift

class ForgetPasswordController: UIViewController {
    let contentView = ForgetPasswordView(frame: .zero)
    let forgetPasswordControllerDismissSubject: PublishSubject<(String, ConrtollerType)>
    
    init(forgetPasswordControllerDismissSubject: PublishSubject<(String, ConrtollerType)>) {
        self.forgetPasswordControllerDismissSubject = forgetPasswordControllerDismissSubject
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.submitButton.addTarget(self, action: #selector(submitButtonAction), for: .touchUpInside)
//         contentView.closeButton.addTarget(self, action: #selector(closeButtonAction), for: .touchUpInside)
        view.layoutIfNeeded()
    }
    
    @objc func closeButtonAction() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func submitButtonAction() {
        callResendOTPAPI()
        return
    }

    func callResendOTPAPI() {
        guard let mobileNumber = contentView.mobileTextField.text, !mobileNumber.isEmpty else {
            Toast.show(style: .error(message: "Please Enter Your Mobile Number!"), view: self.view)
            return
        }
        APICommunicator.resendOTP(mobile: mobileNumber) { result in
            switch result {
            case .success(let otp):
                guard let otp = otp else { return }
                if otp.isOTPReSent {
                    
                    self.forgetPasswordControllerDismissSubject.onNext(
                        (mobileNumber, .forgetPassword)
                    )
                    
                }
            case .failure(_):
                Toast.show(style: .error(message: "Error Resending OTP"), view: self.view)
            }
        }
        
//            self.forgetPasswordControllerDismissSubject.onNext(
//                ("034512345", .forgetPassword)
//            )
    }
}
