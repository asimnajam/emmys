//
//  QRCodeReaderView.swift
//  Emmys
//
//  Created by Amir on 2/5/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class QRCodeReaderView: UIView {
    let qrCodeScannerView: QRReaderView = {
        let view = QRReaderView(frame: .zero)
        return view
    }()
    
    let qrCodeScannerFrameView: UIView = {
        let view = UIView()
        return view
    }()
    
    let cameraImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "camera")?.maskWithColor(color: Colors.blueColor)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let scanBarCodeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Colors.blueColor
        label.font = UIFont.mediumFont
        label.numberOfLines = 2
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        add(views: [qrCodeScannerFrameView, cameraImage, scanBarCodeLabel])
        qrCodeScannerFrameView.addSubview(qrCodeScannerView)
        qrCodeScannerView.snp.makeConstraints { make in
//            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(30.0)
            make.center.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.9)
            make.height.equalTo(qrCodeScannerView.snp.width)
        }
        qrCodeScannerFrameView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(30.0)
            make.centerX.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.9)
            make.height.equalTo(qrCodeScannerFrameView.snp.width)
        }
        cameraImage.snp.makeConstraints { make in
            make.top.equalTo(qrCodeScannerFrameView.snp.bottom).offset(10.0)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(40.0)
        }
        scanBarCodeLabel.snp.makeConstraints { make in
            make.top.equalTo(cameraImage.snp.bottom).offset(10.0)
            make.centerX.equalToSuperview()
//            make.width.height.equalTo(40.0)
        }
        qrCodeScannerFrameView.layer.borderColor = Colors.blueColor.cgColor
        qrCodeScannerFrameView.layer.borderWidth = 2.0
    }
}
