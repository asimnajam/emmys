//
//  AppDelegate.swift
//  Emmys
//
//  Created by Syed Asim Najam on 31/01/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import RealmSwift
import IQKeyboardManager

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
//    let currentLanguage = "ar"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        decideRootViewPresentation()
        UITextField.setupKeyboardHack(window: window)
        
        GMSPlacesClient.provideAPIKey("AIzaSyBD1ZWcO_PXp0BPGVjZlyMwGv37vcjzeyM")
        GMSServices.provideAPIKey("AIzaSyBD1ZWcO_PXp0BPGVjZlyMwGv37vcjzeyM")
        showRealmDefaultConfig()
//        LanguageManager.shared.defaultLanguage = .en
        IQKeyboardManager.shared().isEnabled = true
        
//        UserDefaults.standard.set(emmysManager.currentLanguage.languageCode, forKey: "AppleLanguage")
        Bundle.swizzleLocalization()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func decideRootViewPresentation() {
        let controller: UIViewController
        if emmysManager.isAuthenticated {
            controller = TabBarController(isShowWalletView: false)
        } else {
            controller = UINavigationController(rootViewController: MainController())
        }
        let navController = controller
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }

    func showRealmDefaultConfig() {
        print(Realm.Configuration.defaultConfiguration.fileURL as Any)
    }
}

extension Bundle {
    static func swizzleLocalization() {
        let orginalSelector = #selector(localizedString(forKey:value:table:))
        guard let orginalMethod = class_getInstanceMethod(self, orginalSelector) else { return }

        let mySelector = #selector(myLocaLizedString(forKey:value:table:))
        guard let myMethod = class_getInstanceMethod(self, mySelector) else { return }

        if class_addMethod(self, orginalSelector, method_getImplementation(myMethod), method_getTypeEncoding(myMethod)) {
            class_replaceMethod(self, mySelector, method_getImplementation(orginalMethod), method_getTypeEncoding(orginalMethod))
        } else {
            method_exchangeImplementations(orginalMethod, myMethod)
        }
    }

    @objc private func myLocaLizedString(forKey key: String,value: String?, table: String?) -> String {
        guard
            let bundlePath = Bundle.main.path(forResource: emmysManager.currentLanguage.languageCode, ofType: "lproj"),
            let bundle = Bundle(path: bundlePath) else {
                return Bundle.main.myLocaLizedString(forKey: key, value: value, table: table)
        }
        return bundle.myLocaLizedString(forKey: key, value: value, table: table)
    }
}
