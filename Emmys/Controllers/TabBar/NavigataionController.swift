//
//  NavigataionController.swift
//  Emmys
//
//  Created by Asim Najam on 2/8/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class NavigataionController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let logo = UIImage(named: "emmysLogo")
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40.0, height: 80.0))
        imageView.contentMode = .scaleAspectFit
        imageView.image = logo
        navigationItem.titleView = imageView
        setNavigationBarButtonsWith()
    }

    private func setNavigationBarButtonsWith() {
        let menuButton = UIButton(type: .custom)
        let menuImage = UIImage(named: "hamburger")?.maskWithColor(color: Colors.blueColor).resizeWith(width: 20.0, height: 20.0)
        menuButton.frame = CGRect(x: 0, y: 0, width: 44.0, height: 44.0)
        menuButton.setImage(menuImage, for: .normal)
        menuButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
        
        
        let homeButton = UIButton(type: .custom)
        let homeImage = UIImage(named: "home")?.maskWithColor(color: Colors.blueColor).resizeWith(width: 20.0, height: 20.0)
        homeButton.frame = CGRect(x: 0, y: 0, width: 44.0, height: 44.0)
        homeButton.setImage(homeImage, for: .normal)
        homeButton.addTarget(self, action: #selector(showWalletView), for: .touchUpInside)
        
        switch emmysManager.currentLanguage {
        case .arabic:
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: menuButton)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: homeButton)
        case .english:
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: homeButton)
        }
    }
    
    @objc private func showWalletView() {
        let walletController = WalletController()
        walletController.modalPresentationStyle = .overCurrentContext
        print("Show Wallet View")
//        showController(walletController)
    }
    
    @objc private func showMenu() {
        print("Show Menu")
    }
}
