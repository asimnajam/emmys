//
//  RegistrationSuccessController.swift
//  Emmys
//
//  Created by Asim Najam on 2/19/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import RxSwift

class RegistrationSuccessController: UIViewController {
    private let contentView = RegistrationSuccessView(frame: .zero)
    private let keepLogin: Bool
    private let customerID: String
    private let authModel: AuthModel
    private let disposeBag = DisposeBag()
    let controllerDismissSubject: PublishSubject<(String, ConrtollerType)>
    
    init(keepLogin: Bool, customerID: String, authModel: AuthModel, controllerDismissSubject: PublishSubject<(String, ConrtollerType)>) {
        self.keepLogin = keepLogin
        self.customerID = customerID
        self.authModel = authModel
        self.controllerDismissSubject = controllerDismissSubject
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.continueButton.addTarget(
            self,
            action: #selector(continueButtonAction),
            for: .touchUpInside
        )
        hideBackButton()
    }
    
    @objc func continueButtonAction() {
        getUser()
    }
    
    private func proceedToLogin() {
        controllerDismissSubject.onNext(("", .registrationSuccess))
    }
    
    private func getUser() {
        view.showLoader()
        guard let mobileNumber = userDefault.object(forKey: userDefaultMobileKey) as? String else {
            assertionFailure("Mobile Number Found Nil from User Defaults")
            return
        }
        authModel.getUser(customerID, mobileNumber: mobileNumber)
    }
}
