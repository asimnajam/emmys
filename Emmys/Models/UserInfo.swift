//
//  UserInfo.swift
//  Emmys
//
//  Created by Asim Najam on 2/13/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import RealmSwift

final class UserInfo: Object, Decodable {
    @objc dynamic var userCode: String = ""
    @objc dynamic var userName: String = ""
    @objc dynamic var userEmail: String = ""
    @objc dynamic var userLastInvoiceFrom: String = ""
    @objc dynamic var userLastInvoiceValue: Int = 0
    @objc dynamic var userLastInvoiceOn: Date = Date()
    @objc dynamic var userTotalPoints: Int = 0
    @objc dynamic var userRedeemedPoints: Int = 0
    @objc dynamic var userBalancePoints: Int = 0
    @objc dynamic var userRedeemableAMT: Int = 0
    @objc dynamic var userSRPerPoints: Int = 0
    @objc dynamic var userJoinDate: Date = Date()
    @objc dynamic var mobileNumber: String = ""
    
    override class func primaryKey() -> String? {
        return "userCode"
    }
    
    enum CodingKeys: String, CodingKey {
        case userCode = "LY_CUST_CODE"
        case userName = "LY_CUST_USER_NAME"
        case userEmail = "LY_CUST_EMAIL"
        case userLastInvoiceFrom = "LAST_INVOICE_FROM"
        case userLastInvoiceValue = "LAST_INVOICE_VALUE"
        case userLastInvoiceOn = "LAST_INVOICE_ON"
        case userTotalPoints = "TOTAL_POINTS"
        case userRedeemedPoints = "REDEEMED_POINTS"
        case userBalancePoints = "BALANCE_POINTS"
        case userRedeemableAMT = "REDEEMABLE_AMT"
        case userSRPerPoints = "SR_PER_POINTS"
        case userJoinDate = "JOIN_DATE"
    }
    
    public required init() {}
    
    public required init(from decoder: Decoder) throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d yyyy"
        
        let joinDateformatter = DateFormatter()
        joinDateformatter.dateFormat = "MMM dd, yyyy"
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        userCode = container.safeDecode(.userCode) ?? ""
        userName = container.safeDecode(.userName) ?? ""
        userEmail = container.safeDecode(.userEmail) ?? ""
        userLastInvoiceFrom = container.safeDecode(.userLastInvoiceFrom) ?? ""
        userLastInvoiceValue = container.safeDecode(.userLastInvoiceValue) ?? 0
        
        userLastInvoiceOn = container.safeDecode(.userLastInvoiceOn, using: formatter) ?? Date()
        userTotalPoints = container.safeDecode(.userTotalPoints) ?? 0
        userRedeemedPoints = container.safeDecode(.userRedeemedPoints) ?? 0
        userBalancePoints = container.safeDecode(.userBalancePoints) ?? 0
        userRedeemableAMT = container.safeDecode(.userRedeemableAMT) ?? 0
        userSRPerPoints = container.safeDecode(.userSRPerPoints) ?? 0
        
        
        userJoinDate = container.safeDecode(.userJoinDate, using: joinDateformatter) ?? Date()
    }
    
    static var activeUserId: String? {
        return getActiveUser?.userCode ?? nil
    }
    
    static var getActiveUser: UserInfo? {
        let realmDB = Database.realmDb()
        guard let activeUser = realmDB.objects(UserInfo.self).first(where: { $0.userCode == User.userID }) else {
            return nil
        }
        return activeUser
    }
    
    static func save(user: UserInfo) {
        if let activeUser = getActiveUser {
            overrideActiveUserInfo(activeUser, user: user)
        } else {
            createUserInfo(user)
        }
    }
    
    private static func createUserInfo(_ user: UserInfo) {
        let realmDB = Database.realmDb()
        realmDB.write(onError: Database.funcAndLine) {
            realmDB.add(user)
        }
    }
    
    private static func overrideActiveUserInfo(_ activeUser: UserInfo, user: UserInfo) {
        let realmDB = Database.realmDb()
        realmDB.write(onError: Database.funcAndLine) {
            activeUser.userName = user.userName
            activeUser.userEmail = user.userEmail
            activeUser.userLastInvoiceFrom = user.userLastInvoiceFrom
            activeUser.userLastInvoiceValue = user.userLastInvoiceValue
            activeUser.userLastInvoiceOn = user.userLastInvoiceOn
            activeUser.userTotalPoints = user.userTotalPoints
            activeUser.userRedeemedPoints = user.userRedeemedPoints
            activeUser.userBalancePoints = user.userBalancePoints
            activeUser.userRedeemableAMT = user.userRedeemableAMT
            activeUser.userSRPerPoints = user.userSRPerPoints
            activeUser.userJoinDate = user.userJoinDate
        }
    }
    
    static func update(name: String, email: String, mobile: String) {
        guard let realmUser = getActiveUser else { return }
        let realmDB = Database.realmDb()
        realmDB.write(onError: Database.funcAndLine) {
            realmUser.userName = name
            realmUser.userEmail = email
            realmUser.mobileNumber = mobile
        }
    }
    
    static func addMobile(mobile: String) {
        guard let realmUser = getActiveUser else { return }
        let realmDB = Database.realmDb()
        realmDB.write(onError: Database.funcAndLine) {
            realmUser.mobileNumber = mobile
        }
    }
}
