//
//  APICommunicator.swift
//  Emmys
//
//  Created by Syed Asim Najam on 31/01/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

fileprivate enum APIError: Error {
    case noData
    case loginFailed
}

final class APICommunicator {
    @discardableResult static func getBranchList() -> Promise<[BranchList]> {
        return Promise<[BranchList]> { seal in
            let url = EndPoints.branchList(brandID: "EM").path
            HTTPClient.requestData(url, completion: { branchData in
                guard let data = branchData else {
                    seal.reject(APIError.noData)
                    return
                }
                do {
                    let branchList = try JSONDecoder().decode([BranchList].self, from: data)
                    seal.fulfill(branchList)
                    BranchList.save(branchList: branchList)
                } catch {
                    seal.reject(error)
                }
            }, failure: { _, error in
                seal.reject(error)
            })
        }
    }
    
    static func getItemPoints(_ completion: @escaping (Swift.Result<[ItemPoint], Error>) -> Void) {
        let url = EndPoints.pointsList(brandID: "EM").path
        HTTPClient.requestData(url, completion: { itemPoints in
            guard let itemPoints = itemPoints else {
                completion(.failure(APIError.noData))
                return
            }
            do {
                let data = try JSONDecoder().decode([ItemPoint].self, from: itemPoints)
                completion(.success(data))
            } catch {
                completion(.failure(error))
            }
        }, failure: { _, error in
            completion(.failure(error))
        })
    }
    
    static func getOfferLists() -> Promise<[OfferList]> {
        return Promise<[OfferList]> { promise in
            let url = EndPoints.getOfferList.path
            HTTPClient.requestData(url, completion: { offersList in
                guard let offersList = offersList else {
                    promise.reject(APIError.noData)
                    return
                }
                do {
                    let data = try JSONDecoder().decode([OfferList].self, from: offersList)
                    promise.fulfill(data)
                    OfferList.save(offerList: data)
                } catch {
                    promise.reject(error)
                }
            }, failure: { _, error in
                promise.reject(error)
            })
        }
    }
    
    static func login(mobile: String, password: String, brandID: String = "EM") -> Promise<User?> {
        return Promise<User?> { promise in
            let url = EndPoints.getCustomerID(
                mobile: mobile,
                password: password,
                brandID: brandID
            ).path
            HTTPClient.requestData(url, completion: { user in
                guard let user = user else {
                    promise.reject(APIError.noData)
                    return
                }
                do {
                    if let userData = try JSONDecoder().decode([User].self, from: user).first {
                        if userData.isLoggedIn {
                            promise.fulfill(userData)
                            User.save(userData)
                        } else {
                            promise.reject(APIError.loginFailed)
                        }
                    }
                    
                } catch {
                    promise.reject(error)
                }
            }, failure: { _, error in
                promise.reject(error)
            })
        }
    }
    
    static func signup(
        name: String,
        mobile: String,
        email: String,
        password: String,
        brandID: String = "EM",
        _ completion: @escaping (Swift.Result<User?, Error>) -> Void) {
        let url = EndPoints.registerCustomer(
            customerName: name,
            mobile: mobile,
            email: email,
            password: password,
            brandID: brandID
        ).path
        
        if let newUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            HTTPClient.requestData(newUrl, completion: { user in
                guard let user = user else {
                    completion(.failure(APIError.noData))
                    return
                }
                do {
                    let data = try JSONDecoder().decode([User].self, from: user)
                    completion(.success(data.first))
                    if let user = data.first {
                        User.save(user)
                    }
                } catch {
                    completion(.failure(error))
                }
            }, failure: { _, error in
                completion(.failure(error))
            })
        }
    }
    
    static func verifyOTP(
        customerID: String,
        otp: String,
        _ completion: @escaping (Swift.Result<OTP?, Error>) -> Void) {
        let url = EndPoints.verifyOTP(customerID: customerID, otp: otp).path
        HTTPClient.requestData(url, completion: { user in
            guard let user = user else {
                completion(.failure(APIError.noData))
                return
            }
            do {
                let data = try JSONDecoder().decode([OTP].self, from: user)
                completion(.success(data.first))
            } catch {
                completion(.failure(error))
            }
        }, failure: { _, error in
            completion(.failure(error))
        })
    }
    
    static func getUserHistory(customerID: String, _ completion: @escaping (Swift.Result<[UserHistory], Error>) -> Void) {
        let url = EndPoints.customerHistory(customerID: customerID).path
        HTTPClient.requestData(url, completion: { offersList in
            guard let offersList = offersList else {
                completion(.failure(APIError.noData))
                return
            }
            do {
                let data = try JSONDecoder().decode([UserHistory].self, from: offersList)
                completion(.success(data))
            } catch {
                completion(.failure(error))
            }
        }, failure: { _, error in
            completion(.failure(error))
        })
    }
    
    static func resetPassword(
        mobile: String,
        brandID: String = "EM",
        password: String,
        otp: String,
        _ completion: @escaping (Swift.Result<ResetPassword?, Error>) -> Void) {
        let url = EndPoints.resetPassword(mobile: mobile, brandID: brandID, password: password, otp: otp).path
        HTTPClient.requestData(url, completion: { user in
            guard let user = user else {
                completion(.failure(APIError.noData))
                return
            }
            do {
                let data = try JSONDecoder().decode([ResetPassword].self, from: user)
                completion(.success(data.first))
            } catch {
                completion(.failure(error))
            }
        }, failure: { _, error in
            completion(.failure(error))
        })
    }
    
    static func getItemList(
        brandID: String = "EM",
        points: String,
        _ completion: @escaping (Swift.Result<[Item], Error>) -> Void) {
        let url = EndPoints.itemList(brandID: brandID, points: points).path
        HTTPClient.requestData(url, completion: { user in
            guard let user = user else {
                completion(.failure(APIError.noData))
                return
            }
            do {
                let data = try JSONDecoder().decode([Item].self, from: user)
                completion(.success(data))
            } catch {
                completion(.failure(error))
            }
        }, failure: { _, error in
            completion(.failure(error))
        })
    }
    
    static func getItemImage(_ itemID: String, _ completion: @escaping (Swift.Result<Data, Error>) -> Void) {
        let url = EndPoints.itemImage(itemID: itemID).path
        HTTPClient.requestData(url, completion: { itemImage in
            guard let itemImage = itemImage else {
                completion(.failure(APIError.noData))
                return
            }
            completion(.success(itemImage))
        }, failure: { _, error in
            completion(.failure(error))
        })
    }
    
    static func getUserInfo(customerID: String) -> Promise<UserInfo?> {
        return Promise<UserInfo?> { seal in
            let url = EndPoints.getUserInfo(customerID: customerID).path
            HTTPClient.requestData(url, completion: { user in
                guard let user = user else {
                    seal.reject(APIError.noData)
                    return
                }
                do {
                    let data = try JSONDecoder().decode([UserInfo].self, from: user)
                    seal.fulfill(data.first)
                    if let user = data.first {
                        UserInfo.save(user: user)
                    }
                } catch {
                    seal.reject(error)
                }
            }, failure: { _, error in
                seal.reject(error)
            })
        }
    }
    
    static func redeemPoints(customerID: String, offerID: String, points: String, _ completion: @escaping (Swift.Result<RedeemVoucher?, Error>) -> Void) {
        let url = EndPoints.redeemPoints(customerID: customerID, offerID: offerID, points: points).path
        HTTPClient.requestData(url, completion: { user in
            guard let user = user else {
                completion(.failure(APIError.noData))
                return
            }
            do {
                let data = try JSONDecoder().decode([RedeemVoucher].self, from: user)
                completion(.success(data.first))
            } catch {
                completion(.failure(error))
            }
        }, failure: { _, error in
            completion(.failure(error))
        })
    }
    
    static func resendOTP(mobile: String, brandID: String = "EM", _ completion: @escaping (Swift.Result<OTP?, Error>) -> Void) {
        let url = EndPoints.resendOTP(mobile: mobile, brandID: brandID).path
        HTTPClient.requestData(url, completion: { user in
            guard let user = user else {
                completion(.failure(APIError.noData))
                return
            }
            do {
                let data = try JSONDecoder().decode([OTP].self, from: user)
                completion(.success(data.first))
            } catch {
                completion(.failure(error))
            }
        }, failure: { _, error in
            completion(.failure(error))
        })
    }
    
    static func getBillHeaderInfo(customerID: String, billID: String, _ completion: @escaping (Swift.Result<BillHeaderInfo, Error>) -> Void) {
        let url = EndPoints.billInfoSummary(customerID: customerID, billNumber: billID).path
        HTTPClient.requestData(url, completion: { user in
            guard let user = user else {
                completion(.failure(APIError.noData))
                return
            }
            do {
                let data = try JSONDecoder().decode(BillHeaderInfo.self, from: user)
                completion(.success(data))
            } catch {
                completion(.failure(error))
            }
        }, failure: { _, error in
            completion(.failure(error))
        })
    }
    
    static func scanBill(customerID: String, billID: String, _ completion: @escaping (Swift.Result<ScanBill?, Error>) -> Void) {
        let url = EndPoints.scanInvoice(customerID: customerID, billNumber: billID).path
        HTTPClient.requestData(url, completion: { user in
            guard let user = user else {
                completion(.failure(APIError.noData))
                return
            }
            do {
                let data = try JSONDecoder().decode([ScanBill].self, from: user)
                completion(.success(data.first))
            } catch {
                completion(.failure(error))
            }
        }, failure: { _, error in
            completion(.failure(error))
        })
    }
    
    static func updateUser(customerID: String, name: String, mobile: String, email: String, _ completion: @escaping (Swift.Result<Bool, Error>) -> Void) {
        let url = EndPoints.updateUser(customerID: customerID, customerName: name, mobile: mobile, email: email).path + "/"
        if let newUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            HTTPClient.requestData(newUrl, completion: { user in
                guard let user = user else {
                    completion(.failure(APIError.noData))
                    return
                }
                do {
                    let apiData = try JSONDecoder().decode([ScanBill].self, from: user)
                    if let data = apiData.first {
                        completion(.success(data.isBillScanned))
                    } else {
                        completion(.success(false))
                    }
                } catch {
                    completion(.failure(error))
                }
            }, failure: { _, error in
                completion(.failure(error))
            })
        }
    }
    
    static func voucherHistory(customerID: String, _ completion: @escaping (Swift.Result<[VoucherHistory], Error>) -> Void) {
        let url = EndPoints.voucherHistory(customerID: customerID).path
        HTTPClient.requestData(url, completion: { user in
            guard let user = user else {
                completion(.failure(APIError.noData))
                return
            }
            do {
                let apiData = try JSONDecoder().decode([VoucherHistory].self, from: user)
                completion(.success(apiData))
            } catch {
                completion(.failure(error))
            }
        }, failure: { _, error in
            completion(.failure(error))
        })
    }
    
    
    
}
