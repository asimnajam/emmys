//
//  UserHistory.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation

final class UserHistory: Decodable {
    var flag: Int = 0
    var billDate: Date? = nil
    var branch: String = ""
    var billNumber: String = ""
    var amount: Float = 0.0
    var point: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case flag = "Flag"
        case billDate = "BILL_DATE"
        case branch = "BRANCH"
        case billNumber = "BILL_NO"
        case amount = "AMOUNT"
        case point = "POINT"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        flag = container.safeDecode(.flag) ?? 0
        let formatter = Date.Formatters.formatterISO8601
        billDate = container.safeDecode(.billDate, using: formatter) ?? nil
        branch = container.safeDecode(.branch) ?? "---"
        amount = container.safeDecode(.amount) ?? 0.0
        point = container.safeDecode(.point) ?? 0
    }
}
