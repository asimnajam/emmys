//
//  OffersController.swift
//  Emmys
//
//  Created by Asim Najam on 2/8/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class OffersController: UIViewController {
    typealias OffersCell = TableViewContainerCell<OffersCellView>
    let contentView = OffersView(frame: .zero)
    let coffeeImage = UIImage(named: "coffee")
    let drinkImage = UIImage(named: "drink")
    let redbullImage = UIImage(named: "redBull")
    var images: [UIImage?] = []
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        images = [coffeeImage, drinkImage, redbullImage, coffeeImage, drinkImage, redbullImage, coffeeImage, drinkImage, redbullImage, coffeeImage, drinkImage, redbullImage]
        contentView.tableView.registerCell(OffersCell.self)
        contentView.tableView.delegate = self
        contentView.tableView.dataSource = self
        
        setEmmysLogo()
    }
}

extension OffersController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: String(describing: OffersCell.self),
            for: indexPath
        )
        guard let cardCell = cell as? OffersCell else {
            return cell
        }
        
        let offer = images[indexPath.row]
        let cardView: OffersCellView
        if let existingCardView = cardCell.cellView {
            cardView = existingCardView
        } else {
            cardView = OffersCellView()
            cardCell.cellView = cardView
        }
        let text: String
        let oddRow = indexPath.row % 2 == 0
        if oddRow {
            text = "Your 500 points Bronze voucher is ready to collect from any branch, Your 500 points Bronze voucher is ready to collect from any branch"
        } else {
            text = "Your 500 points"
        }
        cardView.productInfoLabel.text = text
        cardView.productImage.image = offer
        return cardCell
    }
}
