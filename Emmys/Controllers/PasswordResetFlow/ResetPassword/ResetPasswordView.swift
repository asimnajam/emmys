//
//  ResetPasswordView.swift
//  Emmys
//
//  Created by Asim Najam on 2/16/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class ResetPasswordView: UIView {
//    let closeButton: UIButton = {
//        let button = UIButton()
//        button.setImage(UIImage(named: "closeIcon"), for: .normal)
//        return button
//    }()
    let headerView: HeaderView = {
        let view = HeaderView(frame: .zero)
        view.configureWithTitle(title: LocalizedLanguage.new_password)
        return view
    }()
    
    let passwordTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .password)
        return textField
    }()
    
    let confirmPasswordTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .confirmPassword)
        return textField
    }()
    
    let submitButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle(LocalizedLanguage.submit, for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 0, leading: 50.0, bottom: 0, trailing: 50.0)
        add(views: [headerView, passwordTextField, confirmPasswordTextField, submitButton])
        
//        closeButton.snp.makeConstraints { make in
//            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
//            make.width.height.equalTo(44.0)
//            make.leading.equalToSuperview()
//        }
        
        headerView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)//.offset(10.0)
            make.leadingMargin.trailingMargin.equalToSuperview()
        }
        passwordTextField.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.height.equalTo(50.0)
            make.top.equalTo(headerView.snp.bottom)
        }
        
        confirmPasswordTextField.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.height.equalTo(50.0)
            make.top.equalTo(passwordTextField.snp.bottom).offset(10)
        }
        
        submitButton.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(confirmPasswordTextField.snp.bottom).offset(10)
            make.height.equalTo(50.0)
        }
    }
}
