//
//  PickerView.swift
//  Emmys
//
//  Created by Asim Najam on 2/9/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

enum PickerViewType {
    case city
    case address
    case branch
}

final class PickerView: UIView {
    let containerView = UIView()
    let closeButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setTitle("Close", for: .normal)
        button.setTitleColor(.red, for: .normal)
        button.addTarget(self, action: #selector(removeView), for: .touchUpInside)
        return button
    }()
    let pickerView: UIPickerView = {
        let pv = UIPickerView()
        pv.backgroundColor = .white
        return pv
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        containerView.backgroundColor = .gray
        containerView.alpha = 0.8
        directionalLayoutMargins = .init(top: 20.0, leading: 20.0, bottom: 20.0, trailing: 20.0)
        
        addSubview(containerView)
        addSubview(pickerView)
        addSubview(closeButton)
        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        pickerView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.9)
            make.height.equalTo(pickerView.snp.width)
        }
        closeButton.snp.makeConstraints { make in
            make.bottom.equalTo(pickerView.snp.top)
            make.leading.equalTo(pickerView)
            make.width.height.equalTo(44.0)
        }
    }
    
    @objc func removeView() {
        removeFromSuperview()
    }
    
    override func didMoveToSuperview() {
//        [picker selectRow:0 inComponent:0 animated:YES];
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: true)
    }
}
