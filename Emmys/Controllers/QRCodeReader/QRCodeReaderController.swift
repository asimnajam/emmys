//
//  QRCodeReaderController.swift
//  Emmys
//
//  Created by Amir on 2/5/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import AVFoundation

class QRCodeReaderController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    let contentView = QRCodeReaderView(frame: .zero)
    let metadataOutput = AVCaptureMetadataOutput()
    let avCaptureSession = AVCaptureSession()
    
    enum cameraError: Error {
        case noCameraAvailable
        case videoInputInitFailed
        case failedToAddCaptureInputDevice
        case failedToAddCaptureOutputDevice
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
            super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        view.layoutIfNeeded()
        contentView.qrCodeScannerView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        contentView.scanBarCodeLabel.text = LocalizedLanguage.scan_barcode_on_your_receipt
        if !contentView.qrCodeScannerView.isRunning {
            contentView.qrCodeScannerView.startScanning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !contentView.qrCodeScannerView.isRunning {
            contentView.qrCodeScannerView.stopScanning()
        }
    }
}

extension QRCodeReaderController: QRScannerViewDelegate {
    func qrScanningDidStop() {
    }
    
    func qrScanningDidFail() {
        Toast.show(style: .error(message: "Failed"), view: view)
        contentView.qrCodeScannerView.stopScanning()
        resumeScanning()
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        print(str ?? "no Reader Data")
        guard let billID = str else {
            Toast.show(style: .error(message: "Bill ID not found"), view: view)
            return
        }
        contentView.qrCodeScannerView.stopScanning()
        callScanBillAPI(billID: billID)
    }
    
    func callScanBillAPI(billID: String) {
        guard let customerID = emmysManager.activeUserID else { return }
        APICommunicator.scanBill(customerID: customerID, billID: billID) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let bill):
                guard let bill = bill else { return }
                if bill.isBillScanned {
                    self.resumeScanning()
                    self.showPointsController(billID: billID)
                } else {
                    self.showBillUsedError()
                }
            case .failure(let error):
                Toast.show(style: .error(message: "Scan bill API failed with error: \(error)"), view: self.view)
                print(error)
            }
        }
    }
    
    private func showPointsController(billID: String) {
        let controller = PointsController()
        controller.billID = billID
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func showBillUsedError() {
        Toast.show(style: .error(message: "Bill already used"), view: self.view)
        resumeScanning()
    }
    
    private func resumeScanning() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.contentView.qrCodeScannerView.startScanning()
        }
    }
}
