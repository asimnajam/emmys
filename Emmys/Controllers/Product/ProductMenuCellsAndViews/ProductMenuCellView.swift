//
//  ProductMenuCellView.swift
//  Emmys
//
//  Created by Asim Najam on 2/10/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class ProductMenuCellView: UIView {
    let menuLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Colors.blueColor
        label.font = UIFont.mediumFont
        label.numberOfLines = 2
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor  = .white
        add(views: [menuLabel])
        
        menuLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6)
//            make.height.equalToSuperview()
        }
    }
}
