//
//  Decoding.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
private struct AnyCodingKey: CodingKey {
    var stringValue: String
    var intValue: Int?

    init(_ string: String) {
        stringValue = string
    }

    init?(stringValue: String) {
        self.stringValue = stringValue
    }

    init?(intValue: Int) {
        self.intValue = intValue
        stringValue = String(intValue)
    }
}

// currently using TryorNil
public struct SafeDecodingContainer<Base: Decodable>: Decodable {
    public let value: Base?
    public init(from decoder: Decoder) throws {
        do {
            let container = try decoder.singleValueContainer()
            value = try container.decode(Base.self)
        } catch {
            print("Mapping ERROR: \(error)")
            value = nil
        }
    }
}

extension KeyedDecodingContainer {
    func decode<T: Decodable>(_ key: KeyedDecodingContainer.Key) throws -> T {
        return try decode(T.self, forKey: key)
    }

    public func safeDecode<T: Decodable>(_ key: KeyedDecodingContainer.Key) -> T? {
        return safeDecode(T.self, forKey: key)
    }

    public func safeDecode<T: Decodable>(_: T.Type, forKey key: KeyedDecodingContainer.Key) -> T? {
        let decoded = try? decodeIfPresent(SafeDecodingContainer<T>.self, forKey: key)
        return decoded?.value
    }

    public func safeDecodeIfPresent<T: Decodable>(_ key: KeyedDecodingContainer.Key) -> T? {
        return safeDecodeIfPresent(T.self, forKey: key)
    }

    public func safeDecodeIfPresent<T: Decodable>(_: T.Type, forKey key: KeyedDecodingContainer.Key) -> T? {
        let decoded = try? decodeIfPresent(SafeDecodingContainer<T>.self, forKey: key)
        return decoded?.value
    }
}

// Array and list Decoder
extension KeyedDecodingContainer {
    func safeDecodeArray<T: Decodable>(_ key: KeyedDecodingContainer.Key) -> [T] {
        return safeDecodeArray(T.self, forKey: key)
    }

    func safeDecodeArray<T: Decodable>(_: T.Type, forKey key: KeyedDecodingContainer.Key) -> [T] {
        let array = try? decode(SafeDecodingContainer<[T]>.self, forKey: key)
        return array?.value?.compactMap { $0 } ?? []
    }

    func safeDecodeArray<T: Decodable>(_ key: KeyedDecodingContainer.Key) -> [T]? {
        return safeDecodeArray(T.self, forKey: key)
    }

    func safeDecodeArray<T: Decodable>(_: T.Type, forKey key: KeyedDecodingContainer.Key) -> [T]? {
        let array = try? decode(SafeDecodingContainer<[T]>.self, forKey: key)
        return array?.value?.compactMap { $0 }
    }

}

// Date Decoder
extension KeyedDecodingContainer {
    public func safeDecode(_ key: KeyedDecodingContainer.Key, using formatter: DateFormatter) -> Date? {
        let decoded = try? decode(SafeDecodingContainer<String>.self, forKey: key)
        guard let dateStr = decoded?.value,
            let date = formatter.date(from: dateStr) else {
            return nil
        }
        return date
    }

    public func safeDecode(_ key: KeyedDecodingContainer.Key, using formatter: DateFormatter) -> Date {
        let decoded = try? decode(SafeDecodingContainer<String>.self, forKey: key)
        guard let dateStr = decoded?.value,
            let date = formatter.date(from: dateStr) else {
            return Date()
        }
        return date
    }
}

// Decode safely array of objects or object
public extension JSONDecoder {
    func safeDecodeArray<T: Decodable>(of _: T.Type, from data: Data) throws -> [T] {
        let array = try? decode(SafeDecodingContainer<[T]>.self, from: data)
        return array?.value?.compactMap { $0 } ?? []
    }

    func safeDecode<T: Decodable>(_: T.Type, from data: Data) throws -> T? {
        return try? decode(SafeDecodingContainer<T>.self, from: data).value
    }
}
