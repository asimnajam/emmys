//
//  UserImage.swift
//  Emmys
//
//  Created by Asim Najam on 3/3/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import RealmSwift

final class UserImage: Object {
    @objc dynamic var imageData: Data? = Data()
    
    init(imageData: Data?) {
        self.imageData = imageData
    }
    
    required init() {}
}

extension UserImage {
    static func updateUserImage(image: Data?) {
        let realmDB = Database.realmDb()
        if let userImage = realmDB.objects(UserImage.self).first {
            realmDB.write(onError: Database.funcAndLine) {
                userImage.imageData = image
            }
        } else {
            let userImage = UserImage(imageData: image)
            realmDB.write(onError: Database.funcAndLine) {
                realmDB.add(userImage)
            }
        }
    }
    
    static var image: UIImage? {
        let realmDB = Database.realmDb()
        guard let data = realmDB.objects(UserImage.self).first,
              let imageData = data.imageData else {
            return nil
        }
        return UIImage(data: imageData)
    }
}

