//
//  PointsHistoryController.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class PointsHistoryController: UIViewController {
    typealias HistoryCell = TableViewContainerCell<PointsHistoryCellView>
    let contentView = PointsHistoryView(frame: .zero)
    var userHistory: [UserHistory] = []
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.tableView.registerCell(HistoryCell.self)
        contentView.tableView.delegate = self
        contentView.tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        contentView.tableView.reloadData()
        contentView.pointHistoryLabel.text = LocalizedLanguage.points_history
        let totalPoints = self.userHistory.compactMap { $0.point }.reduce(0, +)
        self.contentView.setTotalPoints(points: Float(totalPoints))
        fetchUserHistory()
    }
    
    func fetchUserHistory() {
        guard let id = emmysManager.activeUserID else {
            assertionFailure("Customer ID found nil")
            return
        }
        APICommunicator.getUserHistory(customerID: id) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let userHistory):
                self.userHistory = userHistory
                let totalPoints = self.userHistory.compactMap { $0.point }.reduce(0, +)
                self.contentView.setTotalPoints(points: Float(totalPoints))
                self.contentView.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension PointsHistoryController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userHistory.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: String(describing: HistoryCell.self),
            for: indexPath
        )
        guard let cardCell = cell as? HistoryCell else {
            return cell
        }
        
        let history = userHistory[indexPath.row]
        let cardView: PointsHistoryCellView
        if let existingCardView = cardCell.cellView {
            cardView = existingCardView
        } else {
            cardView = PointsHistoryCellView(frame: .zero, kind: .cell)
            cardCell.cellView = cardView
        }
        let oddRow = indexPath.row % 2 == 0
        let color: UIColor = oddRow ? Colors.lightGrey : .white
        cardView.configure(date: history.billDate?.formattedString ?? "---", branch: history.branch, ammount: "\(history.amount)", point: "\(history.point)", color: color)
        cardCell.selectionStyle = .none
        return cardCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return PointsHistoryCellView(frame: .zero, kind: .header)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let history = userHistory[indexPath.row]
        let detailView = PointsHistoryDetailView(frame: .zero)
        detailView.configure(date: history.billDate?.formattedString ?? "---", branch: history.branch, ammount: "\(history.amount)", point: "\(history.point)")
        view.add(views: [detailView])
        detailView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
