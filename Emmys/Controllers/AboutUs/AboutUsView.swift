//
//  AboutUsView.swift
//  Emmys
//
//  Created by Asim Najam on 2/9/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class AboutUsView: UIView {
    private let contentScrollView = UIScrollView(frame: .zero)
    private let contentView = UIView(frame: .zero)

    let headingLabel: UILabel = {
        let label = LocalizableLabel()
        label.textColor = Colors.blueColor
        label.font = UIFont.xlargeFont
        label.text = LocalizedLanguage.about_emmys
        label.directionalLayoutMargins = .init(top: 0, leading: 10.0, bottom: 0, trailing: 10.0)
        return label
    }()

    let introLabel: UILabel = {
        let label = LocalizableLabel()
        label.textColor = .lightGray
        label.numberOfLines = 0
        label.textAlignment = .justified
        label.font = UIFont.mediumFont
        label.text = LocalizedLanguage.about_us_first_para
        label.directionalLayoutMargins = .init(top: 0, leading: 10.0, bottom: 0, trailing: 10.0)
        return label
    }()

    let secondHeadingLabel: UILabel = {
        let label = LocalizableLabel()
        label.textColor = Colors.blueColor
        label.font = UIFont.xlargeFont
        label.text = "Second Heading"
        return label
    }()

    let secondIntroLabel: UILabel = {
        let label = LocalizableLabel()
        label.textColor = .lightGray
        label.numberOfLines = 0
        label.font = UIFont.mediumFont
        label.textAlignment = .justified
        label.text = LocalizedLanguage.about_us_second_para
        return label
    }()

    let thirdHeadingLabel: UILabel = {
        let label = AboutUsHeadingLabel()
        label.textColor = Colors.blueColor
        label.font = UIFont.xlargeFont
        label.text = "Third Heading"
        return label
    }()

    let thirdIntroLabel: UILabel = {
        let label = LocalizableLabel()
        label.textColor = Colors.blueColor
        label.numberOfLines = 0
        label.font = UIFont.mediumFont
        label.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        return label
    }()

    lazy var verticalStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            headingLabel,
            introLabel,
            secondIntroLabel
        ])
        
//        let stackView = UIStackView(arrangedSubviews: [
//            headingLabel,
//            introLabel,
//            secondHeadingLabel,
//            secondIntroLabel,
//            thirdHeadingLabel,
//            thirdIntroLabel
//        ])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .equalSpacing
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.spacing = 0
        stackView.directionalLayoutMargins = .init(top: 0, leading: 10, bottom: 0, trailing: 10)
        return stackView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        directionalLayoutMargins = .init(top: 10.0, leading: 10.0, bottom: 10.0, trailing: 10.0)
        backgroundColor = .white
        contentView.directionalLayoutMargins = .init(top: 0, leading: 32, bottom: 24, trailing: 32)

        translatesAutoresizingMaskIntoConstraints = false
        contentScrollView.addSubview(verticalStackView)
        add(views: [contentScrollView])

//        aboutUsLabel.snp.makeConstraints { make in
//            make.top.equalTo(safeAreaLayoutGuide.snp.top)
//            make.leadingMargin.trailingMargin.equalToSuperview()
//        }
    
        contentScrollView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
            make.bottom.equalToSuperview()
            make.centerX.equalToSuperview()
            make.width.equalToSuperview()
                //.multipliedBy(1)
        }

        verticalStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.width.equalTo(contentScrollView.snp.width)
        }
        verticalStackView.setCustomSpacing(20.0, after: headingLabel)
    }

}
