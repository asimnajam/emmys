//
//  NavigationBar.swift
//  Emmys
//
//  Created by Syed Asim Najam on 04/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import UIKit

public extension UIViewController {
    func hideBackButton() {
        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
    }
    enum NavigationBarStyle {
        case standard
        case transparent
    }

    func applyNavigationBarStyle(_ style: NavigationBarStyle) {
        switch style {
        case .standard:
            navigationController?.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor: UIColor.black,
                NSAttributedString.Key.font: UIFont.mediumFont
            ]
            navigationController?.navigationBar.barTintColor = UIColor.white
        default:
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationController?.navigationBar.shadowImage = UIImage()
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationItem.leftBarButtonItem = nil
            navigationController?.navigationItem.rightBarButtonItem = nil
        }

        switch style {
        default:
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationController?.navigationBar.shadowImage = UIImage()
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationItem.leftBarButtonItem = nil
            navigationController?.navigationItem.rightBarButtonItem = nil
        }
    }
}
