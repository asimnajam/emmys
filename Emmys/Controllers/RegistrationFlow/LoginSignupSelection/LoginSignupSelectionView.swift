//
//  LoginSignupSelectionView.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

final class LoginSignupSelectionView: UIView {
    let headerView: HeaderView = {
        let view = HeaderView(frame: .zero)
        view.configureWithTitle(title: LocalizedLanguage.loyality_program)
        return view
    }()
    
    let registerButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle(LocalizedLanguage.register_app, for: .normal)
        return button
    }()
    
    let loginButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle(LocalizedLanguage.login_app, for: .normal)
        return button
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        backgroundColor = .white
        add(views: [headerView, registerButton, loginButton])
        directionalLayoutMargins = .init(
            top: 0,
            leading: 50.0,
            bottom: 0,
            trailing: 50.0
        )
        headerView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.leadingMargin.trailingMargin.equalToSuperview()
        }
        
        registerButton.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom)
            make.height.equalTo(50.0)
        }
        
        loginButton.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(registerButton.snp.bottom).offset(20.0)
            make.height.equalTo(50.0)
        }
        
        registerButton.setTitle(LocalizedLanguage.register_app, for: .normal)
        loginButton.setTitle(localizationManager.localizedStringForKey(key: LocalizedLanguage.login), for: .normal)
    }

}
