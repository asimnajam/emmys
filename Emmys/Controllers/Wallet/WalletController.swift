//
//  WalletController.swift
//  Emmys
//
//  Created by Syed Asim Najam on 04/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class WalletController: UIViewController {
    private let contentView = WalletView(frame: .zero)
    private let disposebag = DisposeBag()
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        view.layoutIfNeeded()
        contentView.walletButton.addTarget(
            self,
            action: #selector(enterButtonAction),
            for: .touchUpInside
        )
        contentView.userImageView.image = emmysManager.userImage
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getUserDetail()
    }
    
    @objc func closeButtonAction() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func enterButtonAction() {}
    
    func setupPoints(user: UserInfo) {
        showRedeemPopover(user: user)
        contentView.pointsLabel.text = "\(user.userBalancePoints) \(LocalizedLanguage.points)"
        contentView.userNameLabel.text = user.userName
    }
    
    func showRedeemPopover(user: UserInfo) {
        if let closestOffer = OfferList.closestOffer(user.userBalancePoints) {
            let redeemPopover = RedeemPointsPopoverView(
                point: closestOffer.points
            )
            redeemPopover.redeemButtonObs.subscribe(onNext: { [weak self] _ in
                self?.showRedeemView()
                }).disposed(by: disposebag)
            view.addSubview(redeemPopover)
            redeemPopover.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
        }
    }
    
    private func showRedeemView() {
        guard let user = emmysManager.user else { return }
        if let closestOffer = OfferList.closestOffer(user.userBalancePoints) {
            redeemVoucherAPI(offer: closestOffer)
        }
    }
    
    func redeemVoucherAPI(offer: OfferList) {
        guard let user = emmysManager.user else { return }
        APICommunicator.redeemPoints(customerID: user.userCode, offerID: "\(offer.id)", points: "\(offer.points)") { [weak self] result in
            switch result {
            case .success(let redeemVoucher):
                guard let self = self, let voucher = redeemVoucher else { return }
                self.showRedeemClaimVoucherView(offer: offer, voucher: voucher)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func showRedeemClaimVoucherView(offer: OfferList, voucher: RedeemVoucher) {
        navigationController?.pushViewController(
            RedeemVoucherCodeController(
                offer: offer,
                voucher: voucher
            ),
            animated: true)
    }
    
    func getUserDetail() {
        guard let id = emmysManager.activeUserID else { return }
        APICommunicator.getUserInfo(customerID: id).done { [weak self] userInfo in
            guard let user = userInfo else { return }
            self?.setupPoints(user: user)
        }.catch { error in }
    }
}
