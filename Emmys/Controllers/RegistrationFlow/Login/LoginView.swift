//
//  LoginView.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class LoginView: UIView {
//    let closeButton: UIButton = {
//        let button = UIButton()
//        button.setImage(UIImage(named: "closeIcon"), for: .normal)
//        return button
//    }()
    
    let headerView: HeaderView = {
        let view = HeaderView(frame: .zero)
//        view.configureWithTitle(title: LocalizedLanguage.login)
        return view
    }()
    
   let mobileTextField: UITextField = {
    let textField = PrimaryTextField(textFieldType: .mobile(type: .default))
        return textField
    }()

    let passwordTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .password)
        return textField
    }()
    
    let forgetPasswordButton: UIButton = {
        let button = UnderLinedButton()
        button.setTitleColor(.gray, for: .normal)
        button.titleLabel?.font = UIFont.smallFont
        button.setTitle(LocalizedLanguage.forget_password, for: .normal)
        return button
    }()
    
    let loginButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle(LocalizedLanguage.login, for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        directionalLayoutMargins = .init(top: 0, leading: 50.0, bottom: 0, trailing: 50.0)
        backgroundColor = .white
        add(views: [
            headerView,
//            closeButton,
            mobileTextField,
            passwordTextField,
            forgetPasswordButton,
            loginButton
        ])
//        closeButton.snp.makeConstraints { make in
//            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
//            make.width.height.equalTo(44.0)
//            make.leading.equalToSuperview()
//        }
        
        headerView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)//.offset(10.0)
            make.leadingMargin.trailingMargin.equalToSuperview()
        }

        
        mobileTextField.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom)
            make.height.equalTo(50.0)
        }
        
        passwordTextField.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.height.equalTo(50.0)
            make.top.equalTo(mobileTextField.snp.bottom).offset(15.0)
        }
        
        forgetPasswordButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(passwordTextField.snp.bottom).offset(20.0)
        }
        
        loginButton.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.height.equalTo(50.0)
            make.top.equalTo(forgetPasswordButton.snp.bottom).offset(20.0)
        }
//        bringSubviewToFront(closeButton)
    }
}
