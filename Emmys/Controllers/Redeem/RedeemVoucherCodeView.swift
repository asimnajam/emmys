
//
//  RedeemView.swift
//  Emmys
//
//  Created by Amir on 2/5/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class RedeemVoucherCodeView: UIView {
    let redeemInfoLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.gray
        label.font = UIFont.mediumFont
        label.numberOfLines = 2
        label.text = "Your 500 points Bronze voucher is ready to collect from any branch"
        return label
    }()
    let redeemLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Colors.blueColor
        label.font = UIFont.xlargeFont
        label.numberOfLines = 2
        label.text = "Redeem"
        return label
    }()
    let voucherNumberLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.gray
        label.font = UIFont.mediumFont
        label.numberOfLines = 2
        label.text = "Voucher Number"
        return label
    }()
    let redeemPointsVoucherNumber: UILabel = {
        let label = UILabel()
        label.text = "12345"
        label.textColor = .gray
        label.textAlignment = .center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        backgroundColor = .white
        addSubview(redeemInfoLabel)
        addSubview(redeemLabel)
        addSubview(voucherNumberLabel)
        addSubview(redeemPointsVoucherNumber)
        redeemInfoLabel.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(150.0)
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
            make.centerX.equalToSuperview()
        }
        redeemLabel.snp.makeConstraints { make in
            make.top.equalTo(redeemInfoLabel.snp.bottom).offset(20.0)
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
            make.centerX.equalToSuperview()
        }
        voucherNumberLabel.snp.makeConstraints { make in
            make.top.equalTo(redeemLabel.snp.bottom)
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
            make.centerX.equalToSuperview()
        }
        
        redeemPointsVoucherNumber.snp.makeConstraints { make in
            make.top.equalTo(voucherNumberLabel.snp.bottom).offset(20.0)
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
            make.centerX.equalToSuperview()
            make.height.equalTo(44.0)
        }
        
        redeemPointsVoucherNumber.layer.borderColor = UIColor.gray.cgColor
        redeemPointsVoucherNumber.layer.borderWidth = 2.0
        redeemPointsVoucherNumber.layer.cornerRadius = 5.0
        redeemPointsVoucherNumber.layer.masksToBounds = true
    }
}
