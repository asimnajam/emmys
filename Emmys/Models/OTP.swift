//
//  OTP.swift
//  Emmys
//
//  Created by Syed Asim Najam on 01/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation

final class OTP: Decodable {
    var flag: Int = 0
    private var result: String = ""
    var isValiedOTP: Bool {
        return result == "Valid" && flag == 1
    }
    
    var isOTPReSent: Bool {
        return result == "Sent" && flag == 1
    }
    
    enum CodingKeys: String, CodingKey {
        case flag = "Flag"
        case result = "Result"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        flag = tryOrNil {
            try container.decode(Int.self, forKey: .flag)
            } ?? 0
        result = tryOrNil {
            try container.decode(String.self, forKey: .result)
            } ?? ""
    }
}
