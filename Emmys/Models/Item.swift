//
//  Item.swift
//  Emmys
//
//  Created by Asim Najam on 2/12/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation

final class Item: Decodable {
    var flag: Int = 0
    var code: String = ""
    var itemName: String = ""
    var itemPoints: Int = 0
    var imageFileName: String = ""
    
    enum CodingKeys: String, CodingKey {
        case flag = "Flag"
        case code = "CODE"
        case itemName = "ITEM_NAME"
        case itemPoints = "ITEM_POINTS"
        case imageFileName = "ImageFileName"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        flag = container.safeDecode(.flag) ?? 0
        code = container.safeDecode(.code) ?? ""
        itemName = container.safeDecode(.itemName) ?? ""
        itemPoints = container.safeDecode(.itemPoints) ?? 0
        imageFileName = container.safeDecode(.imageFileName) ?? ""
    }
}
