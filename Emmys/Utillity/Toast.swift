//
//  Toast.swift
//  Emmys
//
//  Created by Syed Asim Najam on 04/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

final class Toast: UIView {
    public enum Style {
        case info(message: String)
        case error(message: String)
        
        var tintColor: UIColor {
            switch self {
            case .info:
                return Colors.greenColor ?? .green
            case .error:
                return Colors.redColor ?? .red
            }
        }

        var textColor: UIColor {
            return .white
        }

        var message: String {
            switch self {
            case let .info(message):
                return message
            case let .error(message):
                return message
            }
        }
    }

    private let style: Style

    private lazy var label: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 0
        label.textAlignment = .center
        label.text = style.message
        return label
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = style.tintColor.withAlphaComponent(0.9)
        view.layer.cornerRadius = 14
        view.clipsToBounds = true
        return view
    }()

    private init(style: Style) {
        self.style = style
        super.init(frame: .zero)
        setupViews()
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupViews() {
        contentView.add(views: [label])
        addSubview(contentView)

        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        label.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(12)
            make.bottom.equalToSuperview().inset(12)
            make.leading.equalToSuperview()//.offset(13)
            make.trailing.equalToSuperview().inset(16)
        }
    }

    private static var toastBeingDisplayed: Toast?
    private static var toastTimer: Timer?
    private static let toastAnimationDuration = 0.3
    private static let toastSideMargin = 20

    public static func show(style: Style, view: UIView, completion: (() -> Void)? = nil) {
        if toastBeingDisplayed.exists {
            toastTimer?.invalidate()
            toastBeingDisplayed?.removeFromSuperview()
        }

        let toast = Toast(style: style)
        view.addSubview(toast)
        toast.bringSubviewToFront(view)

        toast.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(toastSideMargin)
            make.bottom.equalToSuperview().offset(view.safeAreaInsets.bottom)
        }
        view.layoutIfNeeded()

        UIView.animate(
            withDuration: toastAnimationDuration,
            delay: 0,
            usingSpringWithDamping: 0.8,
            initialSpringVelocity: 1,
            options: .curveEaseIn,
            animations: {
                toast.snp.remakeConstraints { make in
                    make.leading.trailing.equalToSuperview().inset(toastSideMargin)
                    make.bottom.equalToSuperview().inset(view.safeAreaInsets.bottom + 8)
                }
                view.layoutIfNeeded()
            },
            completion: { _ in
                toastTimer = Timer.scheduledTimer(withTimeInterval: 2, repeats: false, block: { _ in
                    dismiss {
                        completion?()
                    }
                })
            }
        )
        toastBeingDisplayed = toast
    }

    private static func dismiss(completion: @escaping () -> Void) {
        guard let parentView = toastBeingDisplayed?.superview else {
            toastBeingDisplayed?.removeFromSuperview()
            return
        }

        UIView.animate(withDuration: toastAnimationDuration, animations: {
            toastBeingDisplayed?.snp.remakeConstraints { make in
                make.leading.trailing.equalToSuperview().inset(toastSideMargin)
                make.bottom.equalToSuperview().offset(
                    parentView.safeAreaInsets.bottom + (toastBeingDisplayed?.frame.height ?? 0)
                )
            }
            completion()
            parentView.layoutIfNeeded()
        })
    }
}
