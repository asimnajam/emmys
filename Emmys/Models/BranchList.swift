//
//  BranchList.swift
//  Emmys
//
//  Created by Syed Asim Najam on 31/01/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import RealmSwift

final class BranchList: Object, Decodable {
    @objc dynamic var flag: Int = 0
    @objc dynamic var code: Int = 0
    @objc dynamic var branch: String = ""
    @objc dynamic var city: String = ""
    @objc dynamic var address: String = ""
    @objc dynamic var location: String = ""
    
//    override class func primaryKey() -> String? {
//        return "code"
//    }
    
    enum CodingKeys: String, CodingKey {
        case flag = "Flag"
        case code = "CODE"
        case branch = "BRANCH"
        case city = "CITY"
        case address = "ADDRESS"
        case location = "BRANCH_LOCATION"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        flag = tryOrNil {
            try container.decode(Int.self, forKey: .flag)
            } ?? 0
        code = tryOrNil {
            try container.decode(Int.self, forKey: .code)
            } ?? 0
        branch = tryOrNil {
            try container.decode(String.self, forKey: .branch)
            } ?? ""
        city = tryOrNil {
            try container.decode(String.self, forKey: .city)
            } ?? ""
        address = tryOrNil {
            try container.decode(String.self, forKey: .address)
            } ?? ""
        location = container.safeDecode(.location) ?? ""
    }
    
    public required init() {}
    
    static var branchesInfo: [BranchList] {
        let realmDB = Database.realmDb()
        let branches = realmDB.objects(BranchList.self)
        return Array(branches)
    }
    
    static func save(branchList: [BranchList]) {
        let realmDB = Database.realmDb()
        realmDB.write(onError: Database.funcAndLine) {
            realmDB.add(branchList)
        }
    }
}

extension Array where Element: BranchList {
    var cities: [String] {
        return compactMap { $0.city }.unique
    }
    
    var addresses: [String] {
        return compactMap { $0.address }.unique
    }
    
    var branches: [String] {
        return compactMap { $0.branch }.unique
    }
    
    func addressesBy(city: String) -> [String] {
        let addresses = filter { $0.city == city }
        return addresses.addresses
    }
    
    func branchBy(city: String, address: String) -> [String] {
        let branches = filter { $0.city == city && $0.address == address }
        return branches.branches
    }
    
    func selectedLocationBy(city: String, address: String, branch: String) -> BranchList? {
        return first(where: { $0.city == city && $0.address == address && $0.branch == branch })
    }
}
