//
//  ContactUsView.swift
//  Emmys
//
//  Created by Asim Najam on 2/9/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class ContactUsView: UIView {
    let selectCityText = LocalizedLanguage.select_city
    let selectAddressText = LocalizedLanguage.select_address
    let selectBranchText = LocalizedLanguage.branch
    
    let header: BorderedView = {
        let view = BorderedView()
        view.configure(type: .normal)
        view.mainIntroLabel.text = LocalizedLanguage.contact_us
        return view
    }()
    let segmentedControl: UISegmentedControl = {
        let segmentItems = [LocalizedLanguage.feedback_and_complaints, LocalizedLanguage.business_opportunities]
        let control = UISegmentedControl(items: segmentItems)
        control.selectedSegmentIndex = 0
        return control
    }()
    
    let nameTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .name, type: .bordered)
        return textField
    }()
    
    let mobileTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .mobile(type: .signup), type: .bordered)
        return textField
    }()
    
    let emailTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .email, type: .bordered)
        return textField
    }()
    
    let cityButton: UIButton = {
        let button = SecondryButton()
        button.setTitle(LocalizedLanguage.city, for: .normal)
        return button
    }()
    
    let addressButton: UIButton = {
        let button = SecondryButton()
        button.setTitle(LocalizedLanguage.location, for: .normal)
        return button
    }()
    
    let branchButton: UIButton = {
        let button = SecondryButton()
        button.setTitle(LocalizedLanguage.branch, for: .normal)
        return button
    }()
    
    let feedbackTextView: UITextView = {
        let textView = UITextView()
        textView.text = LocalizedLanguage.write_your_complaints_and_feedback
        textView.textColor = UIColor.lightGray
        textView.font = UIFont.mediumFont
        return textView
    }()
    
    let submitButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle(LocalizedLanguage.submit, for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 20.0, leading: 20.0, bottom: 0.0, trailing: 20.0)
        let width = (frame.width - 30.0) / 2
        add(views:
            [
                header,
                segmentedControl,
                nameTextField,
                mobileTextField,
                emailTextField,
                cityButton,
                addressButton,
                branchButton,
                feedbackTextView,
                submitButton
            ]
        )
        
        header.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
            make.trailingMargin.equalToSuperview()
            make.leadingMargin.equalToSuperview()
            make.height.equalTo(100.0)
        }
        
        segmentedControl.snp.makeConstraints { make in
            make.top.equalTo(header.snp.bottom).offset(10.0)
            make.trailingMargin.equalToSuperview()
            make.leadingMargin.equalToSuperview()
            make.height.equalTo(40.0)
        }
        
        
        
        nameTextField.snp.makeConstraints { make in
            make.top.equalTo(segmentedControl.snp.bottom).offset(10.0)
            make.leadingMargin.equalToSuperview()
            make.width.equalTo(width)
            make.height.equalTo(40.0)
        }
        
        mobileTextField.snp.makeConstraints { make in
            make.top.equalTo(nameTextField)
            make.trailingMargin.equalToSuperview()
            make.width.equalTo(width)
            make.height.equalTo(40.0)
        }
        
        emailTextField.snp.makeConstraints { make in
            make.top.equalTo(nameTextField.snp.bottom).offset(10.0)
            make.leadingMargin.equalToSuperview()
            make.width.equalTo(width)
            make.height.equalTo(40.0)
        }
        
        cityButton.snp.makeConstraints { make in
            make.top.equalTo(emailTextField)
            make.trailingMargin.equalToSuperview()
            make.width.equalTo(width)
            make.height.equalTo(40.0)
        }
        
        addressButton.snp.makeConstraints { make in
            make.top.equalTo(emailTextField.snp.bottom).offset(10.0)
            make.leadingMargin.equalToSuperview()
            make.width.equalTo(width)
            make.height.equalTo(40.0)
        }
        branchButton.snp.makeConstraints { make in
            make.top.equalTo(addressButton)
            make.trailingMargin.equalToSuperview()
            make.width.equalTo(width)
            make.height.equalTo(40.0)
        }
        
        feedbackTextView.snp.makeConstraints { make in
            make.top.equalTo(branchButton.snp.bottom).offset(10.0)
            make.trailingMargin.equalToSuperview()
            make.leadingMargin.equalToSuperview()
            make.bottom.greaterThanOrEqualTo(submitButton.snp.top).inset(-10.0)
        }
        
        submitButton.snp.makeConstraints { make in
            make.leadingMargin.equalToSuperview()
            make.height.equalTo(44.0)
            make.width.equalTo(width)
            make.bottom.equalToSuperview().inset(20.0)
        }
        feedbackTextView.layer.cornerRadius = 5.0
        feedbackTextView.layer.borderColor = Colors.blueColor.cgColor
        feedbackTextView.layer.borderWidth = 1.0
        feedbackTextView.textAlignment = emmysManager.currentLanguage == .arabic ? .right : .left
    }

}
