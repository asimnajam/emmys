//
//  AuthModel.swift
//  Emmys
//
//  Created by Asim Najam on 2/23/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift
import PromiseKit

let userDefaultMobileKey = "MobileNumber"
final class AuthModel {
    
    private(set) var loginAPIPubSubject = PublishSubject<(String, Bool)>()
    
    func login(mobileNumber: String, password: String) {
        APICommunicator.login(mobile: mobileNumber, password: password)
        .done({ user in
            guard let user = user else { return }
            self.getUser(user.id, mobileNumber: mobileNumber)
        })
        .catch { [weak self] error in
            self?.loginAPIPubSubject.onNext(("Login Failed! Invalid email or password", false))
        }
    }
    
    func getUser(_ userID: String, mobileNumber: String) {
        APICommunicator.getUserInfo(customerID: userID)
        .then { userInfo -> Promise<[OfferList]> in
            UserInfo.addMobile(mobile: mobileNumber)
            return APICommunicator.getOfferLists()
        }.then { _ -> Promise<[BranchList]> in
            APICommunicator.getBranchList()
        }.done { _ in
            self.setRootControllerAfterLogin()
        }.catch { _ in
            
        }
    }
    
    private func setRootControllerAfterLogin() {
        let navController = TabBarController(isShowWalletView: true)
        if let window = UIApplication.shared.keyWindow {
            navController.view.frame = window.bounds
            UIView.transition(
                with: window,
                duration: 0.5,
                options: [.allowAnimatedContent, .transitionFlipFromRight],
                animations: {
                    window.rootViewController = navController
                },
                completion: nil
            )
        }
    }
}
