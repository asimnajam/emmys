//
//  LocalizationManager.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import UIKit

let LanguageKey = "AppleLanguage"
enum SupportingLangauges: String {
    case english = "en"
    case arabic = "ar"
    
    var languageCode: String {
        switch self {
        case .arabic:
            return "ar"
        case .english:
            return "en"
        }
    }
    
    var sementicContentAttribution: UISemanticContentAttribute {
        switch self {
        case .english:
            return .forceLeftToRight
        default:
            return .forceRightToLeft
        }
    }
}

//final class LocalizationManager:NSObject {
//
//    var bundle: Bundle!
//
//    class var sharedInstance: LocalizationManager {
//        struct Singleton {
//            static let instance: LocalizationManager = LocalizationManager()
//        }
//        return Singleton.instance
//    }
//
//    override init() {
//        super.init()
//        bundle = Bundle.main
//    }
//
//    func localizedStringForKey(key: String, comment: String = "") -> String {
//        return bundle.localizedString(forKey: key, value: comment, table: nil)
//    }
//
//    func localizedImagePathForImg(imagename: String, type: String) -> String {
//        guard let imagePath =  bundle.path(forResource: imagename, ofType: type) else {
//            return ""
//        }
//        return imagePath
//    }
//
//    //MARK:- setLanguage
//    // Sets the desired language of the ones you have.
//    // If this function is not called it will use the default OS language.
//    // If the language does not exists y returns the default OS language.
//    func setLanguage(languageCode: String) {
//        var appleLanguages = UserDefaults.standard.object(forKey: "AppleLanguages") as! [String]
//        appleLanguages.remove(at: 0)
//        appleLanguages.insert(languageCode, at: 0)
//        UserDefaults.standard.set(appleLanguages, forKey: "AppleLanguages")
//        UserDefaults.standard.synchronize() //needs restrat
//
//        if let languageDirectoryPath = Bundle.main.path(forResource: languageCode, ofType: "lproj")  {
//            bundle = Bundle.init(path: languageDirectoryPath)
//        } else {
//            resetLocalization()
//        }
//    }
//
//    //MARK:- resetLocalization
//    //Resets the localization system, so it uses the OS default language.
//    func resetLocalization() {
//        bundle = Bundle.main
//    }
//
//    //MARK:- getLanguage
//    // Just gets the current setted up language.
//    func getLanguage() -> String {
//        let appleLanguages = UserDefaults.standard.object(forKey: "AppleLanguages") as! [String]
//        let prefferedLanguage = appleLanguages[0]
//        if prefferedLanguage.contains("-") {
//            let array = prefferedLanguage.components(separatedBy: "-")
//            return array[0]
//        }
//        return prefferedLanguage
//    }
//}

final class LocalizationManager:NSObject {
    
    var bundle: Bundle!
    
    class var sharedInstance: LocalizationManager {
        struct Singleton {
            static let instance: LocalizationManager = LocalizationManager()
        }
        return Singleton.instance
    }
    
    override init() {
        super.init()
        bundle = Bundle.main
    }
    
    func abcd() {
//        let locaisedString = NSLocalizedString("Hello", tableName: nil, bundle: bundle, value: "", comment: "")
//        print(locaisedString)
        
        print(bundle.localizedString(forKey: "name", value: nil, table: nil))
//        let localizedTextKey = "Hey"
//        if let bundlePath = Bundle.main.path(forResource: "ar", ofType: "lproj"), let bundle = Bundle(path: bundlePath) {
//            print(NSLocalizedString(localizedTextKey, tableName: nil, bundle: bundle, comment: ""))
//        } else {
//            print( NSLocalizedString(localizedTextKey, comment: ""))
//        }

        

    }
    
    func localizedStringForKey(key: String, comment: String = "") -> String {
        return bundle.localizedString(forKey: key, value: comment, table: nil)
    }
    
    func localizedImagePathForImg(imagename: String, type: String) -> String {
        guard let imagePath =  bundle.path(forResource: imagename, ofType: type) else {
            return ""
        }
        return imagePath
    }
    
    //MARK:- setLanguage
    // Sets the desired language of the ones you have.
    // If this function is not called it will use the default OS language.
    // If the language does not exists y returns the default OS language.
    func setLanguage(languageCode: String) {
//        var appleLanguages = UserDefaults.standard.object(forKey: LanguageKey) as? String
//        appleLanguages.remove(at: 0)
//        appleLanguages.insert(languageCode, at: 0)
        UserDefaults.standard.set(languageCode, forKey: LanguageKey)
        UserDefaults.standard.synchronize() //needs restrat
        
        if let languageDirectoryPath = Bundle.main.path(forResource: languageCode, ofType: "lproj")  {
            bundle = Bundle.init(path: languageDirectoryPath)
        } else {
            resetLocalization()
        }
    }
    
    //MARK:- resetLocalization
    //Resets the localization system, so it uses the OS default language.
    func resetLocalization() {
        bundle = Bundle.main
    }
    
    //MARK:- getLanguage
    // Just gets the current setted up language.
    
    var currentLangauge: SupportingLangauges {
        guard let langauge = UserDefaults.standard.object(forKey: LanguageKey) as? String,
              let currentLanguage = SupportingLangauges(rawValue: langauge)
            else {
            return SupportingLangauges.english
        }
        return currentLanguage
    }
    func getLanguage() -> SupportingLangauges {
        guard let langauge = UserDefaults.standard.object(forKey: "LanguageKey") as? String,
              let currentLanguage = SupportingLangauges(rawValue: langauge)
            else {
            return SupportingLangauges.english
        }
        return currentLanguage
//        let prefferedLanguage = appleLanguages[0]
//        if prefferedLanguage.contains("-") {
//            let array = prefferedLanguage.components(separatedBy: "-")
//            return array[0]
//        }
//        return prefferedLanguage
    }
}

