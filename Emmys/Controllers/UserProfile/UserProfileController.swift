//
//  UserProfileController.swift
//  Emmys
//
//  Created by Asim Najam on 2/9/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class UserProfileController: UIViewController {
    private let contentView = UserProfileView(frame: .zero)
    private var userInfo: UserInfo?
    private lazy var imagePicker = ImagePicker(
        presentationController: self,
        delegate: self
    )
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        setUserInfo()
        contentView.submitButton.addTarget(
            self,
            action: #selector(updateProfile),
            for: .touchUpInside
        )
        contentView.userImageButton.addTarget(
            self,
            action: #selector(showPickerView(_:)),
            for: .touchUpInside
        )
        setEmmysLogo()
    }
    
    func setUserInfo() {
        guard let userInfo = emmysManager.user else { return }
        contentView.emailTextField.text = userInfo.userEmail
        contentView.nameTextField.text = userInfo.userName
        contentView.mobileTextField.text = userInfo.mobileNumber
        contentView.userImageButton.setBackgroundImage(emmysManager.userImage, for: .normal)
    }
    
    @objc func showPickerView(_ sender: UIButton) {
        imagePicker.present(from: sender)
    }
    
    @objc func updateProfile() {
        guard let name = contentView.nameTextField.text, !name.isEmpty else {
            Toast.show(style: .error(message: "Please enter your name"), view: view)
            return
        }

        guard let email = contentView.emailTextField.text, !email.isEmpty else {
            Toast.show(style: .error(message: "Please enter your email"), view: view)
            return
        }

        guard let mobile = contentView.mobileTextField.text, !mobile.isEmpty else {
            Toast.show(style: .error(message: "Please enter your mobile number"), view: view)
            return
        }
        updateUserInfo(name: name, mobile: mobile, email: email)
        contentView.submitButton.isEnabled = false
    }
    
    func updateUserInfo(name: String, mobile: String, email: String) {
        guard let customerID = emmysManager.activeUserID else { return }
        APICommunicator.updateUser(
            customerID: customerID,
            name: name.removeWhiteSpaces(),
            mobile: mobile,
            email: email.removeWhiteSpaces()
        ) { [weak self] result in
            guard let self = self else { return }
            self.contentView.submitButton.isEnabled = true
            switch result {
            case .success(let isUpdated):
                if isUpdated {
                    UserInfo.update(name: name, email: email, mobile: mobile)
                    self.setUserInfo()
                    Toast.show(style: .info(message: LocalizedLanguage.your_profile_has_been_updated), view: self.view)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension UserProfileController: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        contentView.userImageButton.setBackgroundImage(image, for: .normal)
        UserImage.updateUserImage(image: image?.pngData())
        
    }
}
