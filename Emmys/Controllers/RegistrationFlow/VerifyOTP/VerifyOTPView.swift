//
//  VerifyOTPView.swift
//  Emmys
//
//  Created by Syed Asim Najam on 02/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit


class VerifyOTPView: UIView {
//    let closeButton: UIButton = {
//        let button = UIButton()
//        button.setImage(UIImage(named: "closeIcon"), for: .normal)
//        return button
//    }()
    lazy var otpTextFieldsStackView: UIStackView = {
        let subViews = [firstOTP, secondtOTP, thirdOTP, fourthOTP]
        let stackView = UIStackView(arrangedSubviews: subViews)
        stackView.spacing = 3.0
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    let firstOTP: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = Colors.blueColor
        textField.textColor = .white
        textField.keyboardType = .numberPad
        textField.font = UIFont.mediumFont
        textField.textAlignment = .center
        textField.tag = 1
        return textField
    }()
    
    let secondtOTP: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = Colors.blueColor
        textField.textColor = .white
        textField.keyboardType = .numberPad
        textField.font = UIFont.mediumFont
        textField.textAlignment = .center
        textField.tag = 2
        return textField
    }()
    
    let thirdOTP: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = Colors.blueColor
        textField.textColor = .white
        textField.font = UIFont.mediumFont
        textField.keyboardType = .numberPad
        textField.textAlignment = .center
        textField.tag = 3
        return textField
    }()
    
    let fourthOTP: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = Colors.blueColor
        textField.textColor = .white
        textField.font = UIFont.mediumFont
        textField.keyboardType = .numberPad
        textField.textAlignment = .center
        textField.tag = 4
        return textField
    }()
    
    let headerView: HeaderView = {
        let view = HeaderView(frame: .zero)
        view.configureWithTitle(title: LocalizedLanguage.verification)
        return view
    }()
    
    let verificationInfoLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.smallFont
        label.text = LocalizedLanguage.a_verification_pin_has_been_sent_throught_sms
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    let thisMightTakeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.xSmallFont
        label.text = LocalizedLanguage.this_might_take_a_few_minutes
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    
    lazy var resendOTPLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.xSmallFont
        label.text = LocalizedLanguage.didnot_get_the_code_yet
        label.textColor = Colors.blueColor
        return label
    }()
    
    let resnedOTPButton: UIButton = {
        let button = UIButton()
        button.setTitle(LocalizedLanguage.resend_code, for: .normal)
        button.setTitleColor(Colors.blueColor, for: .normal)
        button.titleLabel?.font = UIFont.smallBoldFont
        return button
    }()
    
    let verifyOTPButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle(LocalizedLanguage.verify, for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 0, leading: 30, bottom: 0, trailing: 30)
       
        let offsetSmall = 25.0
        let offsetBig = 45.0
        add(views: [
            headerView,
//            closeButton,
            verificationInfoLabel,
            otpTextFieldsStackView,
            thisMightTakeLabel,
            resendOTPLabel,
            resnedOTPButton,
            verifyOTPButton
        ])
        
//        closeButton.snp.makeConstraints { make in
//            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
//            make.width.height.equalTo(44.0)
//            make.leading.equalToSuperview()
//        }
        headerView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)//.offset(10.0)
            make.leadingMargin.trailingMargin.equalToSuperview()
        }
        
        verificationInfoLabel.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom)//.offset(offsetSmall)
            
        }
        
        otpTextFieldsStackView.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(verificationInfoLabel.snp.bottom).offset(offsetSmall)
            make.height.equalTo(60.0)
        }
        
        thisMightTakeLabel.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(otpTextFieldsStackView.snp.bottom).offset(10)
            
        }
        
        resendOTPLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(50.0)
            make.top.equalTo(thisMightTakeLabel.snp.bottom).offset(offsetBig)

        }

        resnedOTPButton.snp.makeConstraints { make in
            make.leading.equalTo(resendOTPLabel.snp.trailing).offset(5.0)
            make.centerY.equalTo(resendOTPLabel)

        }
       
        verifyOTPButton.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(resnedOTPButton.snp.bottom)//.offset(offsetSmall)
            make.height.equalTo(50.0)
        }
    }
    
    func setVerifyOTPView() {
        headerView.configureWithTitle(title: LocalizedLanguage.verification)
//        verificationTitleLabel.text = "Verfication"
        verificationInfoLabel.text = LocalizedLanguage.a_verification_pin_has_been_sent_throught_sms
        thisMightTakeLabel.text = LocalizedLanguage.this_might_take_a_few_minutes
        resendOTPLabel.text = LocalizedLanguage.didnot_get_the_code_yet
        resnedOTPButton.setTitle(LocalizedLanguage.resend_code, for: .normal)
        verifyOTPButton.setTitle(LocalizedLanguage.verify, for: .normal)
    }
    
    func setResendOTPView() {
        headerView.configureWithTitle(title: LocalizedLanguage.verification_code)
        verificationInfoLabel.text = LocalizedLanguage.please_enter_sms_code_sent_to_your_mobile
        thisMightTakeLabel.isHidden = true
        resendOTPLabel.isHidden = true
        resnedOTPButton.isHidden = true
        verifyOTPButton.setTitle(LocalizedLanguage.enter, for: .normal)
    }
}
