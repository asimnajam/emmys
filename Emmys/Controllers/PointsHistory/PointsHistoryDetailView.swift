//
//  PointsHistoryDetailView.swift
//  Emmys
//
//  Created by Syed Asim Najam on 04/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class PointsHistoryDetailView: UIView {
    let detailLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.largeFont
        label.textColor = Colors.blueColor
        label.text = LocalizedLanguage.details
        label.textAlignment = .center
        return label
    }()
    
    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.directionalLayoutMargins = .init(top: 10.0, leading: 0.0, bottom: 10.0, trailing: 0.0)
        return view
    }()
    
    lazy var verticalStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            dateDetailLabel,
            branchDetailLabel,
            ammountDetailLabel,
            pointsDetailLabel
        ])
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        return stackView
    }()
    
    lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "closeIcon"), for: .normal)
        button.addTarget(self, action: #selector(closeButtonAction), for: .touchUpInside)
        return button
    }()
    
    let dateDetailLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.mediumFont
        label.textColor = Colors.blueColor
        label.textAlignment = .left
        return label
    }()
    
    let branchDetailLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.mediumFont
        label.textColor = Colors.blueColor
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    let ammountDetailLabel: UILabel = {
           let label = UILabel()
           label.font = UIFont.mediumFont
           label.textColor = Colors.blueColor
           label.textAlignment = .left
           return label
       }()
    
    let pointsDetailLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.mediumFont
        label.textColor = Colors.blueColor
        label.textAlignment = .left
        return label
    }()
    
    override init(frame: CGRect) {
          super.init(frame: frame)
          setupView()
      }
      
      required init?(coder: NSCoder) {
          fatalError("init(coder:) has not been implemented")
      }

    private func setupView() {
        backgroundColor = UIColor(displayP3Red: CGFloat(242.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(244.0/255.0), alpha: 0.8)
        
        verticalStackView.distribution = .fillEqually
        verticalStackView.axis = .vertical
        
        add(views: [containerView, closeButton])
        
        containerView.add(views: [detailLabel, verticalStackView])
        
        detailLabel.snp.makeConstraints { make in
//            make.width.equalToSuperview().multipliedBy(0.9)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(containerView.snp.top)
        }
        
        containerView.snp.makeConstraints { make in
            make.width.equalToSuperview().multipliedBy(0.9)
            make.height.equalTo(containerView.snp.width)
            make.center.equalToSuperview()
        }
            
        verticalStackView.snp.makeConstraints { make in
            make.margins.equalToSuperview()
        }
        closeButton.snp.makeConstraints { make in
            make.bottom.equalTo(containerView.snp.top).offset(-5.0)
            make.width.height.equalTo(44.0)
            make.leading.equalToSuperview().offset(5.0)
        }
        containerView.layer.borderColor = Colors.blueColor.cgColor
        containerView.layer.borderWidth = 1.0
        layoutIfNeeded()
    }
    
    func configure(date: String, branch: String, ammount: String, point: String) {
        dateDetailLabel.text = "   Date: \(date)"
        branchDetailLabel.text = "   Branch: \(branch)"
        ammountDetailLabel.text = "   Amount: \(ammount)"
        pointsDetailLabel.text = "   Point: \(point)"
    }
    
    @objc func closeButtonAction() {
        removeFromSuperview()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        addBorder(label: dateDetailLabel)
        addBorder(label: branchDetailLabel)
        addBorder(label: ammountDetailLabel)
    }
    
    func addBorder(label: UILabel) {
        let border = CALayer()
        let width = containerView.frame.width
        border.backgroundColor = Colors.blueColor.cgColor
        border.frame = CGRect(
            x: 0,
            y: (width/4) - 1,
            width: width,
            height: 1
        )
        label.layer.addSublayer(border)
    }
}
