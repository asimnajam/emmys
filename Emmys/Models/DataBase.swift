//
//  DataBase.swift
//  Emmys
//
//  Created by Asim Najam on 3/15/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import RealmSwift

public final class Database {
    public var realmObj: Realm?
    static let funcAndLine = "FileName: \(#file), Function: \(#function), line: \(#line)"
    
    public class var shared: Database {
        struct Static {
            static let instance: Database = Database()
        }
        return Static.instance
    }

    public static let defaultConfig = Realm.Configuration.defaultConfiguration

    public static func realmDb(config: Realm.Configuration = defaultConfig) -> Realm {
        if let realmObj = Database.shared.realmObj, Database.shared.realmObj != nil {
            return realmObj
        } else {
            do {
                Database.shared.realmObj = try Realm(configuration: config)
                return Database.shared.realmObj!
            } catch let error as NSError {
                print("\(error)")
                // Try again, by deleting and re-creating; this time
                try? FileManager().removeItem(at: config.fileURL!)
                Database.shared.realmObj = try? Realm(configuration: config)
                return Database.shared.realmObj!
            }
        }
    }

    public static func reset() {
        let realm = Database.realmDb()
        realm.write(onError: funcAndLine) {
            realm.deleteAll()
        }
    }
}

extension Realm {
    public func write(onError: String?, _ block: () -> Void) {
        do {
            try write {
                block()
            }
        } catch {
            print("Realm Error executing a write: \(onError ?? ""): \(error.localizedDescription)")
        }
    }
}
