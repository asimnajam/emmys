//
//  BorderedButton.swift
//  Emmys
//
//  Created by Asim Najam on 2/9/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

final class BorderedButton: UIButton {
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func setupViews() {
        backgroundColor = UIColor.white
        setTitleColor(Colors.blueColor, for: .normal)
//        contentHorizontalAlignment = .left
    }
    
    public override var isEnabled: Bool {
        didSet {
            let color = isEnabled ? Colors.blueColor : UIColor.gray
            setTitleColor(color, for: .normal)
//                = isEnabled ? Colors.blueColor : UIColor.gray
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
//        let border = CALayer()
//        border.backgroundColor = UIColor.black.cgColor
//        border.frame = CGRect(
//            x: 0,
//            y: self.frame.size.height - 1,
//            width: self.frame.size.width,
//            height: 1
//        )
//        layer.addSublayer(border)
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.gray.cgColor
        layer.cornerRadius = 5.0
    }
}
