//
//  MainController.swift
//  Emmys
//
//  Created by Syed Asim Najam on 04/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class MainController: UIViewController {
    let contentView = MainView(frame: .zero)
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        view.layoutIfNeeded()
        applyNavigationBarStyle(UIViewController.NavigationBarStyle.transparent)
        contentView.enterButton.addTarget(self, action: #selector(enterButtonAction), for: .touchUpInside)
    }
    
    @objc func enterButtonAction() {
//        userDefault.set("English", forKey: languageSelectorKey)
//        emmysManager.changeLanguage(language: SupportingLangauges.english)
        navigationController?.pushViewController(
            SelectLanguageController(type: .registration),
            animated: true
        )
    }
}
