//
//  PointsHistoryView.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class PointsHistoryView: UIView {
    let containerView: UIView = {
        let view = UIView()
        view.layer.borderColor = Colors.blueColor.cgColor
        view.layer.borderWidth = 1.0
        return view
    }()
    let totalPointLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Colors.blueColor
        label.font = UIFont.xlargeFont
        label.numberOfLines = 2
        return label
    }()
    
    let pointHistoryLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Colors.blueColor
        label.font = UIFont.xlargeFont
        label.text = LocalizedLanguage.points_history
        label.numberOfLines = 2
        label.layer.borderColor = Colors.blueColor.cgColor
        label.layer.borderWidth = 1.0
        return label
    }()
    
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60.0
        tableView.separatorInset = .zero
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.tableFooterView = UIView()
        tableView.tableHeaderView = UIView()
        return tableView
    }()
   override init(frame: CGRect) {
       super.init(frame: frame)
       setupView()
   }
   
   required init?(coder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
   }

    func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 0, leading: 20.0, bottom: 0, trailing: 20.0)
        addSubview(containerView)
        addSubview(tableView)
        containerView.add(views: [totalPointLabel, pointHistoryLabel])
        containerView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.height.equalTo(200.0)
        }
        totalPointLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        pointHistoryLabel.snp.makeConstraints { make in
            make.width.equalToSuperview().multipliedBy(0.6)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(10.0)
        }
        
        tableView.snp.makeConstraints { make in
            make.top.equalTo(containerView.snp.bottom).offset(10.0)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottom)
            make.leadingMargin.trailingMargin.equalToSuperview()
        }
//        totalPointLabel.text = "Total Points \n90.0"
    }
    
    func setTotalPoints(points: Float) {
        totalPointLabel.text = "\(LocalizedLanguage.total_points) \n\(points)"
    }
}
