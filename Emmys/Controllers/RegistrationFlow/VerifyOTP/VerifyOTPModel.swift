//
//  VerifyOTPModel.swift
//  Emmys
//
//  Created by Asim Najam on 2/15/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation


final class VerifyOTPModel {
    enum Kind {
        case registration(id: String)
        case forgetPassword
    }
    private(set) var customerID: String? = ""
    private(set) var type: Kind
    let keepLogin: Bool
    
    init(type: Kind, keepLogin: Bool = false) {
        switch type {
        case .registration(let id):
            self.customerID = id
        case .forgetPassword:
            break
        }
        self.type = type
        self.keepLogin = keepLogin
    }
}
