//
//  ProductView.swift
//  Emmys
//
//  Created by Amir on 2/6/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class ProductView: UIView {
    static let menuCollectionViewInterimSpacing: CGFloat = 10.0
    let menuContainerView: UIView = {
        let view = UIView()
        view.directionalLayoutMargins = .init(top: 5.0, leading: 5.0, bottom: 5.0, trailing: 5.0)
        return view
    }()
    let menuCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = menuCollectionViewInterimSpacing
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.white
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.white
        collectionView.showsVerticalScrollIndicator = false
        return collectionView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor  = .white
        add(views: [menuContainerView, collectionView])
        menuContainerView.addSubview(menuCollectionView)
        menuContainerView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(20.0)
            make.leading.equalToSuperview().offset(30.0)
            make.trailing.equalToSuperview().inset(30.0)
            make.height.equalTo(44.0)
        }
        
        menuCollectionView.snp.makeConstraints { make in
            make.margins.equalToSuperview()
        }
        
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(menuCollectionView.snp.bottom).offset(20.0)
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
            make.bottom.equalToSuperview()
        }
    }
}
