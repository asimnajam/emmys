//
//  SideMenuView.swift
//  Emmys
//
//  Created by Asim Najam on 2/8/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class SideMenuView: UIView {
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60.0
        tableView.separatorInset = .zero
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.tableFooterView = UIView()
        tableView.tableHeaderView = UIView()
        tableView.isScrollEnabled = false
        return tableView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        backgroundColor = .white
        addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(44.0)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottom)
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
        }
    }

}
