//
//  MainView.swift
//  Emmys
//
//  Created by Syed Asim Najam on 04/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class MainView: UIView {
    private let emmysLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "logo")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let enterButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle(LocalizedLanguage.enter, for: .normal)
        button.setTitleColor(.white, for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        add(views: [emmysLogo, enterButton])
        emmysLogo.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.9)
            make.height.equalTo(emmysLogo.snp.width)
        }
        enterButton.snp.makeConstraints { make in
            make.width.equalToSuperview().multipliedBy(0.5)
            make.height.equalTo(44.0)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottom).inset(20.0)
            make.centerX.equalToSuperview()
        }
    }
    
}
