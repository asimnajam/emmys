//
//  Loader.swift
//  Emmys
//
//  Created by Asim Najam on 2/23/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

extension UIView {
    func showLoader() {
        let blurLoader = Loader(frame: frame)
        self.addSubview(blurLoader)
    }

    func hideLoader() {
        if let blurLoader = subviews.first(where: { $0 is Loader }) {
            blurLoader.removeFromSuperview()
        }
    }
    
}

class Loader: UIView {

    var blurEffectView: UIView?
    //UIVisualEffectView?

    override init(frame: CGRect) {
//        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIView()
            //UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = frame
        blurEffectView.backgroundColor = .clear
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.blurEffectView = blurEffectView
        super.init(frame: frame)
        addSubview(blurEffectView)
        addLoader()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addLoader() {
        guard let blurEffectView = blurEffectView else { return }
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.color = .gray
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        blurEffectView.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(100.0)
        }
//        activityIndicator.center = blurEffectView.center
        activityIndicator.startAnimating()
    }
}
