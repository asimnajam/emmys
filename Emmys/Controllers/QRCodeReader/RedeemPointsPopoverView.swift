//
//  RedeemPointsPopoverView.swift
//  Emmys
//
//  Created by Asim Najam on 3/10/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class RedeemPointsPopoverView: UIView {
    private let containerView = UIView()
    private let blurView = UIView()
    private let redeemButtonPubSubject = PublishSubject<Void>()
    private(set) lazy var redeemButtonObs = redeemButtonPubSubject.asObservable()
    
    private let congratsLabel: UILabel = {
        let label = UILabel()
        label.text = LocalizedLanguage.congratulations
        label.textAlignment = .center
        return label
    }()
    
    private let introLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    
    private let redeemButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle(LocalizedLanguage.redeem, for: .normal)
        button.addTarget(self, action: #selector(redeemButtonAction), for: .touchUpInside)
        return button
    }()
    
    private let ignoreButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle(LocalizedLanguage.ignore, for: .normal)
        button.addTarget(self, action: #selector(ignoreButtonAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [redeemButton, ignoreButton])
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.spacing = 5.0
        return stackView
    }()
    
    init(point: Int) {
        super.init(frame: .zero)
        setupView()
        configure(point)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        blurView.backgroundColor = .black
        blurView.alpha = 0.3
        containerView.backgroundColor = .white
        containerView.directionalLayoutMargins = .init(top: 30.0, leading: 10.0, bottom: 10.0, trailing: 10.0)
        containerView.layer.cornerRadius = 10.0
        containerView.layer.masksToBounds = true
        alpha = 0.0
        
        addSubview(blurView)
        addSubview(containerView)
        
        blurView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        containerView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.7)
            make.height.equalTo(150.0)
        }
        
        containerView.add(views: [congratsLabel, introLabel, buttonsStackView])
        
        congratsLabel.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.topMargin.equalToSuperview()
        }
        
        introLabel.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(congratsLabel.snp.bottom).offset(10.0)
        }
        
        buttonsStackView.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.bottomMargin.equalToSuperview()
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
        addGestureRecognizer(tapGesture)
        showView()
    }
    
    private func showView() {
        UIView.animate(withDuration: 0.3, delay: 0.25, options: .curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: nil)
    }
    
    @objc private func tapGestureAction() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: { _ in
            self.removeFromSuperview()
        })
    }
    
    private func configure(_ userBalancePoint: Int) {
        introLabel.text = "\(LocalizedLanguage.your_points_reach_upto) \(userBalancePoint)"
    }

    static func setAlertPoint(point: Int) {
        userDefault.set(point, forKey: "LastAlertPoint")
    }
    
    static var getAlertPoint: Int {
        userDefault.integer(forKey: "LastAlertPoint")
    }
    
    @objc private func ignoreButtonAction() {
        tapGestureAction()
    }
    
    @objc private func redeemButtonAction() {
        tapGestureAction()
        redeemButtonPubSubject.onNext(())
    }
}
