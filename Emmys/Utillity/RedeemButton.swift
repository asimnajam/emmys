//
//  RedeemButton.swift
//  Emmys
//
//  Created by Asim Najam on 2/16/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

final class RedeemButton: UIButton {
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override var isEnabled: Bool {
        didSet {
                    
            if isEnabled {
                setEnableButton()
            } else {
                setDisableButton()
            }
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        if isEnabled {
            setEnableButton()
        } else {
            setDisableButton()
        }
//        layer.cornerRadius = 5.0
    }
    
    private func setDisableButton() {
        backgroundColor = UIColor.white
        setTitleColor(Colors.blueColor, for: .normal)
//        contentHorizontalAlignment = .left
        let border = CALayer()
        border.backgroundColor = UIColor.gray.cgColor
        border.frame = CGRect(
            x: 0,
            y: self.frame.size.height - 1,
            width: self.frame.size.width,
            height: 1
        )
        layer.addSublayer(border)
//        if emmysManager.currentLanguage == .arabic {
//            contentHorizontalAlignment = .right
//        } else {
//            contentHorizontalAlignment = .left
//        }
    }
    
    private func setEnableButton() {
        backgroundColor = Colors.blueColor
        setTitleColor(.white, for: .normal)
        layer.cornerRadius = 5.0
        contentHorizontalAlignment = .center
    }
}
