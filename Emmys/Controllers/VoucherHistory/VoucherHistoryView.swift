//
//  VoucherHistoryView.swift
//  Emmys
//
//  Created by Asim Najam on 2/21/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class VoucherHistoryView: UIView {
    let voucherHistoryLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Colors.blueColor
        label.font = UIFont.xlargeFont
        label.text = LocalizedLanguage.voucherHistory
        return label
    }()
    
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .white
        tableView.tableFooterView = UIView()
        tableView.tableHeaderView = UIView()
        return tableView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        directionalLayoutMargins = .init(top: 0, leading: 10.0, bottom: 0, trailing: 10.0)
        backgroundColor = .white
        add(views: [voucherHistoryLabel, tableView])
        
        voucherHistoryLabel.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.height.equalTo(100.0)
        }
        
        tableView.snp.makeConstraints { make in
            make.top.equalTo(voucherHistoryLabel.snp.bottom).offset(10.0)
            make.leadingMargin.trailingMargin.bottomMargin.equalToSuperview()
        }
        
    }

}
