//
//  AboutUsController.swift
//  Emmys
//
//  Created by Asim Najam on 2/9/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class AboutUsController: UIViewController {
//    let contentView = AboutUsView(frame: .zero)
    var contentView: AboutUsView?
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView = AboutUsView()
        guard let contentView = contentView else { return }
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        setEmmysLogo()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
