//
//  TabBarMenu.swift
//  Emmys
//
//  Created by Asim Najam on 2/28/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

enum TabBarMenu: String {
    case menu
    case points
    case scan
    case redeem
    
    private var width: CGFloat {
        30.0
    }
    
    private var selectedColor: UIColor {
        .white
    }
    
    private var normalColor: UIColor {
        .lightGray
    }
    
    private var imageForMenuItem: UIImage? {
        switch self {
        case .menu:
            return UIImage(named: "menu")
        case .points:
            return UIImage(named: "points2")
        case .scan:
            return UIImage(named: "bar-code")
        case .redeem:
            return UIImage(named: "reedem2")
        }
    }
    
    private var resizedImage: UIImage? {
        return self.imageForMenuItem?.resizeWith(width: width, height: width)
    }
    
    private var selectedImage: UIImage? {
        return self.resizedImage?.maskWithColor(color: selectedColor).withRenderingMode(.alwaysOriginal)
    }
    
    private var normalImage: UIImage? {
        return self.resizedImage?.maskWithColor(color: normalColor).withRenderingMode(.alwaysOriginal)
    }
    
    private var title: String {
        switch self {
        case .menu:
            return LocalizedLanguage.menu
        case .points:
            return LocalizedLanguage.points
        case .scan:
            return LocalizedLanguage.scan
        case .redeem:
            return LocalizedLanguage.redeem
        }
    }
    
    var tabBarItem: UITabBarItem {
        let barItem = UITabBarItem(
            title: self.title,
            image: self.normalImage,
            selectedImage: self.selectedImage
        )
        barItem.setTitleTextAttributes(
            [
                .foregroundColor : normalColor
            ],
            for: .normal
        )
        barItem.setTitleTextAttributes(
            [
                .foregroundColor : selectedColor
            ],
            for: .selected
        )
        return barItem
    }
}

