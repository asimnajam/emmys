//
//  PointsHistoryHeaderView.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class PointsHistoryHeaderView: UIView {
    
    lazy var otpTextFieldsStackView: UIStackView = {
        let subViews = [dateLabel, branchLabel, ammountLabel, pointsLabel, detailLabel]
        let stackView = UIStackView(arrangedSubviews: subViews)
        stackView.axis = .horizontal
        stackView.distribution = .fill
        return stackView
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.text = LocalizedLanguage.date
        label.font = UIFont.mediumFont
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    let branchLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.mediumFont
        label.text = LocalizedLanguage.branch
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    let ammountLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.mediumFont
        label.text = LocalizedLanguage.amount
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    let pointsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.mediumFont
        label.text = LocalizedLanguage.points
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    let detailLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.mediumFont
        label.text = LocalizedLanguage.detail
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 0, leading: 5.0, bottom: 0, trailing: 5.0)
        addSubview(otpTextFieldsStackView)
        otpTextFieldsStackView.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.height.equalTo(60.0)
        }
        
        dateLabel.snp.makeConstraints { make in
            make.width.equalTo(65.0)
        }
        
        branchLabel.snp.makeConstraints { make in
            make.width.greaterThanOrEqualTo(100.0)
        }
        
        ammountLabel.snp.makeConstraints { make in
            make.width.equalTo(70.0)
        }
        
        pointsLabel.snp.makeConstraints { make in
            make.width.equalTo(80.0)
        }
        
        detailLabel.snp.makeConstraints { make in
            make.width.equalTo(65.0)
        }
        
    }
    
}
