//
//  EmmysManager.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import UIKit

let userDefault = UserDefaults.standard
let languageSelectorKey = "SelectedLanguage"
let localizationManager = LocalizationManager.sharedInstance
let emmysManager = EmmysManager.sharedInstance

final class EmmysManager {
    static let sharedInstance = EmmysManager()
    
    var currentLanguage: SupportingLangauges {
        guard let lang = UserDefaults.standard.object(forKey: "AppleLanguage") as? String,
        let supportingLang = SupportingLangauges(rawValue: lang)
        else {
            return .english
        }
        return supportingLang
    }
    
    private init() {}
    
    var user: UserInfo? {
        UserInfo.getActiveUser
    }
    
    var userImage: UIImage {
        if let userImage = UserImage.image {
            return userImage
        } else {
            return UIImage(named: "male_user")!.maskWithColor(color: Colors.blueColor)
            
        }
    }
    
    var activeUserID: String? {
        User.userID
    }
    
    var isAuthenticated: Bool {
        UserInfo.getActiveUser != nil
    }
    
    func changeLanguage(language: SupportingLangauges) {
        LocalizationManager.sharedInstance.setLanguage(languageCode: language.languageCode)
    }
    
    var otp: String = ""
    var mobile: String = ""
}
