//
//  User.swift
//  Emmys
//
//  Created by Syed Asim Najam on 01/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import RealmSwift

final class User: Object,Decodable {
    var flag: Int = 0
    @objc dynamic var id: String = ""
    var isLoggedIn: Bool {
        flag == 1
    }
    var isSignedUp: Bool {
        flag == 2
    }
    enum CodingKeys: String, CodingKey {
        case flag = "Flag"
        case id = "Result"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        flag = tryOrNil {
            try container.decode(Int.self, forKey: .flag)
            } ?? 0
        id = tryOrNil {
            try container.decode(String.self, forKey: .id)
            } ?? ""
    }
    
    public required init() {}
    
    static func save(_ user: User) {
        let realmDB = Database.realmDb()
        realmDB.write(onError: Database.funcAndLine) {
            realmDB.add(user)
        }
    }
    
    static var userID: String? {
        let realmDB = Database.realmDb()
        return realmDB.objects(User.self).first?.id ?? nil
    }
}
