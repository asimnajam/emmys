//
//  ResetPassword.swift
//  Emmys
//
//  Created by Asim Najam on 2/11/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation

final class ResetPassword: Decodable {
    var flag: Int = 0
    private var result: String = ""
    var isUpdated: Bool {
        return result == "Updated" && flag == 1
    }
    
    enum CodingKeys: String, CodingKey {
        case flag = "Flag"
        case result = "Result"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        flag = tryOrNil {
            try container.decode(Int.self, forKey: .flag)
            } ?? 0
        result = tryOrNil {
            try container.decode(String.self, forKey: .result)
            } ?? ""
    }
}
