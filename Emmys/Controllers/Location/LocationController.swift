//
//  LocationController.swift
//  Emmys
//
//  Created by Asim Najam on 2/13/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import DropDown
import GoogleMaps
import GooglePlaces

/// Google Map API key: `AIzaSyBD1ZWcO_PXp0BPGVjZlyMwGv37vcjzeyM`
class LocationController: UIViewController {
    private let contentView = LocationView(frame: .zero)
    private let dropDown = DropDown()
    private var pickerViewType: PickerViewType = .city
    private var selectedCity: String = "" {
        willSet(newValue) {
            let text = newValue.isEmpty ? contentView.selectCityText : newValue
            selectedAddress = ""
            selectedBranch = ""
            toggleMap(false)
            contentView.cityButton.setTitle(text, for: .normal)
            contentView.addressButton.isEnabled = !newValue.isEmpty
        }
    }
    private var selectedAddress: String = "" {
        willSet(newValue) {
            selectedBranch = ""
            toggleMap(false)
            let text = newValue.isEmpty ? contentView.selectAddressText : newValue
            contentView.addressButton.setTitle(text, for: .normal)
            contentView.branchButton.isEnabled = !newValue.isEmpty
        }
    }
    private var selectedBranch: String = "" {
        willSet(newValue) {
            let text = newValue.isEmpty ? contentView.selectBranchText : newValue
            contentView.branchButton.setTitle(text, for: .normal)
        }
        didSet {
            goToLocation()
        }
    }
    
    private var branches: [String] {
        BranchList.branchesInfo.branchBy(city: selectedCity, address: selectedAddress)
    }
    private var cities: [String] {
        BranchList.branchesInfo.cities
    }
    private var addresses: [String] {
        BranchList.branchesInfo.addressesBy(city: selectedCity)
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        getBranchList()
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.cityButton.addTarget(self, action: #selector(showCityPickerView), for: .touchUpInside)
        contentView.addressButton.addTarget(self, action: #selector(showAddressPickerView), for: .touchUpInside)
        contentView.branchButton.addTarget(self, action: #selector(showBranchPickerView), for: .touchUpInside)
        
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let self = self else { return }
            switch self.pickerViewType {
            case .city:
                self.selectedCity = item
            case .address:
                self.selectedAddress = item
            case .branch:
                self.selectedBranch = item
            }
        }
        setEmmysLogo()
    }
    
    @objc func showCityPickerView() {
        pickerViewType = .city
        showDropDown(view: contentView.cityButton, dataSource: cities)
        
    }
    
    @objc func showAddressPickerView() {
        pickerViewType = .address
        showDropDown(view: contentView.addressButton, dataSource: addresses)
    }
    
    @objc func showBranchPickerView() {
        pickerViewType = .branch
        showDropDown(view: contentView.branchButton, dataSource: branches)
    }
    
    func getBranchList() {
        guard !BranchList.branchesInfo.isEmpty else { return }
    }
}

extension LocationController {
    func showDropDown(view: UIView, dataSource: [String]) {
        dropDown.dataSource = dataSource
        dropDown.anchorView = view
        dropDown.show()
    }
    
    func goToLocation() {
        guard !selectedCity.isEmpty, !selectedAddress.isEmpty, !selectedBranch.isEmpty else { return }
        if let selectedLocation = BranchList.branchesInfo.selectedLocationBy(
            city: selectedCity,
            address: selectedAddress,
            branch: selectedBranch) {
            let model = LocationPickerModel(branch: selectedLocation)
            guard let coordinates = model.coordinate else { return }
            
            let camera = GMSCameraPosition.camera(
                withTarget: coordinates,
                zoom: 16.0
            )
            contentView.mapView.camera = camera
            let marker = GMSMarker()
            marker.appearAnimation = .pop
            marker.position = coordinates
            marker.title = model.branch.branch
            marker.snippet = model.branch.address
            marker.map = contentView.mapView
            toggleMap(true)
        }
    }
    
    private func toggleMap(_ isShow: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.contentView.mapView.alpha = isShow ? 1.0 : 0.0
        }
    }
}
