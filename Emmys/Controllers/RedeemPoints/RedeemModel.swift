//
//  RedeemModel.swift
//  Emmys
//
//  Created by Asim Najam on 3/14/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
import UIKit

final class RedeemModel: NSObject {
    private weak var controller: UIViewController?
    
    init(controller: UIViewController) {
        self.controller = controller
    }
    
//    func redeemVoucherAPI(offer: OfferList) {
//        guard let user = emmysManager.user else { return }
//        APICommunicator.redeemPoints(customerID: user.userCode, offerID: "\(offer.id)", points: "\(offer.points)") { [weak self] result in
//            switch result {
//            case .success(let redeemVoucher):
//                guard let self = self, let voucher = redeemVoucher else { return }
//                self.showRedeemClaimVoucherView(offer: offer, voucher: voucher)
//            case .failure(let error):
//                print(error)
//            }
//        }
//    }
}
