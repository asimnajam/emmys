//
//  LocationPickerController.swift
//  Emmys
//
//  Created by Asim Najam on 2/13/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class LocationPickerController: UIViewController {
    let contentView = LocationPickerView(frame: .zero)
    let viewModel: LocationPickerModel
    
    init(viewModel: LocationPickerModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        goToLocation()
    }
    
    func goToLocation() {
        guard let coordinates = viewModel.coordinate else { return }
        let camera = GMSCameraPosition.camera(
            withTarget: coordinates,
            zoom: 16.0
        )
        contentView.mapView.camera = camera
        let marker = GMSMarker()
        marker.appearAnimation = .pop
        marker.position = coordinates
        marker.title = viewModel.branch.branch
        marker.snippet = viewModel.branch.address
        marker.map = contentView.mapView
    }
}
