//
//  VoucherHistoryCellView.swift
//  Emmys
//
//  Created by Asim Najam on 2/21/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

//class VoucherHistoryCellView: UIView {
//    private let isVoucherUsed: Bool
//
//    let voucherCodeLabel: UILabel = {
//        let label = UILabel()
//        label.text = LocalizedLanguage.date
//        label.font = UIFont.smallFont
//        label.textColor = Colors.blueColor
//        label.textAlignment = .center
//        return label
//    }()
//
//    let voucherAmountLabel: UILabel = {
//        let label = UILabel()
//        label.font = UIFont.smallFont
//        label.text = LocalizedLanguage.branch
//        label.textColor = Colors.blueColor
//        label.textAlignment = .center
//        return label
//    }()
//
//    let voucherDateLabel: UILabel = {
//        let label = UILabel()
//        label.font = UIFont.smallFont
//        label.text = "21: 2: 20"
//        label.textColor = Colors.blueColor
//        label.textAlignment = .center
//        return label
//    }()
//
//
//
//    let checkmarkButton: CheckmarkButton = {
//        let button = CheckmarkButton()
//        button.isChecked = true
//        button.isEnabled = false
//        return button
//    }()
//
//    init(isUsedVoucher: Bool) {
//        isVoucherUsed = isUsedVoucher
//        super.init(frame: .zero)
//        setupView()
//    }
//
//    override init(frame: CGRect) {
//        isVoucherUsed = false
//        super.init(frame: frame)
//        setupView()
//    }
//
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//    private func setupView() {
//        if isVoucherUsed {
//            configureUsedVoucer()
//        } else {
//            configureUnusedVoucer()
//        }
//    }
//
//    func configureUsedVoucer() {
//        let width = frame.width
//        let voucherCodeWidth = (width / 2) - 20
//        let voucherAmountWidth = voucherCodeWidth / 2
//
//
//        let customSpacig: CGFloat = 5.0
//        let cornerRadius: CGFloat = 3.0
//        let borderColor = UIColor.gray
//        let borderWidth: CGFloat = 1.0
//
//        let otpTextFieldsStackView: UIStackView = {
//            let stackView = UIStackView(arrangedSubviews: [voucherCodeLabel, voucherAmountLabel, checkmarkButton])
//            stackView.axis = .horizontal
//            stackView.distribution = .fillProportionally
//            return stackView
//        }()
//        directionalLayoutMargins = .init(top: 10, leading: 0.0, bottom: 10, trailing: 0.0)
//        backgroundColor = .white
//        add(views: [otpTextFieldsStackView])
//
//        otpTextFieldsStackView.snp.makeConstraints { make in
//            make.edges.equalToSuperview()
//        }
//
//        voucherCodeLabel.snp.makeConstraints { make in
//            make.width.equalToSuperview().multipliedBy(0.57)
//        }
//
//        voucherAmountLabel.snp.makeConstraints { make in
//            make.width.equalToSuperview().multipliedBy(0.25)
//        }
//
//        checkmarkButton.snp.makeConstraints { make in
//            make.width.equalToSuperview().multipliedBy(0.15)
//        }
//
//        otpTextFieldsStackView.setCustomSpacing(customSpacig, after: voucherCodeLabel)
//        otpTextFieldsStackView.setCustomSpacing(customSpacig, after: voucherAmountLabel)
//
//
//        voucherCodeLabel.layer.cornerRadius = cornerRadius
//        voucherCodeLabel.layer.borderColor = borderColor.cgColor
//        voucherCodeLabel.layer.borderWidth = borderWidth
//
//        voucherAmountLabel.layer.cornerRadius = cornerRadius
//        voucherAmountLabel.layer.borderColor = borderColor.cgColor
//        voucherAmountLabel.layer.borderWidth = borderWidth
//
//        checkmarkButton.layer.cornerRadius = cornerRadius
//        checkmarkButton.layer.borderColor = borderColor.cgColor
//        checkmarkButton.layer.borderWidth = borderWidth
//    }
//
//    func configureUnusedVoucer() {
//        let customSpacig: CGFloat = 5.0
//        let otpTextFieldsStackView: UIStackView = {
//            let stackView = UIStackView(arrangedSubviews: [voucherDateLabel, voucherCodeLabel, voucherAmountLabel])
//            stackView.axis = .horizontal
//            stackView.distribution = .fillProportionally
//            return stackView
//        }()
//        directionalLayoutMargins = .init(top: 10, leading: 0.0, bottom: 10, trailing: 0.0)
//        backgroundColor = .white
//        add(views: [otpTextFieldsStackView])
//
//        otpTextFieldsStackView.snp.makeConstraints { make in
//            make.edges.equalToSuperview()
//        }
//        voucherDateLabel.snp.makeConstraints { make in
//            make.width.equalToSuperview().multipliedBy(0.25)
//        }
//
//        voucherCodeLabel.snp.makeConstraints { make in
//            make.width.equalToSuperview().multipliedBy(0.5)
//        }
//
//        voucherAmountLabel.snp.makeConstraints { make in
//            make.width.equalToSuperview().multipliedBy(0.25)
//        }
//
//        //            checkmarkButton.snp.makeConstraints { make in
//        //                make.width.equalToSuperview().multipliedBy(0.15)
//        //            }
//
//
//
//        otpTextFieldsStackView.setCustomSpacing(customSpacig, after: voucherCodeLabel)
//        otpTextFieldsStackView.setCustomSpacing(customSpacig, after: voucherAmountLabel)
//    }
//
//}

class VoucherHistoryCellView: UIView {
    enum ViewKind {
        case header
        case cell
        case voucher
        case voucherHeader
    }
    var kind: ViewKind = .cell
    let detailViewWidth = 40
    lazy var otpTextFieldsStackView: UIStackView = {
        let subViews = kind == .header ?
            [dateLabel, voucherCodeLabel, pointsLabel, detailLabel] :
            [dateLabel, voucherCodeLabel, pointsLabel, detailView]
        
        let stackView = UIStackView(arrangedSubviews: subViews)
        stackView.axis = .horizontal
        stackView.distribution = .fill
        return stackView
    }()
    
    private let dateLabel: UILabel = {
        let label = UILabel()
        label.text = LocalizedLanguage.date
        label.font = UIFont.xSmallFont
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    private let voucherCodeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.xSmallFont
        label.text = LocalizedLanguage.branch
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    private let pointsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.xSmallFont
        label.text = LocalizedLanguage.amount
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
//    private let pointsLabel: UILabel = {
//        let label = UILabel()
//        label.font = UIFont.xSmallFont
//        label.text = LocalizedLanguage.points
//        label.textColor = Colors.blueColor
//        label.textAlignment = .center
//        return label
//    }()
    private let detailLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.xSmallFont
        label.text = LocalizedLanguage.detail
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    private lazy var detailView: UIView = {
        let view = UIView()
        return view
    }()
    private let checkMarkImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .white
        imageView.layer.cornerRadius = 3.0
        imageView.layer.masksToBounds = true
        imageView.layer.borderColor = Colors.blueColor.cgColor
        imageView.layer.borderWidth = 1.0
        return imageView
    }()
    
    init(frame: CGRect, kind: ViewKind) {
        super.init(frame: frame)
        self.kind = kind
        setupView()
    }
   override init(frame: CGRect) {
       super.init(frame: frame)
       setupView()
   }
   
   required init?(coder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
   }

    private func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 0, leading: 5.0, bottom: 0, trailing: 5.0)
        addSubview(otpTextFieldsStackView)
        otpTextFieldsStackView.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.height.equalTo(40.0)
        }
        dateLabel.snp.makeConstraints { make in
            make.width.equalTo(80.0)
        }
        
        voucherCodeLabel.snp.makeConstraints { make in
            make.width.greaterThanOrEqualTo(100.0)
        }
        
        pointsLabel.snp.makeConstraints { make in
            make.width.equalTo(70.0)
        }
        
        
        if kind == .cell {
            detailView.snp.makeConstraints { make in
                make.width.equalTo(65.0)
            }
            detailView.addSubview(checkMarkImageView)
            checkMarkImageView.snp.makeConstraints { make in
                make.center.equalToSuperview()
                make.width.height.equalTo(20.0)
            }
        } else {
            detailLabel.snp.makeConstraints { make in
                make.width.equalTo(65.0)
            }
            dateLabel.font = UIFont.mediumFont
            voucherCodeLabel.font = UIFont.mediumFont
            pointsLabel.font = UIFont.mediumFont
            
            detailLabel.font = UIFont.mediumFont
        }
    }
    
    func configure(with voucher: VoucherHistory) {
        dateLabel.text = voucher.voucherDate?.formattedString ?? " --- "
        voucherCodeLabel.text = voucher.voucherNumber
        pointsLabel.text = "\(voucher.points)"
        
        dateLabel.font = UIFont.smallFont
        voucherCodeLabel.font = UIFont.smallFont
        pointsLabel.font = UIFont.smallFont
        
        let color = voucher.isUsed ? Colors.blueColor : .white
        let checkImage = UIImage(named: "checkmark")?.maskWithColor(color: color)
        checkMarkImageView.image = voucher.isUsed ? checkImage : nil
    }
    
    func configureVoucherHeader() {
        
        dateLabel.text = LocalizedLanguage.date
        voucherCodeLabel.text = LocalizedLanguage.voucher_number
        pointsLabel.text = LocalizedLanguage.points
        detailLabel.text = LocalizedLanguage.used
        
        
        dateLabel.textColor = .white
        voucherCodeLabel.textColor = .white
        pointsLabel.textColor = .white
        detailLabel.textColor = .white
        backgroundColor = Colors.blueColor
    }
}
