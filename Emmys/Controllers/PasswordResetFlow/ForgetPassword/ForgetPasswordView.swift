//
//  ForgetPasswordView.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class ForgetPasswordView: UIView {
//    let closeButton: UIButton = {
//        let button = UIButton()
//        button.setImage(UIImage(named: "closeIcon"), for: .normal)
//        return button
//    }()
    let headerView: HeaderView = {
        let view = HeaderView(frame: .zero)
        view.configureWithTitle(title: LocalizedLanguage.forget_password)
        return view
    }()
    
    let forgetPasswordInfoLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.xSmallFont
        label.text = LocalizedLanguage.enter_your_mobile_number_to_verify_your_account
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    
    let mobileTextField: UITextField = {
        let textField = PrimaryTextField(textFieldType: .mobile(type: .default), type: .filled)
        return textField
    }()
    
    let submitButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle(LocalizedLanguage.submit, for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 0, leading: 50.0, bottom: 0, trailing: 50.0)
        add(views: [headerView, forgetPasswordInfoLabel, mobileTextField, submitButton])
//        closeButton.snp.makeConstraints { make in
//            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
//            make.width.height.equalTo(44.0)
//            make.leading.equalToSuperview()
//        }
        headerView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)//.offset(10.0)
            make.leadingMargin.trailingMargin.equalToSuperview()
        }
        
        forgetPasswordInfoLabel.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom)
        }
        
        mobileTextField.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(forgetPasswordInfoLabel.snp.bottom).offset(10.0)
            make.height.equalTo(44.0)
        }
        
        submitButton.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(mobileTextField.snp.bottom).offset(10.0)
            make.height.equalTo(44.0)
        }
    }

}
