//
//  RedeemPointsView.swift
//  Emmys
//
//  Created by Asim Najam on 2/9/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class RedeemPointsView: UIView {
    enum ViewType {
        case redeemInfo
        case redeemClaim
    }
    var type: ViewType
    
    let noPointsLabel: UILabel = {
        let label = LocalizableLabel()
        label.font = UIFont.largeFont
        label.numberOfLines = 0
        label.textColor = Colors.blueColor
        label.isHidden = true
        return label
    }()
    
    let underLinedLabel: UILabel = {
        let label = UnderLinedLabel()
        label.font = UIFont.smallFont
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = Colors.blueColor
        label.isHidden = true
        return label
    }()
    
    
    lazy var redeemButton: UIButton = {
        let button = PrimaryButton()
        return button
    }()
    
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60.0
        tableView.separatorInset = .zero
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.tableFooterView = UIView()
        tableView.tableHeaderView = UIView()
        return tableView
    }()
    
    private let customSpacing: CGFloat = 10.0
    private lazy var stackView: UIStackView = {
        let views: [UIView] = type == .redeemClaim ?
            [noPointsLabel, redeemButton, underLinedLabel, tableView] :
            [underLinedLabel, tableView]
        let sv = UIStackView(arrangedSubviews: views)
        sv.axis = .vertical
        sv.distribution = .fill
        return sv
    }()
    
    init(frame: CGRect, type: ViewType) {
        self.type = type
        super.init(frame: frame)
        setupView()
    }
    
    override init(frame: CGRect) {
        self.type = .redeemClaim
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        self.type = .redeemClaim
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 20.0, leading: 20.0, bottom: 0.0, trailing: 20.0)
        
        add(views: [
            stackView
        ])
        stackView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(20.0)
            make.leadingMargin.equalToSuperview()
            make.trailingMargin.equalToSuperview()
            make.bottomMargin.equalToSuperview()
        }
        
        for customView in stackView.subviews.dropLast() {
                stackView.setCustomSpacing(customSpacing, after: customView)
        }
        
        switch type {
        case .redeemInfo:
            underLinedLabel.text = LocalizedLanguage.emmys_reward_system
            underLinedLabel.font = UIFont.largeFont
            underLinedLabel.isHidden = false
            underLinedLabel.textAlignment = .center
        case .redeemClaim:
            underLinedLabel.text = LocalizedLanguage.min_500_points_required
            underLinedLabel.textAlignment = .left
        }
    }
}
