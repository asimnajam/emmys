//
//  Labels.swift
//  Emmys
//
//  Created by Asim Najam on 2/19/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

final class CustomLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        textAlignment = .left
        textColor = Colors.blueColor
        numberOfLines = 0
    }
}

extension UILabel {
    static var mediumLabel: CustomLabel {
        let label = CustomLabel()
        label.font = UIFont.mediumFont
        label.textAlignment = .center
        return label
    }
    
    static var largeLabel: CustomLabel {
        let label = CustomLabel()
        label.font = UIFont.largeFont
        label.textAlignment = .center
        return label
    }
    
    static var xLargeLabel: CustomLabel {
        let label = CustomLabel()
        label.font = UIFont.xlargeFont
        label.textAlignment = .center
        return label
    }
    
    static var smallLabel: CustomLabel {
        let label = CustomLabel()
        label.font = UIFont.xSmallFont
        return label
    }
    
    static var smallBoldLabel: CustomLabel {
        let label = CustomLabel()
        label.font = UIFont.smallBoldFont
        return label
    }
}
