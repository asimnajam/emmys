//
//  ProductController.swift
//  Emmys
//
//  Created by Amir on 2/6/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class ProductController: UIViewController {
   typealias ProductCell = ContainerCollectionViewCell<ProductCollectionCellView>
    typealias ProductMenuCell = ContainerCollectionViewCell<ProductMenuCellView>
    let contentView = ProductView()
    var itemPoints: [ItemPoint] = []
    var itemList: [Item] = []
    var selectedMenu: IndexPath? {
        didSet {
            contentView.collectionView.reloadData()
        }
    }
    
    var pointsLocalized: [String] {
        [
            LocalizedLanguage.fifteen,
            LocalizedLanguage.twenty,
            LocalizedLanguage.twenty_five
        ]
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        view.layoutIfNeeded()
        
        contentView.collectionView.register(
            ProductCell.self,
            forCellWithReuseIdentifier: String(
                describing: ProductCell.self
            )
        )
        contentView.collectionView.delegate = self
        contentView.collectionView.dataSource = self
        
        contentView.menuCollectionView.register(
            ProductMenuCell.self,
            forCellWithReuseIdentifier: String(
                describing: ProductMenuCell.self
            )
        )
        contentView.menuCollectionView.delegate = self
        contentView.menuCollectionView.dataSource = self
        getOfferList()
        getItemList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        contentView.menuContainerView.dropShadow()
        contentView.menuCollectionView.reloadData()
    }

    func getItemList(points: String = "0") {
        APICommunicator.getItemList(points: points) { [weak self]  result in
            guard let self = self else { return }
            switch result {
            case .success(let itemList):
                self.itemList = itemList
                self.contentView.collectionView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func filterProduct(byPoints points: String = "0") -> [Item] {
        guard let selectedPoint = selectedMenu else {
            return itemList
        }
        let itemPoint = itemPoints[selectedPoint.row]
        return itemList.filter { $0.itemPoints == itemPoint.itemPoints }
    }
}

extension ProductController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == contentView.collectionView {
            return filterProduct().count//itemList.count
        } else {
            return itemPoints.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == contentView.collectionView {
            return productCell(collectionView, indexPath: indexPath)
        } else {
            return productMenuCell(collectionView, indexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == contentView.collectionView {
            let padding: CGFloat =  10
            let collectionViewSize = (collectionView.frame.size.width - padding) / 2

            return CGSize(width: collectionViewSize, height: collectionViewSize + 40.0)
        } else {
            let dividingVal: Int
            if collectionView.numberOfItems(inSection: 0) > 3 {
                dividingVal = 3
            } else {
                dividingVal = collectionView.numberOfItems(inSection: 0)
            }
            let padding = Int(ProductView.menuCollectionViewInterimSpacing) * (2)
            let collectionViewSize = (Int(collectionView.frame.size.width) - padding) / dividingVal

            return CGSize(width: collectionViewSize, height: collectionViewSize)
        }
        
    }
    
    func productCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: String(
                describing: ProductCell.self
            ),
            for: indexPath) as? ProductCell
            else {
                return UICollectionViewCell()
        }
        let filteredItem = filterProduct()
        let item = filteredItem[indexPath.row]
        let card: ProductCollectionCellView
        if let existing = cell.cellView {
            card = existing
        } else {
            let newCardView = ProductCollectionCellView()
            cell.cellView = newCardView
            card = newCardView
        }
        card.productInfoLabel.text = item.itemName.capitalized
        card.productImage.loadImageFromURL(itemID: item.code)
        return cell
    }
    
    func productMenuCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: String(
                describing: ProductMenuCell.self
            ),
            for: indexPath) as? ProductMenuCell
            else {
                return UICollectionViewCell()
        }
        cell.contentView.layer.cornerRadius = 5.0
        cell.contentView.layer.masksToBounds = true
        let card: ProductMenuCellView
        if let existing = cell.cellView {
            card = existing
        } else {
            let newCardView = ProductMenuCellView()
            cell.cellView = newCardView
            card = newCardView
        }
        if let selectedMenu = selectedMenu, selectedMenu == indexPath {
            card.alpha = 1.0
        } else {
            card.alpha = 0.4
        }
        
        card.menuLabel.text = "\(itemPoints[indexPath.row].itemPoints)\n\(LocalizedLanguage.points)"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == contentView.collectionView {
            
        } else {
            if let cell = collectionView.cellForItem(at: indexPath) as?
                ProductMenuCell {
                cell.cellView?.alpha = 1.0
            }
            selectedMenu = indexPath
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == contentView.collectionView {
            print("Product Selected at indexPath: \(indexPath.row)")
        } else {
            if let cell = collectionView.cellForItem(at: indexPath) as?
                ProductMenuCell {
                cell.cellView?.alpha = 0.4
            }
        }
    }
    
    func getOfferList() {
        APICommunicator.getItemPoints { [weak self] result in
            switch result {
            case .success(let itemPoints):
                guard let self = self else { return }
                self.itemPoints = itemPoints
                self.contentView.menuCollectionView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
}
