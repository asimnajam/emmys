//
//  PointsController.swift
//  Emmys
//
//  Created by Amir on 2/5/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class PointsController: UIViewController {
    let contentView = PointsView()
    var billID: String?
    private var billDetail: BillHeaderInfo?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.totalPointsButton.addTarget(
            self,
            action: #selector(showTotalPointsView),
            for: .touchUpInside
        )
        contentView.pointsView.button.addTarget(
            self,
            action: #selector(showPointDetail),
            for: .touchUpInside
        )
        getBillDetail()
    }
    
    @objc func showTotalPointsView() {
        navigationController?.pushViewController(PointsHistoryController(), animated: true)
    }
    
    @objc func showPointDetail() {
        guard let billDetail = billDetail else { return }
        let amount = billDetail.details.compactMap { $0.points }.reduce(0, +)
        let detailView = PointsHistoryDetailView(frame: .zero)
        detailView.configure(
            date: billDetail.resultSet[0].addedOn.formattedString,
            branch: billDetail.resultSet[0].branch,
            ammount: "\(amount)",
            point: "\(billDetail.resultSet[0].earnedPoints)"
        )
        view.add(views: [detailView])
        detailView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func getBillDetail() {
        guard let customerID = emmysManager.activeUserID, let billID = billID else { return }
        APICommunicator.getBillHeaderInfo(customerID: customerID, billID: billID) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let billInfo):
                self.billDetail = billInfo
                self.contentView.pointsView.topLabel.text = "You have earned"
                self.contentView.pointsView.mainIntroLabel.text = "\(billInfo.resultSet[0].earnedPoints) Points"
            case .failure(let error):
                print(error)
            }
        }
    }
}
