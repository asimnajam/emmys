//
//  RedeemPointsCellView.swift
//  Emmys
//
//  Created by Asim Najam on 2/11/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class RedeemPointsCellView: UIView {
    let mainLabel: UILabel = {
        let label = LocalizableLabel()
        label.textColor = Colors.blueColor
        label.font = UIFont.mediumFont
        label.numberOfLines = 0
        label.text = "Bronze = 500"
        return label
    }()
    
    let infoLabel: UILabel = {
        let label = LocalizableLabel()
        label.textColor = .gray
        label.font = UIFont.smallFont
        label.numberOfLines = 0
        label.text = "(Juice and Sandwich free)"
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 20.0, leading: 0.0, bottom: 20.0, trailing: 0.0)
        add(views: [
            mainLabel,
            infoLabel
        ])
        mainLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        infoLabel.snp.makeConstraints { make in
            make.top.equalTo(mainLabel.snp.bottom).offset(5.0)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottomMargin.equalToSuperview()
        }
    }

}
