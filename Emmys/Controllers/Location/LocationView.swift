//
//  LocationView.swift
//  Emmys
//
//  Created by Asim Najam on 2/13/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class LocationView: UIView {
    let selectCityText = LocalizedLanguage.select_city
    let selectAddressText = LocalizedLanguage.select_address
    let selectBranchText = LocalizedLanguage.branch
    
    let headerView: BorderedView = {
        let view = BorderedView()
        view.configure(type: .normal)
        view.mainIntroLabel.text = LocalizedLanguage.location
        return view
    }()
    
    let mapView: GMSMapView = {
        let view = GMSMapView()
        view.mapType = GMSMapViewType.normal
        view.accessibilityElementsHidden = false
        view.isMyLocationEnabled = true
        view.layer.borderColor = Colors.blueColor.cgColor
        view.layer.borderWidth = 1.0
        view.alpha = 0
        return view
    }()
    
    let cityButton: UIButton = {
        let button = BorderedButton()
        button.setTitle(LocalizedLanguage.select_city, for: .normal)
        return button
    }()
    
    let addressButton: UIButton = {
        let button = BorderedButton()
        button.setTitle(LocalizedLanguage.select_address, for: .normal)
        button.isEnabled = false
        return button
    }()
    
    let branchButton: UIButton = {
        let button = BorderedButton()
        button.setTitle(LocalizedLanguage.branch, for: .normal)
        button.isEnabled = false
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 0, leading: 30.0, bottom: 30.0, trailing: 30.0)
        add(views: [headerView, cityButton, addressButton, branchButton, mapView])
        
        headerView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.height.equalTo(80.0)
        }
        
        cityButton.snp.makeConstraints { make in
            make.trailingMargin.leadingMargin.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom).offset(10.0)
            make.height.equalTo(44.0)
        }
        
        addressButton.snp.makeConstraints { make in
            make.trailingMargin.leadingMargin.equalToSuperview()
            make.top.equalTo(cityButton.snp.bottom).offset(10.0)
            make.height.equalTo(44.0)
        }
        
        branchButton.snp.makeConstraints { make in
            make.trailingMargin.leadingMargin.equalToSuperview()
            make.top.equalTo(addressButton.snp.bottom).offset(10.0)
            make.height.equalTo(44.0)
        }
        
        mapView.snp.makeConstraints { make in
            make.bottomMargin.trailingMargin.leadingMargin.equalToSuperview()
            make.top.equalTo(branchButton.snp.bottom).offset(10.0)
        }
    }

}
