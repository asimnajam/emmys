//
//  SideMenuCellView.swift
//  Emmys
//
//  Created by Asim Najam on 2/8/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class SideMenuCellView: UIView {
    let menuImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let menuLabel: UILabel = {
        let label = LocalizableLabel()
        label.textColor = Colors.blueColor
        label.font = UIFont.mediumFont
        label.numberOfLines = 0
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        add(views: [menuImage, menuLabel])
        if emmysManager.currentLanguage == .english {
            englishLanguageView()
        } else {
            arabicLanguageView()
        }
    }
    
    func englishLanguageView() {
        menuImage.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(10.0)
            make.width.height.equalTo(25.0)
        }
        menuLabel.snp.makeConstraints { make in
            make.leading.equalTo(menuImage.snp.trailing).offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
            make.centerY.equalToSuperview()
        }
    }
    
    func arabicLanguageView() {
        menuImage.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(10.0)
            make.width.height.equalTo(25.0)
        }
        menuLabel.snp.makeConstraints { make in
            make.trailing.equalTo(menuImage.snp.leading).offset(-10.0)
            make.leading.equalToSuperview().offset(10.0)
            make.centerY.equalToSuperview()
        }
    }
    
    func remakeContraints() {
        menuImage.snp.removeConstraints()
        menuLabel.snp.removeConstraints()
        if emmysManager.currentLanguage == .english {
            englishLanguageView()
        } else {
            arabicLanguageView()
        }
    }
    
}
