//
//  LocalizedLangauge.swift
//  Emmys
//
//  Created by Asim Najam on 2/20/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation
struct LocalizedLanguage {
    static var select_language: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.select_language)
    }
    
    static var name: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.name)
    }
    
    static var mobile_number: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.mobile_number)
    }
    
    static var email: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.email)
        
    }
    
    static var password: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.password)
    }
    
    static var confirm_password: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.confirm_password)
    }
    
    static var register_now: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.register_now)
    }
    
    static var verification: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.verification)
    }
    
    static var a_verification_pin_has_been_sent_throught_sms: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.a_verification_pin_has_been_sent_throught_sms)
    }
    
    static var this_might_take_a_few_minutes: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.this_might_take_a_few_minutes)
    }
    
    static var didnot_get_the_code_yet: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.didnot_get_the_code_yet)
    }
    
    static var resend_code: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.resend_code)
    }
    
    static var verify: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.verify)
    }
    
    static var loyality_program: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.loyality_program)
    }
    
    static var register_app: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.register_app)
    }
    
    static var login_app: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.login_app)
    }
    
    static var login: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.login)
    }
    
    static var forget_password: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.forget_password)
    }
    
    static var enter_your_mobile_number_to_verify_your_account: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.enter_your_mobile_number_to_verify_your_account)
    }
    
    static var submit: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.submit)
    }
    
    static var verification_code: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.verification_code)
    }
    
    static var please_enter_sms_code_sent_to_your_mobile: String { localizationManager.localizedStringForKey(key: LanguageKeys.please_enter_sms_code_sent_to_your_mobile)
    }
    
    static var enter: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.enter)
    }
    
    static var new_password: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.new_password)
    }
    
    static var wallet: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.wallet)
    }
    
    static var scan_barcode_on_your_receipt: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.scan_barcode_on_your_receipt)
    }
    
    static var thank_you_for_registration: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.thank_you_for_registration)
    }
    
    static var you_have_free_50_points: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.you_have_free_50_points)
    }
    
    static var on_your_successfull_registration: String { localizationManager.localizedStringForKey(key: LanguageKeys.on_your_successfull_registration)
    }
    
    static var _continue: String {
        localizationManager.localizedStringForKey(key: LanguageKeys._continue)
    }
    
    static var home: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.home)
    }
    
    static var my_profile: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.my_profile)
    }
    
    static var reward_system: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.reward_system)
    }
    
    static var deals_and_offers: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.deals_and_offers)
    }
    
    static var location: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.location)
    }
    
    static var contact_us: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.contact_us)
    }
    
    static var language: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.language)
    }
    
    static var logout: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.logout)
    }
    
    static var emmys_reward_system: String { localizationManager.localizedStringForKey(key: LanguageKeys.emmys_reward_system)
    }
    
    static var feedback_and_complaints: String { localizationManager.localizedStringForKey(key: LanguageKeys.feedback_and_complaints)
    }
    
    static var business_opportunities: String { localizationManager.localizedStringForKey(key: LanguageKeys.business_opportunities)
    }
    
    static var your_name: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.your_name)
    }
    
    static var branch_name: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.branch_name)
    }
    
    static var you_have_earned: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.you_have_earned)
    }
    
    static var total_points: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.total_points)
    }
    
    static var points: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.points)
    }
    
    static var country: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.country)
    }
    
    static var city: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.city)
    }
    
    static var branch: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.branch)
    }
    
    static var date: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.date)
    }
    
    static var amount: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.amount)
    }
    
    static var detail: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.detail)
    }
    
    static var points_history: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.points_history)
    }
    
    static var register: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.register)
    }
    
    static var non_mandotry: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.non_mandotry)
    }
    
    static var select_city: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.select_city)
    }
    
    static var select_address: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.select_address)
    }
    
    static var select_branch: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.select_branch)
    }
    
    static var redeem: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.redeem)
    }
    
    static var bronze: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.bronze)
    }
    
    static var silver: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.silver)
    }
    
    static var gold: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.gold)
    }
    
    static var platinum: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.platinum)
    }
    
    static var diamond: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.diamond)
    }
    
    static var juice_and_sandwich_free: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.juice_and_sandwich_free)
    }
    
    static var sar_Coupon_Consumable_in_Emmys_or_Crunched: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.sar_Coupon_Consumable_in_Emmys_or_Crunched)
    }
    
    static var playstation_4: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.playstation_4)
    }
    
    static var iPhone_11: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.iPhone_11)
    }
    
    static var round_Trip_to_Dubai_or_any_approved_location_Hotel_3_Days: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.round_Trip_to_Dubai_or_any_approved_location_Hotel_3_Days)
    }
    
    static var voucherHistory: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.voucher_history)
    }
    
    static var aboutUs: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.about_us)
    }
    
    static var you_donot_have_sufficient_points_to_redeem: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.you_donot_have_sufficient_points_to_redeem)
    }
    
    static var min_500_points_required: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.min_500_points_required)
    }
    //
    static var about_us_first_para: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.about_us_first_para)
    }
    static var about_us_second_para: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.about_us_second_para)
    }
    static var about_emmys: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.about_emmys)
    }
    
    static var your: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.your)
    }
    static var voucher_is_ready_to_collect_from_any_branch: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.voucher_is_ready_to_collect_from_any_branch)
    }
    
    static var menu: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.menu)
    }
    static var scan: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.scan)
    }
    
    static var update_profile: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.update_profile)
    }
    static var voucher_number: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.voucher_number)
    }
    
    static var used: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.used)
    }
    
    static var change_language: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.change_language)
    }
    
    static var keep_me_login: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.keep_me_login)
    }
    
    static var write_your_complaints_and_feedback: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.write_your_complaints_and_feedback)
    }
    
    static var details: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.details)
    }
    
    static var your_profile_has_been_updated: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.your_profile_has_been_updated)
    }
    
    // Numbers
    static var fifteen: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.fifteen)
    }
    static var twenty: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.twenty)
    }
    static var twenty_five: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.twenty_five)
    }
    
    static var congratulations: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.congratulations)
    }
    
    static var your_points_reach_upto: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.your_points_reach_upto)
    }
    
    static var ignore: String {
        localizationManager.localizedStringForKey(key: LanguageKeys.ignore)
    }
}
