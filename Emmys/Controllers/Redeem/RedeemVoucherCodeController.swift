//
//  RedeemViewController.swift
//  Emmys
//
//  Created by Amir on 2/5/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class RedeemVoucherCodeController: UIViewController {
    private let contentView = RedeemVoucherCodeView()
    private let offer: OfferList
    private let voucher: RedeemVoucher
    
    init(offer: OfferList, voucher: RedeemVoucher) {
        self.offer = offer
        self.voucher = voucher
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.redeemInfoLabel.text = "\(LocalizedLanguage.your) \(offer.points) \(LocalizedLanguage.points) \(offer.levels) \(LocalizedLanguage.voucher_is_ready_to_collect_from_any_branch)"
        contentView.redeemPointsVoucherNumber.text = voucher.result
    }
}
