//
//  SelectLanguageView.swift
//  Emmys
//
//  Created by Syed Asim Najam on 02/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

final class SelectLanguageView: UIView {
    lazy var selectLanguageLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.xlargeFont
        label.text = "Select Language"
        label.textAlignment = .center
        label.textColor = Colors.blueColor
        return label
    }()
    
    let englishButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle("English", for: .normal)
        return button
    }()
    
    let arabicButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle("عربى", for: .normal)
        return button
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 0, leading: 30, bottom: 0, trailing: 30)
        add(views: [selectLanguageLabel, arabicButton, englishButton])
        
        selectLanguageLabel.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(100)
        }
        
        arabicButton.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(selectLanguageLabel.snp.bottom).offset(10.0)
            make.height.equalTo(50.0)
        }
        
        englishButton.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(arabicButton.snp.bottom).offset(20.0)
            make.height.equalTo(50.0)
        }
    }
}
