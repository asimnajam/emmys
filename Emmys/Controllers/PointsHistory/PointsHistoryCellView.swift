//
//  PointsHistoryCellView.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class PointsHistoryCellView: UIView {
    enum ViewKind {
        case header
        case cell
        case voucher
        case voucherHeader
    }
    var kind: ViewKind = .cell
    let detailViewWidth = 40
    lazy var otpTextFieldsStackView: UIStackView = {
        let subViews = kind == .header ?
            [dateLabel, branchLabel, ammountLabel, pointsLabel, detailLabel] :
            [dateLabel, branchLabel, ammountLabel, pointsLabel, detailView]
        
        let stackView = UIStackView(arrangedSubviews: subViews)
        stackView.axis = .horizontal
        stackView.distribution = .fill
        return stackView
    }()
    
    private let dateLabel: UILabel = {
        let label = UILabel()
        label.text = LocalizedLanguage.date
        label.font = UIFont.xSmallFont
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    private let branchLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.xSmallFont
        label.text = LocalizedLanguage.branch
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    private let ammountLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.xSmallFont
        label.text = LocalizedLanguage.amount
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    private let pointsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.xSmallFont
        label.text = LocalizedLanguage.points
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    private let detailLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.xSmallFont
        label.text = LocalizedLanguage.detail
        label.textColor = Colors.blueColor
        label.textAlignment = .center
        return label
    }()
    private lazy var detailView: UIView = {
        let view = UIView()
        return view
    }()
    private let circleView: UIView = {
        let circleView = UIView(frame: .zero)
        circleView.backgroundColor = Colors.lightGrey
        return circleView
    }()
    
    private let circleViewHollow: UIView = {
        let circleView = UIView(frame: .zero)
        circleView.backgroundColor = .white
        return circleView
    }()
    
    init(frame: CGRect, kind: ViewKind) {
        super.init(frame: frame)
        self.kind = kind
        setupView()
    }
   override init(frame: CGRect) {
       super.init(frame: frame)
       setupView()
   }
   
   required init?(coder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
   }

    private func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 0, leading: 5.0, bottom: 0, trailing: 5.0)
        addSubview(otpTextFieldsStackView)
        otpTextFieldsStackView.snp.makeConstraints { make in
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.height.equalTo(40.0)
        }
        dateLabel.snp.makeConstraints { make in
            make.width.equalTo(65.0)
        }
        
        branchLabel.snp.makeConstraints { make in
            make.width.greaterThanOrEqualTo(100.0)
        }
        
        ammountLabel.snp.makeConstraints { make in
            make.width.equalTo(70.0)
        }
        
        pointsLabel.snp.makeConstraints { make in
            make.width.equalTo(80.0)
        }
        
        
        
        if kind == .cell {
            detailView.snp.makeConstraints { make in
                make.width.equalTo(65.0)
            }
            detailView.addSubview(circleViewHollow)
            circleViewHollow.addSubview(circleView)
            circleViewHollow.layer.cornerRadius = 10.0
            circleViewHollow.layer.borderWidth = 2.0
            circleViewHollow.layer.borderColor = Colors.blueColor.cgColor
            
            circleViewHollow.snp.makeConstraints { make in
                make.center.equalToSuperview()
                make.width.height.equalTo(20.0)
            }
            
            circleView.snp.makeConstraints { make in
                make.center.equalToSuperview()
                make.width.height.equalTo(6.0)
            }
            circleView.layer.cornerRadius = 3.0
        } else {
            detailLabel.snp.makeConstraints { make in
                make.width.equalTo(65.0)
            }
            dateLabel.font = UIFont.mediumFont
            branchLabel.font = UIFont.mediumFont
            ammountLabel.font = UIFont.mediumFont
            pointsLabel.font = UIFont.mediumFont
            detailLabel.font = UIFont.mediumFont
        }
    }
    
    func configure(date: String, branch: String, ammount: String, point: String, color: UIColor) {
        dateLabel.text = date
        branchLabel.text = branch
        ammountLabel.text = ammount
        pointsLabel.text = point
        
        dateLabel.backgroundColor = color
        branchLabel.backgroundColor = color
        ammountLabel.backgroundColor = color
        pointsLabel.backgroundColor = color
        detailView.backgroundColor = color
    }
    
    func configureVoucherHistory(date: String, voucherCode: String, points: String, isUsed: Bool) {
        dateLabel.text = date
        branchLabel.text = voucherCode
        ammountLabel.text = points
        
        dateLabel.font = UIFont.smallFont
        branchLabel.font = UIFont.smallFont
        ammountLabel.font = UIFont.smallFont
        
        let color = isUsed ? Colors.blueColor : .white
        let checkImage = UIImage(named: "checkmark")?.maskWithColor(color: color)
        let imageView = UIImageView()
        imageView.image = checkImage
        addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.edges.equalTo(circleView)
            make.width.height.equalTo(20.0)
        }
        circleView.isHidden = true
        pointsLabel.removeFromSuperview()
    }
    
    func configureVoucherHeader() {
        pointsLabel.removeFromSuperview()
        dateLabel.text = "Date"
        branchLabel.text = "Voucher Number"
        ammountLabel.text = "Points"
        detailLabel.text = ""

    }

}
