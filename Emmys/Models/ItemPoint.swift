//
//  ItemPoint.swift
//  Emmys
//
//  Created by Syed Asim Najam on 31/01/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation

final class ItemPoint: Decodable {
    var flag: Int = 0
    var itemPoints: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case flag = "Flag"
        case itemPoints = "ITEM_POINTS"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        flag = tryOrNil {
            try container.decode(Int.self, forKey: .flag)
            } ?? 0
        itemPoints = tryOrNil {
            try container.decode(Int.self, forKey: .itemPoints)
            } ?? 0
    }
}
