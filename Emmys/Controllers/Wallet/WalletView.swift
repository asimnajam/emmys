//
//  HomeView.swift
//  Emmys
//
//  Created by Syed Asim Najam on 04/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class WalletView: UIView {
    let userImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "userImage")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let userNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Colors.blueColor
        label.font = UIFont.xlargeFont
        label.numberOfLines = 2
        return label
    }()
    
    let walletContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.blueColor
        return view
    }()
    
    let walletLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont.xlargeFont
        label.numberOfLines = 2
        label.text = LocalizedLanguage.wallet
        return label
    }()
    
    let walletButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "wallet2")?.maskWithColor(color: .white), for: .normal)
        button.imageView?.contentMode = .scaleAspectFill
        return button
    }()
    
    let pointsLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont.mediumFont
        label.numberOfLines = 2
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        add(views: [
            userImageView,
            userNameLabel,
            walletContainerView
        ])
        walletContainerView.add(views: [walletLabel, walletButton, pointsLabel])
        
        userImageView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
            make.height.width.equalTo(200.0)
            make.centerX.equalToSuperview()
        }
        
        userNameLabel.snp.makeConstraints { make in
            make.top.equalTo(userImageView.snp.bottom).offset(20.0)
            make.leading.equalToSuperview().offset(20.0)
            make.trailing.equalToSuperview().inset(20.0)
            make.centerX.equalToSuperview()
//            make.bottom.equalTo(walletContainerView.snp.top).offset(-30.0)
        }
        
        walletContainerView.snp.makeConstraints { make in
            make.top.equalTo(userNameLabel.snp.bottom).offset(30.0)
//            make.center.equalToSuperview()
            make.leading.equalToSuperview().offset(20.0)
            make.trailing.equalToSuperview().inset(20.0)
            make.height.equalTo(200.0)
//            make.bottom.greaterThanOrEqualToSuperview().inset(30.0)
        }
        
        walletLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10.0)
            make.centerX.equalToSuperview()
        }
        
        walletButton.snp.makeConstraints { make in
//            make.top.equalTo(walletLabel.snp.bottom)
            make.width.equalToSuperview().multipliedBy(0.25)
            make.height.equalTo(walletButton.snp.width)
            make.center.equalToSuperview()
        }
        
        pointsLabel.snp.makeConstraints { make in
            make.top.equalTo(walletButton.snp.bottom).offset(5.0)
            make.width.equalToSuperview()
        }
        
        walletContainerView.layer.cornerRadius = 5.0
        userImageView.layer.borderColor = Colors.blueColor.cgColor
        userImageView.layer.borderWidth = 1.0
    }
    
    private func adjustImageAndTitleOffsetsForButton (button: UIButton) {
        let spacing: CGFloat = 20.0
        let imageSize = button.imageView!.frame.size
        button.titleEdgeInsets = UIEdgeInsets(top: (imageSize.height + spacing), left: -imageSize.width, bottom: 0, right: 0)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
    }
}
