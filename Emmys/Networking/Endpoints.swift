//
//  Endpoints.swift
//  Emmys
//
//  Created by Syed Asim Najam on 31/01/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation

struct HTTPHeaders: Codable {
    private let companyID = "700255"
    private let DB_User_Key = "3WW^^$@p!"
    
    static var header: [String: String] {
        guard let data = try? JSONEncoder().encode(self.init()),
            let dict = try? JSONSerialization.jsonObject(
                with: data,
                options: []
                ) as? [String: String] else {
                    return [:]
        }
        return dict
    }
}
enum EndPoints {
    private var baseUrl: String {
        "http://37.224.88.244:19315/LYLTY/api/Code7"
    }
    case registerCustomer(customerName: String, mobile: String, email: String, password: String, brandID: String)
    case verifyOTP(customerID: String, otp: String)
    case getCustomerID(mobile: String, password: String, brandID: String)
    case resendOTP(mobile: String, brandID: String)
    case resetPassword(mobile: String, brandID: String, password: String, otp: String)
    case loginValidity(mobile: String, brandID: String, password: String)
    case scanInvoice(customerID: String, billNumber: String)
    case billInfoSummary(customerID: String, billNumber: String)
    case customerHistory(customerID: String)
    case currentOfferForLoyality
    case redeemPoints(customerID: String, offerID: String, points: String)
    case branchList(brandID: String)
    case pointsList(brandID: String)
    case itemList(brandID: String, points: String)
    case getOfferList
    case itemImage(itemID: String)
    case updateUser(customerID: String, customerName: String, mobile: String, email: String)
    case getUserInfo(customerID: String)
    case voucherHistory(customerID: String)
    
    
    var path: String {
        let endPointUrl: String
        switch self {
        case .registerCustomer(let name, let mobile, let email, let password, let brandID):
            endPointUrl = "RegisterCustomer/\(name)/\(mobile)/\(email)/\(password)/\(brandID)"
        case .branchList(let brandID):
            endPointUrl = "GetBranchList/\(brandID)"
        case .pointsList(let brandID):
            endPointUrl = "GetItemPoints/\(brandID)"
        case .getOfferList:
            endPointUrl = "GetOfferList"
        case .getCustomerID(let mobile, let password, let brandID):
            endPointUrl = "GetCustomerID/\(mobile)/\(password)/\(brandID)"
        case .verifyOTP(let customerID, let otp):
            endPointUrl = "VerifyOTP/\(customerID)/\(otp)"
        case .customerHistory(let customerID):
            endPointUrl = "GetHistory/\(customerID)"
        case .resetPassword(let mobile, let brandID, let password, let otp):
            endPointUrl = "ReSetPassword/\(mobile)/\(brandID)/\(password)/\(otp)"
        case .itemList(let brandID, let points):
            endPointUrl = "GetItemList/\(brandID)/\(points)"
        case .itemImage(let itemID):
            endPointUrl = "http://37.224.88.244:19315/LYLTY/itemImages/\(itemID).jpg"
            return endPointUrl
        case .updateUser(let customerID, let customerName, let mobile, let email):
            endPointUrl = "UpdateCustomerInfo/\(customerID)/\(customerName)/\(mobile)/\(email)"
        case .getUserInfo(let customerID):
            endPointUrl = "GetSummaryInfo/\(customerID)"
        case .redeemPoints(let customerID, let offerID, let points):
            endPointUrl = "Redeem/\(customerID)/\(offerID)/\(points)"
        case .resendOTP(let mobile, let brandID):
            endPointUrl = "ReSendOTP/\(mobile)/\(brandID)"
        case .billInfoSummary(let customerID, let billNumber):
            endPointUrl = "BillInfoHeader/\(customerID)/\(billNumber)"
        case .scanInvoice(let customerID, let billNumber):
            endPointUrl = "ScanBill/\(customerID)/\(billNumber)"
        case .voucherHistory(let customerID):
            endPointUrl = "GetVoucherHistory/\(customerID)"
        default:
            endPointUrl = ""
        }
        return baseUrl + "/" + endPointUrl
    }
}
