//
//  RegistrationSuccessView.swift
//  Emmys
//
//  Created by Asim Najam on 2/19/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class RegistrationSuccessView: UIView {
    let closeButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "closeIcon"), for: .normal)
        return button
    }()
    let headerView: HeaderView = {
        let view = HeaderView(frame: .zero)
        view.configureWithTitle(title: LocalizedLanguage.thank_you_for_registration)
        return view
    }()
    
    let continueButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle(LocalizedLanguage._continue, for: .normal)
        return button
    }()
    
    let mainLabel: UILabel = {
        let label = UILabel.xLargeLabel
        label.text = LocalizedLanguage.you_have_free_50_points
        return label
    }()
    
    let subLabel: UILabel = {
        let label = UILabel.mediumLabel
        label.text = LocalizedLanguage.on_your_successfull_registration
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        backgroundColor = .white
        directionalLayoutMargins = .init(top: 0, leading: 40.0, bottom: 0, trailing: 40.0)
        add(views: [headerView, closeButton, mainLabel, subLabel, continueButton])
//        closeButton.snp.makeConstraints { make in
//            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
//            make.width.height.equalTo(44.0)
//            make.leading.equalToSuperview()
//        }
        headerView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(10.0)
            make.leadingMargin.trailingMargin.equalToSuperview()
        }
        mainLabel.snp.makeConstraints { make in
            make.top.equalTo(headerView.snp.bottom).offset(10.0)
            make.leadingMargin.trailingMargin.equalToSuperview()
        }
        subLabel.snp.makeConstraints { make in
            make.top.equalTo(mainLabel.snp.bottom).offset(30.0)
            make.leadingMargin.trailingMargin.equalToSuperview()
        }
        
        continueButton.snp.makeConstraints { make in
            make.top.equalTo(subLabel.snp.bottom).offset(40.0)
            make.leadingMargin.trailingMargin.equalToSuperview()
            make.height.equalTo(PrimaryButton.height)
        }
        
    }
}
