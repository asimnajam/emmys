//
//  SignupController.swift
//  Emmys
//
//  Created by Syed Asim Najam on 02/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class SignupController: UIViewController {
    let contentView = SignupView(frame: .zero)
    private var keepLogin: Bool = false
    let controllerDismissSubject: PublishSubject<(String, ConrtollerType)>
    
    init(controllerDismissSubject: PublishSubject<(String, ConrtollerType)>) {
        self.controllerDismissSubject = controllerDismissSubject
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        view.layoutIfNeeded()
        hideKeyboardWhenTappedAround()
        applyNavigationBarStyle(.transparent)
        contentView.registerNowButton.addTarget(
            self,
            action: #selector(registerUser),
            for: .touchUpInside
        )
        contentView.checkmarkButton.addTarget(
            self,
            action: #selector(checkMarkButtonAction),
            for: .touchUpInside
        )
    }
    
    @objc func closeButtonAction() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func checkMarkButtonAction() {
        if contentView.checkmarkButton.isChecked {
            contentView.checkmarkButton.isChecked = false
        } else {
            contentView.checkmarkButton.isChecked = true
        }
        keepLogin = contentView.checkmarkButton.isChecked
    }

    @objc func registerUser(_ sender: UIButton) {
        guard let name = contentView.nameTextField.text, !name.isEmpty else {
            Toast.show(style: Toast.Style.error(message: "Please Enter Your Name"), view: view)
            return
        }

        guard let mobile = contentView.mobileTextField.text, !mobile.isEmpty else {
            Toast.show(style: Toast.Style.error(message: "Please Enter Your Mobile Number"), view: view)
            return
        }

        guard mobile.isValidMobileNumber else {
            Toast.show(style: Toast.Style.error(message: "Please enter your valid mobile number"), view: view)
            return
        }

        guard let email = contentView.emailTextField.text, !email.isEmpty else {
            Toast.show(style: Toast.Style.error(message: "Please Enter Your Email"), view: view)
            return
        }

        guard email.isValid else {
            Toast.show(style: Toast.Style.error(message: "Please Enter Your Valid Email"), view: view)
            return
        }

        guard let password = contentView.passwordTextField.text, !password.isEmpty else {
            Toast.show(style: Toast.Style.error(message: "Please Enter Your Password"), view: view)
            return
        }

        guard let confirmPassword = contentView.confirmPasswordTextField.text, !confirmPassword.isEmpty else {
            Toast.show(style: Toast.Style.error(message: "Please Enter Your Confirm Password"), view: view)
            return
        }

        guard password == confirmPassword else {
            Toast.show(style: Toast.Style.error(message: "Password and Confirm Password should be same"), view: view)
            return
        }

        guard password.isValidPassword else {
            Toast.show(style: Toast.Style.error(message: "Password length should be minimum 6 charactors"), view: view)
            return
        }

        APICommunicator.signup(
            name: name.removeWhiteSpaces(),
            mobile: mobile,
            email: email.removeWhiteSpaces(),
            password: password.removeWhiteSpaces()
        ) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let user):
                guard let user = user else { return }
                if user.isSignedUp {
                    self.showOTPView(id: user.id)
                    userDefault.set(mobile, forKey: userDefaultMobileKey)
                } else {
                    Toast.show(style: .error(message: "\(user.id)"), view: self.view)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func showOTPView(id: String) {
        self.controllerDismissSubject.onNext((id, .signup))
    }
}


