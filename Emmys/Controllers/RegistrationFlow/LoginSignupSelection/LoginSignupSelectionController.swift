//
//  LoginSignupSelectionController.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

enum ConrtollerType {
    case signup
    case otp(kind: VerifyOTPModel.Kind)
    case forgetPassword
    case login
    case resetPassword
    case registrationSuccess
}

class LoginSignupSelectionController: UIViewController {
    private let contentView = LoginSignupSelectionView(frame: .zero)
    private let disposeBag = DisposeBag()
    private let controllerDismissSubject = PublishSubject<(String, ConrtollerType)>()
    private var mobileNumber: String?
    private var otp: String?
    private let authModel = AuthModel()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        bindObservable()
    }
    
    @available(*, unavailable) required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        contentView.registerButton.addTarget(
            self,
            action: #selector(registerAction),
            for: .touchUpInside
        )
        
        contentView.loginButton.addTarget(
            self,
            action: #selector(loginAction),
            for: .touchUpInside
        )
        
        view.layoutIfNeeded()
    }
    
    @objc private func registerAction() {
        presentSignupController()
    }
    
    @objc private func loginAction() {
        presentLoginController()
    }
}

extension LoginSignupSelectionController {
    private func bindObservable() {
        controllerDismissSubject.subscribe(onNext: { [weak self] (value, controller) in
            guard let self = self else { return }
            switch controller {
            case .signup:
                self.presentOTPController(kind: .registration(id: value))
            case .otp(let kind):
                switch kind {
                case .registration(let customerID):
                    self.presentSignupSuccessController(customerID: customerID)
                case .forgetPassword:
                    self.otp = value
                    self.presentResetPasswordController()
                }
            case .forgetPassword:
                self.mobileNumber = value
                self.presentOTPController(kind: .forgetPassword)
            case .login:
                self.presentForgetPasswordController()
            case .resetPassword:
                self.presentLoginController()
            case .registrationSuccess:
                self.presentLoginController()
            }
        }).disposed(by: disposeBag)
    }
    
    private func presentOTPController(kind: VerifyOTPModel.Kind) {
        let model = VerifyOTPModel(type: kind, keepLogin: true)
        let controller = VerifyOTPController(model: model, otpControllerDismissSubject: controllerDismissSubject)
        presentController(controller)
    }
    
    private func presentSignupSuccessController(customerID: String) {
        let controller = RegistrationSuccessController(
            keepLogin: true,
            customerID: customerID,
            authModel: authModel,
            controllerDismissSubject: controllerDismissSubject
        )
        presentController(controller)
    }
    
    private func presentForgetPasswordController() {
        let controller = ForgetPasswordController(
            forgetPasswordControllerDismissSubject: controllerDismissSubject
        )
        presentController(controller)
    }
    
    private func presentResetPasswordController() {
        guard let mobileNumber = mobileNumber,
            let otp = otp
            else {
                assertionFailure("OTP and Mobile Number found nil")
                return
        }
        let controller = ResetPasswordController(mobileNumber: mobileNumber, otp: otp, controllerDismissSubject: controllerDismissSubject)
        presentController(controller)
    }
    
    private func presentLoginController() {
        let controller = LoginController(
            loginControllerDismissSubject: controllerDismissSubject,
            authModel: authModel
        )
        presentController(controller)
    }
    
    private func presentSignupController() {
        let controller = SignupController(controllerDismissSubject: controllerDismissSubject)
        presentController(controller)
    }
    
    private func presentController(_ controller: UIViewController) {
        navigationController?.pushViewController(controller, animated: true)
//        present(controller, animated: true, completion: nil)
    }
}
