//
//  ContactUsController.swift
//  Emmys
//
//  Created by Asim Najam on 2/9/20.
//  Copyright © 2020 Emmys. All rights reserved.
//
import MessageUI
import UIKit
import DropDown

class ContactUsController: UIViewController {
    enum EmailSubject: Int {
        case feedBackAndComplaint
        case businessOpportunities
        
        var text: String {
            switch self {
            case .feedBackAndComplaint:
                return "Feedback and complaints"
            case .businessOpportunities:
                return "business opportunities"
            }
        }
    }
    
    private var emailSubject: EmailSubject = .feedBackAndComplaint
    private var pickerViewType: PickerViewType = .city
    private let dropDown = DropDown()
//    private let mail = MFMailComposeViewController()
//    fileprivate var mail: MFMailComposeViewController!
    private var selectedCity: String = "" {
        willSet(newValue) {
            let text = newValue.isEmpty ? contentView.selectCityText : newValue
            contentView.cityButton.setTitle(text, for: .normal)
            contentView.addressButton.isEnabled = !newValue.isEmpty
        }
    }
    private var selectedAddress: String = "" {
        willSet(newValue) {
            let text = newValue.isEmpty ? contentView.selectAddressText : newValue
            contentView.addressButton.setTitle(text, for: .normal)
            contentView.branchButton.isEnabled = !newValue.isEmpty
        }
    }
    private var selectedBranch: String = "" {
        willSet(newValue) {
            let text = newValue.isEmpty ? contentView.selectBranchText : newValue
            contentView.branchButton.setTitle(text, for: .normal)
        }
    }
    
    private var branches: [String] {
        BranchList.branchesInfo.branchBy(city: selectedCity, address: selectedAddress)
    }
    private var cities: [String] {
        BranchList.branchesInfo.cities
    }
    private var addresses: [String] {
        BranchList.branchesInfo.addressesBy(city: selectedCity)
    }
    
    let contentView = ContactUsView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        view.layoutIfNeeded()
        contentView.setupView()
        
        contentView.cityButton.addTarget(self, action: #selector(showCityPicker), for: .touchUpInside)
        contentView.addressButton.addTarget(self, action: #selector(showAddressPicker), for: .touchUpInside)
        contentView.branchButton.addTarget(self, action: #selector(showBranchPicker), for: .touchUpInside)
        contentView.submitButton.addTarget(self, action: #selector(sendEmail), for: .touchUpInside)
        contentView.segmentedControl.addTarget(self, action: #selector(segmentedControlAction(_:)), for: .valueChanged)
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let self = self else { return }
            switch self.pickerViewType {
            case .city:
                self.selectedCity = item
            case .address:
                self.selectedAddress = item
            case .branch:
                self.selectedBranch = item
            }
        }
        hideKeyboardWhenTappedAround()
        setEmmysLogo()
        contentView.feedbackTextView.delegate = self
    }
    
    @objc func showCityPicker() {
        resginResponder()
        pickerViewType = .city
        showDropDown(view: contentView.cityButton, dataSource: cities)
    }
    
    @objc func showAddressPicker() {
        resginResponder()
        pickerViewType = .address
        showDropDown(view: contentView.addressButton, dataSource: addresses)
    }
    
    @objc func showBranchPicker() {
        resginResponder()
        pickerViewType = .branch
        showDropDown(view: contentView.branchButton, dataSource: branches)
    }
    
    @objc func sendEmail() {
        sendMail()
    }
    
    func didSelectCity(city: String) {
        if !city.isEmpty {
            selectedCity = city
            contentView.cityButton.setTitle(selectedCity, for: .normal)
        }
        
    }
    
    func didSelectAddress(address: String) {
        if !address.isEmpty {
            selectedAddress = address
            contentView.addressButton.setTitle(address, for: .normal)
        }
        
    }
    
    func didSelectBranch(branch: String) {
        if !branch.isEmpty {
            selectedBranch = branch
            contentView.branchButton.setTitle(branch, for: .normal)
        }
        
    }
    
    func resginResponder() {
        contentView.nameTextField.resignFirstResponder()
        contentView.emailTextField.resignFirstResponder()
        contentView.mobileTextField.resignFirstResponder()
    }
    
    func showDropDown(view: UIView, dataSource: [String]) {
        dropDown.dataSource = dataSource
        dropDown.anchorView = view
        dropDown.show()
    }
    
    @objc func segmentedControlAction(_ sender: UISegmentedControl) {
        emailSubject = EmailSubject(rawValue: sender.selectedSegmentIndex) ?? EmailSubject.feedBackAndComplaint
    }

}
extension ContactUsController: MFMailComposeViewControllerDelegate {
    
    func sendMail() {
        guard let name = contentView.nameTextField.text, !name.isEmpty else {
            Toast.show(style: Toast.Style.error(message: "Please enter your name"), view: view)
            return
        }
        
        guard let mobile = contentView.mobileTextField.text, !mobile.isEmpty else {
            Toast.show(style: Toast.Style.error(message: "Please enter your Mobile Number"), view: view)
            return
        }
        
        guard mobile.isValidMobileNumber else {
            Toast.show(style: Toast.Style.error(message: "Please enter your valid Mobile Number"), view: view)
            return
        }

        guard let email = contentView.emailTextField.text, !email.isEmpty else {
            Toast.show(style: Toast.Style.error(message: "Please enter your Email Address"), view: view)
            return
        }

        guard email.isValid else {
            Toast.show(style: Toast.Style.error(message: "Please enter your valid Email Address"), view: view)
            return
        }

        guard !selectedCity.isEmpty  else {
            Toast.show(style: Toast.Style.error(message: "Please select City"), view: view)
                       return
        }

        guard !selectedAddress.isEmpty  else {
            Toast.show(style: Toast.Style.error(message: "Please select Address"), view: view)
                       return
        }

        guard !selectedBranch.isEmpty  else {
            Toast.show(style: Toast.Style.error(message: "Please select Branch"), view: view)
                       return
        }
        
        guard let desc = contentView.feedbackTextView.text, !desc.isEmpty else {
            Toast.show(style: Toast.Style.error(message: "Please enter your Message"), view: view)
                       return
        }
        
        Toast.show(style: .info(message: "Email has been sent."), view: view)
            return
        
//        guard MFMailComposeViewController.canSendMail() else {
//            Toast.show(style: .error(message: "Mail services are not available."), view: view)
//            return
//        }
        
        let country = selectedCity
        let city = selectedAddress
        let branch = selectedBranch
        let body = "Name: \(name) \n Email: \(email) \n Mobile: \(mobile) \n Country: \(country) \n City: \(city) \n Branch: \(branch) \n Message: \(desc)"
        
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = self
        mail.setToRecipients(["info@emmysksa.com"])
        mail.setSubject(emailSubject.text)
        mail.setMessageBody(body, isHTML: false)
        self.present(mail, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension ContactUsController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = LocalizedLanguage.write_your_complaints_and_feedback
            textView.textColor = UIColor.lightGray
        }
    }
}
