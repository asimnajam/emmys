//
//  ProductCollectionCellView.swift
//  Emmys
//
//  Created by Amir on 2/6/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

class ProductCollectionCellView: UIView {
    let productImage: CustomImageView = {
        let imageView = CustomImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let productInfoLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor.gray
        label.font = UIFont.smallFont
        label.numberOfLines = 2
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor  = .white
        add(views: [productImage, productInfoLabel])
        productImage.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.9)
            make.height.equalTo(productImage.snp.width)
            make.centerX.equalToSuperview()
        }
        productInfoLabel.snp.makeConstraints { make in
            make.top.equalTo(productImage.snp.bottom).offset(10.0)
            make.leading.equalToSuperview().offset(10.0)
            make.trailing.equalToSuperview().inset(10.0)
        }
    }
}
