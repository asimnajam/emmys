//
//  DateFormatter.swift
//  Emmys
//
//  Created by Syed Asim Najam on 03/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import Foundation

extension Foundation.Date {
    public struct Formatters {
        public static let formatterISO8601: DateFormatter = {
            let formatter = DateFormatter()
            let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
            formatter.locale = enUSPosixLocale
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            formatter.calendar = Calendar(identifier: .gregorian)
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            return formatter
        }()

        public static let formatterUtcUS: DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            return formatter
        }()

        public static let formatterUTC: DateFormatter = {
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone(abbreviation: "UTC")
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            return formatter
        }()

        public static let formattedString: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-M-yy"
            return formatter
        }()
        
        public static let emmysDateFormatterString: DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formattedMonth.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            return formatter
            
//            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//            let date = dateFormatter.date(from: "2017-01-09T11:00:00.000Z")
        }()
        
        public static let formattedString2: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "d MMM, yy"
            return formatter
        }()

        public static let formattedWString: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "EEE, MMM d, hh:mm a"
            return formatter
        }()

        public static let formattedMonthDay: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM d"
            return formatter
        }()

        public static let formattedDay: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd"
            return formatter
        }()

        public static let formattedMonth: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM"
            return formatter
        }()

        /// e.g., Fri, Jul 1
        public static let formattedDayMonthDay: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "EEE, MMM d"
            return formatter
        }()
    }

    public var formattedUTC: String {
        return Formatters.formatterUTC.string(from: self)
    }

    public var formattedUTCtoDate: Foundation.Date {
        let dateString = Formatters.formatterUTC.string(from: self)
        return Formatters.formatterUTC.date(from: dateString)!
    }

    public var formattedWordDateString: String {
        return Formatters.formattedWString.string(from: self)
    }

    public var formattedString: String {
        return Formatters.formattedString.string(from: self)
    }
    
    public var formattedString2: String {
        return Formatters.formattedString2.string(from: self)
    }

    public var formattedMonthDay: String {
        return Formatters.formattedMonthDay.string(from: self)
    }

    public var formattedDay: String {
        return Formatters.formattedDay.string(from: self)
    }

    public var formattedMonth: String {
        return Formatters.formattedMonth.string(from: self)
    }

    public var formattedDayMonthDay: String {
        return Formatters.formattedDayMonthDay.string(from: self)
    }

    public var formattedHourMinute: String {
        return formattedWith("h:mm a")
    }

    public func formattedWith(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    public func formattedWith2(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
//    func abcEmmysDateFormatter: String {
//         return formattedWith("h:mm a")
//    }
}
