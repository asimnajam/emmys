//
//  SecondryButton.swift
//  Emmys
//
//  Created by Asim Najam on 2/9/20.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

final class SecondryButton: UIButton {
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func setupViews() {
        backgroundColor = UIColor.white
        setTitleColor(UIColor.gray, for: .normal)
        layer.cornerRadius = 5.0
        layer.borderColor = Colors.blueColor.cgColor
        layer.borderWidth = 1.0
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 5.0
        layer.borderColor = Colors.blueColor.cgColor
        layer.borderWidth = 1.0
        
        if emmysManager.currentLanguage == .arabic {
            contentHorizontalAlignment = .right
            titleEdgeInsets.right = 10.0
        } else {
            contentHorizontalAlignment = .left
            titleEdgeInsets.left = 10.0
        }
    }
}
