//
//  PrimaryButton.swift
//  Emmys
//
//  Created by Syed Asim Najam on 02/02/2020.
//  Copyright © 2020 Emmys. All rights reserved.
//

import UIKit

public final class PrimaryButton: UIButton {
    static let height: CGFloat = 50.0
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }

    func setupViews() {
        backgroundColor = Colors.blueColor
        setTitleColor(.white, for: .normal)
    }

    public override var isEnabled: Bool {
        didSet {
            backgroundColor = isEnabled ? Colors.blueColor : UIColor.gray
        }
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 5.0
    }
}
